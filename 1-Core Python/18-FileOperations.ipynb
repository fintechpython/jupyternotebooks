{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "a59b416e",
   "metadata": {},
   "source": [
    "<div class=\"pagebreak\"></div>\n",
    "\n",
    "# File Operations\n",
    "\n",
    "Pythonʼs view of files and directories derives from the Unix/Linux operating system variants. For more information on files, look at the [Basics of the Unix Shell](../8-The%20Tools/1-Bash-Basics.md) in Section 8, _The Tools_.\n",
    "\n",
    "Python's [os](https://docs.python.org/3/library/os.html) module provides support for file operations and interacting with the operating system.\n",
    "\n",
    "Python's functionality largely mirrors that as provided by various command-line programs and the underlying standard C libraries upon which Python is implemented.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f8b1594f",
   "metadata": {},
   "source": [
    "## Existence\n",
    "To see whether or not a given file or directory exists, call `os.path.exists()` with the name as the argument."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "293ba1a1",
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "print(\"test_binary.dat\",os.path.exists(\"test_binary.dat\"))\n",
    "print(\"binary.dat\",os.path.exists(\"binary.dat\"))\n",
    "print(\".\",os.path.exists(\".\"))        # current directory\n",
    "print(\"..\",os.path.exists(\"..\"))      # parent directory"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0a5f5f9f",
   "metadata": {},
   "source": [
    "## Checking Filetype\n",
    "Use `os.path.isfile()` to return a Boolean on whether or the argument is a file.\n",
    "\n",
    "Use `os.path.isdir()` to return a Boolean on whether or the argument is a directory."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "da3861df",
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"isfile: test_binary.dat\", os.path.isfile(\"test_binary.dat\"))\n",
    "print(\"isdir: test_binary.dat\", os.path.isdir(\"test_binary.dat\"))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b0ef70d4",
   "metadata": {},
   "source": [
    "## Deleting Files\n",
    "To delete a file, use `os.remove()`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1f0f889c",
   "metadata": {},
   "outputs": [],
   "source": [
    "os.remove(\"test_binary.dat\")\n",
    "os.path.exists(\"test_binary.dat\")   #verify that file was removed"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "022de68a",
   "metadata": {},
   "source": [
    "## File Information: stat\n",
    "To get details (Unix/Linux calls \"status\"), call `os.stat()`.  This returns an object with various fields to represent the permissions on the file, the file's type, size, owner, group, and various timestamps. \n",
    "\n",
    "[stat documentation](https://docs.python.org/3/library/os.html#os.stat)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d1cda02e",
   "metadata": {},
   "outputs": [],
   "source": [
    "stat_obj = os.stat('.')\n",
    "print(stat_obj)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "649eefe7",
   "metadata": {},
   "source": [
    "Initially, that result looks very esoteric, but once we break down a few of the fields, it makes more sense.\n",
    "\n",
    "The `st_mode` contains the file type and permissions associated with the file.  Using `ls -l`, we see this data represented with a string that looks like '-rwxr-xr-x'.  This first character specifies the type: '-' for files and 'd' for directories.  The next nine characters represent the user, group, and world permissions in terms of read, write, and execute.  Typically, st_mode makes more sense in its octal representation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c1d63006",
   "metadata": {},
   "outputs": [],
   "source": [
    "print(oct(stat_obj.st_mode))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2df4e38c",
   "metadata": {},
   "source": [
    "The first number represents the file type.  You will see 40 for a directory and 100 for a file.  The last three numbers correspond to the owner, group, and world permissions using a bit representation for read, write, and execute. For example, 111 in binary equals 7 in octal - so read, write, and execute permssions are set for that group.  101 = 5 in octal, so only read and execute permissions are set.  100 = 4 in octal, so only read.\n",
    "\n",
    "For more explanation, see the \"Understanding and Modifying File Permissions\" section of [Overview of the Unix File System](https://web.archive.org/web/20210419161551/https://homepages.uc.edu/~thomam/Intro_Unix_Text/File_System.html).\n",
    "\n",
    "st_size is the number of bytes to contain the file's contents.\n",
    "\n",
    "st_atime, st_mtime, and st_ctime represent when the file was last accessed, modified, and created.  The times are specified in seconds. To convert to a date and time, they present the number of seconds since the Unix epoch, which is midnight on January 1st, 1970.  While this fact seems  esoteric, this is a ubiquitous representation of dates and times.  Fortunately, as with other languages, Python provides APIs to perform the necessary conversion into a datetime object."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8d028e43",
   "metadata": {},
   "outputs": [],
   "source": [
    "import datetime\n",
    "accessed_dt = datetime.datetime.utcfromtimestamp(stat_obj.st_atime).replace(tzinfo=datetime.timezone.utc)\n",
    "print(accessed_dt.isoformat())"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "930fb9e5",
   "metadata": {},
   "source": [
    "## Directory Operations\n",
    "As with files, Python supports various directory operations."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a7aa62cc",
   "metadata": {},
   "source": [
    "### Create Directory\n",
    "Use `os.mkdir()` to create a new directory "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "655ecf20",
   "metadata": {},
   "outputs": [],
   "source": [
    "os.mkdir('newDir')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4c76e051",
   "metadata": {},
   "source": [
    "### List Directory Contents\n",
    "Use `os.listdir()` list the contents of a directory.  This method returns a list of file names (strings) within that directory."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6ff6dec4",
   "metadata": {},
   "outputs": [],
   "source": [
    "os.listdir('newDir')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "51c165ed",
   "metadata": {},
   "outputs": [],
   "source": [
    "os.listdir('.')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e21b73f0",
   "metadata": {},
   "outputs": [],
   "source": [
    "# now, make a subdirectory in newDir\n",
    "os.mkdir('newDir/newSubDir')\n",
    "os.listdir('newDir')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "159f7b93",
   "metadata": {},
   "outputs": [],
   "source": [
    "with open(\"newDir/newSubDir/dickens.txt\", 'w') as f:\n",
    "    f.write('It was the best of times,\\n, it was the worst of times.\\n')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6e4fde8c",
   "metadata": {},
   "outputs": [],
   "source": [
    "os.listdir('newDir/newSubDir')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "151430c8",
   "metadata": {},
   "source": [
    "### Delete Directory\n",
    "To delete a directory, use `os.rmdir()`.  However, the directory must be empty to be deleted – it cannot contain any other files or directories.  You cannot use `os.remove()` to delete a directory, only a file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5bac0a8b",
   "metadata": {},
   "outputs": [],
   "source": [
    "# this will cause an error as remove can't be used on directory\n",
    "os.remove('newDir/newSubDir')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ba973db5",
   "metadata": {},
   "outputs": [],
   "source": [
    "# this will cause an error as the directory is not empty\n",
    "os.rmdir('newDir/newSubDir')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b91d921c",
   "metadata": {},
   "source": [
    "Fix the following code block to delete the text file created above first."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1132ef06",
   "metadata": {},
   "outputs": [],
   "source": [
    "# add a method call here\n",
    "\n",
    "# the following two lines of code are correct\n",
    "os.rmdir('newDir/newSubDir')\n",
    "os.path.exists('newDir/newSubDir')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "da403b73",
   "metadata": {},
   "source": [
    "### Change the Current Working Directory\n",
    "Use `os.chdir()` to change the current working directory."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d0c2e967",
   "metadata": {},
   "outputs": [],
   "source": [
    "os.chdir('newDir')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c5916c35",
   "metadata": {},
   "source": [
    "Now, enter the method call to list the contents of the current directory"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3d3f7824",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "fbe9a3df",
   "metadata": {},
   "source": [
    "For other file and directory operations, look at the [os](https://docs.python.org/3/library/os.html#module-os) module."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "463cda19",
   "metadata": {},
   "outputs": [],
   "source": [
    "os.chdir('..')  # move the current directory back to our starting point"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a091ca77",
   "metadata": {},
   "source": [
    "## Pathnames\n",
    "Most computers use a hierarchical file system. As such, we have a current working directory based on our current shell session. Other times, a setting when an executable starts can establish the working directory. At the command line within the shell session), you can print the working directory with `pwd`.  With Python, we get the current working directory with "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "de10f850",
   "metadata": {},
   "outputs": [],
   "source": [
    "os.getcwd()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "54378660",
   "metadata": {},
   "source": [
    "Within Jupyter Notebooks, we can also call out to the operating system:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8c33d7bf",
   "metadata": {},
   "outputs": [],
   "source": [
    "!pwd"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c22a83c6",
   "metadata": {},
   "source": [
    "Throughout this notebook (and in most file/directory operation commands), we pass a directory name or file name as arguments into the various function calls. As we specify those names, we can  either specify *absolute* or *relative* pathnames.  *Absolute* pathnames start from the root (top) directory - these pathnames start with a `/`. *Relative* pathnames start from the current directory.  As demonstrated in this notebook's first code block, `.` refers to the current directory, and `..` refers to its parents.  \n",
    "\n",
    "To separate directories, most systems use a forward slash `/`. The exception is Windows, which uses a backward slash `\\`.  The reasoning dates back to the early days of MS-DOS in the 1980s.  The '/' was used to specify command line arguments, whereas Unix typically uses a dash `-`.  Windows is slowly migrating away from the `\\`.  Within PowerShell, you can specify names with a `/`, PowerShell converts it automatically to `\\`. Powershell uses `-` to specify arguments. This migration demonstrates how difficult it is to overcome an implemented decision."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "15c84899",
   "metadata": {},
   "source": [
    "### Finding Absolute Pathnames\n",
    "From a relative pathname, we can determine the absolute pathname with `os.path.abspath()`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "43fddc93",
   "metadata": {},
   "outputs": [],
   "source": [
    "os.path.abspath('.')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c54cbbae",
   "metadata": {},
   "source": [
    "### Creating Pathnames\n",
    "We can build a pathname from several parts(i.e., strings) by using `os.path.join()`.  This function combines names with the proper path separation character for the current operating system."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0d7c0080",
   "metadata": {},
   "outputs": [],
   "source": [
    "os.path.join('stuff','foo','bar.txt')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "77c04267",
   "metadata": {},
   "source": [
    "## Pathlib\n",
    "In Python 3.4, the language developers added the `pathlib` module.  This module provides an alternative to the `os` module presented in this notebook.\n",
    "\n",
    "The `pathlib` module introduced a `Path` class to treat files and directories as objects with methods we call from that object rather than strings and calling functions under `os`.  \n",
    "\n",
    "[Further details](https://docs.python.org/3/library/pathlib.html)  The very bottom of that page shows the correspondence between the two approaches."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "286a7c69",
   "metadata": {},
   "source": [
    "## Suggest LLM Prompts\n",
    "- Explain how to work with file paths and directories in Python using the os and pathlib modules. Cover operations such as creating, \n",
    "  renaming, moving, and deleting files and directories. Provide examples of joining paths and handling relative and absolute paths.\n",
    "- Create a beginner-friendly tutorial that introduces the basic file operations in Python using the os and pathlib modules. Cover \n",
    "  topics such as creating, reading, writing, and deleting files and directories. Include clear examples and explanations for each \n",
    "  operation. Ensure each section has a detailed explanation.\n",
    "- Write a detailed tutorial on managing file and directory permissions in Python using the os module. Cover topics such as checking \n",
    "  permissions, modifying permissions (chmod), and handling permission-related exceptions. Provide examples for different operating systems.\n",
    "- Develop a comprehensive guide on advanced path manipulation techniques using the pathlib module in Python. Cover topics such as \n",
    "  path normalization, joining paths, extracting components (parent, stem, suffix), and handling different path flavors (Windows vs. Unix).\n",
    "- Create a comprehensive article that compares the os.path module and the pathlib module in Python for handling file and directory operations. \n",
    "  Discuss the advantages and disadvantages of each approach, and provide guidance on when to use one over the other."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "df957562",
   "metadata": {},
   "source": [
    "## Review Questions\n",
    "\n",
    "1. What is the purpose of the os module in Python, and what kind of operations does it support?\n",
    "2. How do you check if a file exists before attempting to open it?\n",
    "3. What is the purpose of the `pathlib` module, and how does it differ from the `os` module when working with file paths?\n",
    "4. How do you create a new directory in Python?\n",
    "5. How can you list the contents of a directory using the os module?\n",
    "6. What is the difference between `os.remove()` and `os.rmdir()` functions, and when would you use each one?\n",
    "7. Explain the concept of the current working directory? How do you determine this within a Python program? How do you change it in Python?\n",
    "8. List the various attributes of a file? How do you access this in Python? What about from the command-line?\n",
    "9. How can you handle file permissions using the `os` module and the `stat` module?\n",
    "10. What is the significance of the Unix epoch (January 1st, 1970) in file operations, and how is it related to the timestamps returned by `os.stat()`?\n",
    "\n",
    "[answers](answers/rq-18-answers.md)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "02949aa6",
   "metadata": {},
   "source": [
    "## Exercise\n",
    "\n",
    "1. *Sizes*:  For the current working directory, print each of the files on a separate line.  Each line should start with the file \n",
    "   size in bytes, followed by a tab character, and then the file's name. Do not display subdirectories. Sort this output by the \n",
    "   file name. After all of the lines have been, print a blank line and then this line: \n",
    "   ```\n",
    "   Directory size: XXXX\n",
    "   ```\n",
    "   where XXX is the total of all the file sizes (excluding subdirectories). \n",
    "\n",
    "2. *File Renaming and Moving*: Develop a Python script that renames all files with the extension \".txt\" in a specified directory \n",
    "   to have the prefix \"backup_\" before the original filename. Additionally, move all renamed files to a new directory called \"backup_files.\"\n",
    "\n",
    "3. *Creating and Deleting Files and Directories*: Write a Python script that creates a new directory called \"project_files\" in the \n",
    "   current working directory. Within this new directory, create three subdirectories named \"data,\" \"scripts,\" and \"output.\" \n",
    "   Additionally, create an empty file called \"README.txt\" in the \"project_files\" directory. Finally, delete the \"scripts\" subdirectory and its contents.\n",
    "\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
