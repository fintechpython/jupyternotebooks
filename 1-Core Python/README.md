# Core Python

1. Introduction
2. Problem Solving by Programming
3. Types, Values, Variables, Names
4. Boolean, Numbers, Operations
5. Basic Input and Output
6. Control Statements
7. Functions
8. Strings
   - Other String Functions
9. Formatting Strings
   - Old Style
   - fStrings
10. Lists
11. Tuples
12. Random Numbers
13. Iteration
14. Dictionaries
15. Sets
16. Files
17. File Operations
18. Recursion
19. Modules
20. Validation, Exceptions, and Error Handling
21. Testing
22. Debugging
23. Classes and Objects
24. Classes and Objects-Inheritance
25. Comprehensions
26. Date and Time
27. Regular Expressions
