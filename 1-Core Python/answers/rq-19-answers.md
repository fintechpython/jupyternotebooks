# Core Python / Errors

1. *Early in this notebook, it was stated that your programs "Do not need to handle errors arising from 
   issues in the environment (e.g., operating system, hardware, or other systems)". Why is this the case for classwork?*

   In a classwork situation, especially an introductory course, the primary focus is typically on teaching and 
   learning fundamental concepts, algorithms, and programming techniques. Handling the complexities of error handling 
   arising from environmental issues like operating system errors, hardware failures, or other system malfunctions is 
   beyond expectations for beginner programmers.


2. *From an industrial perspective, discuss why we should handle such errors.  Provide an example of what could occur if such errors were
   not appropriately handled.  It may help to look at 
   [cyber-physical systems](https://en.wikipedia.org/wiki/Cyber%E2%80%93physical_system).*

   Industrial applications often run in unpredictable and diverse environments where errors can occur due 
   to various factors such as hardware failures, network issues, or operating system glitches. Proper error 
   handling ensures that the software remains reliable and stable even in the face of unexpected circumstances, 
   minimizing downtime and ensuring continuous operation.
   
   Imagine a financial trading application used by investment firms to execute trades in the stock market. This application relies on network connections to communicate with stock exchanges and execute trades. If the application encounters a network error due to unstable internet connectivity or server issues on the exchange's side, and this error is not appropriately handled, several negative consequences could occur:
   - The application might crash, resulting in loss of trading opportunities and potential financial losses for the firm.
   - Unhandled errors could leave trades in an inconsistent state, leading to discrepancies in the firm's trading records and potentially violating regulatory requirements.
   - Without proper error handling, users may not be aware of the underlying issues, leading to frustration and loss of trust in the application's reliability.
   - Debugging the issue becomes more challenging without detailed error logs, prolonging downtime and impacting the firm's ability to conduct business effectively.
   
   In this scenario, robust error handling mechanisms are essential to ensure the reliability, integrity, and security of the financial trading application, as well as to maintain compliance with industry regulations and standards.


3. *Revisit some of your recent programs.  How can the program be restructured into more functions?  Does this make your program easier to understand?*

   Reading a series of well-named function calls should be easier to understand than a larger series of statements.
