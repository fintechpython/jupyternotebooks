# Core Python / File Operations

1. *What is the purpose of the os module in Python, and what kind of operations does it support?*
 
   The os module in Python provides a way of using operating system-dependent functionality, offering 
   a suite of functions to interact with the operating system. It supports a wide range of operations 
   such as file and directory manipulation (e.g., creating, deleting, and checking the existence of 
   files and directories), process management (e.g., executing shell commands and retrieving process 
   information), and environment variable handling. This module is essential for tasks that require 
   interaction with the file system or the execution environment.

2. *How do you check if a file exists before attempting to open it?*

   You can check if a file exists before attempting to open it using either the `os` module or the `pathlib` module. 

   **`os` Module**: use the `os.path.isfile()` function to check if a file exists and is a regular file:

   ```python
   import os
   
   file_path = 'example_file.txt'
   if os.path.isfile(file_path):
       print("File exists.")
   else:
       print("File does not exist.")
   ```

   **`pathlib` Module**: use the `exists()` method of a `Path` object to check if a file exists:
   ```python
   from pathlib import Path
   
   file_path = Path('example_file.txt')
   if file_path.exists():
       print("File exists.")
   else:
       print("File does not exist.")
   ```

3. *What is the purpose of the `pathlib` module, and how does it differ from the `os` module when working with file paths?*

   The pathlib module in Python provides an object-oriented approach to handling file system paths. 
   Its purpose is to simplify path manipulations and make them more intuitive by representing file 
   system paths as `Path` objects, which offer methods and properties for common operations.

   Key Differences from the `os` Module:
   1. Object-Oriented vs. Function-Based:
      - `pathlib`: Uses Path objects, allowing methods to be called directly on these objects (e.g., `path.exists()`).
      - `os`: Uses function-based calls with string paths (e.g., `os.path.exists('path/to/file')`).
   2. Ease of Use:
      - `pathlib`: Provides a more readable and concise syntax, particularly for complex path manipulations and chaining methods.
      - `os`: Typically requires combining multiple functions from `os` and `os.path`, which can be less intuitive.
   3. Cross-Platform Compatibility:
      - `pathlib`: Automatically handles differences between operating systems (e.g., path separators), providing a more uniform interface.
      - `os`: Requires more manual handling of these differences (e.g., using `os.path.join()` to ensure correct separators).
   4. Enhanced Functionality:
      - `pathlib`: Offers additional methods and properties specific to path objects (e.g., `path.parents`, `path.parts`), 
        making it easier to work with hierarchical structures and metadata.
      - `os`: While comprehensive, it relies on a combination of different modules and functions to achieve similar functionality.

4. *How do you create a new directory in Python?*
  
   To create a new directory in Python using the os module, you can use the `os.mkdir()` function. 
   This function creates a single directory at the specified path. If you need to create multiple 
   nested directories, you can use `os.makedirs()`.
   ```python
   import os

   # Specify the directory path
   directory_path = 'new_directory'

   # Create the directory
   os.mkdir(directory_path)

   # Verify the directory was created
   if os.path.isdir(directory_path):
       print(f"Directory '{directory_path}' created successfully.")
   else:
       print(f"Failed to create directory '{directory_path}'.")

   # Specify the nested directory path
   nested_directory_path = 'parent_directory/child_directory'

   # Create the nested directories
   os.makedirs(nested_directory_path)

   # Verify the nested directories were created
   if os.path.isdir(nested_directory_path):
       print(f"Nested directories '{nested_directory_path}' created successfully.")
   else:
       print(f"Failed to create nested directories '{nested_directory_path}'.")
   ```

5. *How can you list the contents of a directory using the os module?*

   To list the contents of a directory using the `os` module, use the `os.listdir()` function. 
   This function returns a list of the names of the entries in the specified directory.
   ```python
   import os
   
   # Specify the directory path
   # directory_path = 'your_directory'

   # List the contents of the directory
   try:
       contents = os.listdir(directory_path)
       print(f"Contents of '{directory_path}':")
       for item in contents:
           print(item)
   except FileNotFoundError:
       print(f"The directory '{directory_path}' does not exist.")
   except NotADirectoryError:
       print(f"The path '{directory_path}' is not a directory.")
   except PermissionError:
       print(f"Permission denied to access '{directory_path}'.")
   ```

6. *What is the difference between `os.remove()` and `os.rmdir()` functions, and when would you use each one?*

   `os.remove()` is used to delete a file while `os.rmdir()` is used to delete an _empty_ directory.

7. *Explain the concept of the current working directory? How do you determine this within a Python program? How do you change it in Python?*

   The current working directory (CWD) is the directory from which a program is currently being executed or from which it is accessing files. When you run a Python script, the CWD is typically the directory containing the script unless explicitly changed.

   **Determining the Current Working Directory in Python**

   Use the os module's os.getcwd() function:
   ```python
   import os

   cwd = os.getcwd()
   print("Current Working Directory:", cwd)
   ```

   **Changing the Current Working Directory in Python**
   Use the os module's os.chdir() function:
   ```python
   import os

   new_directory = '/path/to/new/directory'

   try:
       os.chdir(new_directory)
       print("Current working directory changed to:", new_directory)
   except FileNotFoundError:
       print("The specified directory does not exist.")
   except PermissionError:
       print("Permission denied to change the current working directory.")
   ```

8. *List the various attributes of a file? How do you access this in Python? What about from the command-line?*

   **Attributes of a File**
   1. File Name: The name of the file.
   2. File Path: The absolute or relative path to the file.
   3. File Size: The size of the file in bytes.
   4. File Type/Extension: The type or extension of the file (e.g., .txt, .jpg, .py).
   5. File Permissions: The permissions that determine who can read, write, or execute the file.
   6. Owner: The owner of the file.
   7. Group: The group associated with the file.
   8. Timestamps:
      - Creation Time: The time when the file was created.
      - Modification Time: The time when the file's content was last modified.
      - Access Time: The time when the file was last accessed.

   **Accessing File Attributes in Python**

   Access file attributes using the `os` and `os.path` modules or the `pathlib` module. 
   ```python
   import os

   file_path = 'example_file.txt'

   # File name
   file_name = os.path.basename(file_path)

   # File path
   absolute_path = os.path.abspath(file_path)
   relative_path = os.path.relpath(file_path)

   # File size
   file_size = os.path.getsize(file_path)

   # File extension
   file_extension = os.path.splitext(file_path)[1]

   # File permissions (Unix-like systems)
   file_permissions = os.stat(file_path).st_mode

   # File timestamps
   creation_time = os.path.getctime(file_path)
   modification_time = os.path.getmtime(file_path)
   access_time = os.path.getatime(file_path)
   ```

   ** Accessing File Attributes from the Command Line **
   - `ls -la`
   - `pwd`

9. *How can you handle file permissions using the `os` module and the `stat` module?*

   **Get the current permissions**

   You can use the `os.stat()` function to get information about a file or directory, including its 
   permissions. The permissions are stored in the st_mode field of the returned stat result object.

   ```python
   import os
   import stat

   file_path = 'example.txt'
   st = os.stat(file_path)
   current_permissions = stat.filemode(st.st_mode)
   print(f"Current permissions: {current_permissions}")
   ```

   **Interpret the permissions**

   The `st_mode` value is an octal number representing the file type and permissions. 
   The `stat.filemode()` function can be used to convert this value into a string representation, 
   which is easier to interpret. For example, 'rw-r--r--' means the file is readable and writable 
   by the owner, but only readable by group and others.

   **Modify the permissions**

   To modify the permissions, you can use the `os.chmod()` function, which takes the file path 
   and the new permissions as arguments. The new permissions can be specified using an octal value or 
   a combination of constants from the stat module.
   ```python
   import os
   import stat

   file_path = 'example.txt'

   # Using octal value
   new_permissions = 0o644  # rw-r--r--
   os.chmod(file_path, new_permissions)

   # Using stat constants
   new_permissions = stat.S_IRUSR | stat.S_IWUSR | stat.S_IRGRP | stat.S_IROTH
   os.chmod(file_path, new_permissions)
   ```
   The `stat` module provides constants representing different permission bits, such as 
   `S_IRUSR` (read permission for owner), `S_IWUSR` (write permission for owner), 
   `S_IXUSR` (execute permission for owner), and similar constants for group and others.

10. *What is the significance of the Unix epoch (January 1st, 1970) in file operations, 
    and how is it related to the timestamps returned by `os.stat()`?*

    The Unix epoch, which is January 1st, 1970, at 00:00:00 UTC, is a widely used reference point for representing timestamps in computer systems. It has significant relevance in file operations and is directly related to the timestamps returned by the os.stat() function in Python.

    When you use `os.stat()` to retrieve information about a file or directory, it returns several fields, including `st_atime`, `st_mtime`, and `st_ctime`, which represent the last access time, last modification time, and last metadata change time, respectively. These timestamps are represented as Unix timestamps, which are the number of seconds that have elapsed since the Unix epoch.

    For example, if `os.stat()` returns `st_mtime` as `1681234567`, it means that the file was last modified at the time corresponding to `1,681,234,567` seconds after the Unix epoch (January 1st, 1970, 00:00:00 UTC).

    The significance of using the Unix epoch as a reference point lies in its simplicity and universality. By representing timestamps as the number of seconds elapsed since a fixed point in time, it becomes easier to perform arithmetic operations on timestamps, such as calculating the difference between two timestamps or adding/subtracting time intervals.

    Additionally, the Unix epoch is a widely accepted standard, and most operating systems, programming languages, and libraries use it as the reference point for representing timestamps. This standardization ensures compatibility and interoperability between different systems and applications.

    To convert the Unix timestamps returned by `os.stat()` into human-readable date and time formats, Python provides the `datetime` module. Here's an example of how to convert the st_mtime timestamp into a formatted date and time string:
    ```python
    import os
    from datetime import datetime
  
    file_path = 'example.txt'
    st = os.stat(file_path)

    # Convert st_mtime to a datetime object
    last_modified = datetime.fromtimestamp(st.st_mtime)

    # Format the datetime object as an ISO-8601 string
    print("Last modified:",last_modified.isoformat())
    ```