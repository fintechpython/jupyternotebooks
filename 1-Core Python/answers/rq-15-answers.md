# Core Python / Sets

1. *What is a set in Python, and how is it different from other data structures like lists and dictionaries?*

   A set is an unordered collection of immutable objects with no duplicates. Sets are mutable similar to sets and dictionaries.
   Tuples and lists are ordered and can contain duplicate entries. Dictionaries are ordered by their insertion order. Sets and 
   dictionaries can be considered to be the same if you ignore the values in the dictionary.

   You can create a set with the `{}` or the `set()` built-in function.

2. *How do you create a set from a list or a string in Python?*

   You can pass the list or string into the `set()` function.

3. *What is the union operation on sets, and how do you perform it in Python?*

   The union operation creates a new set from the combination of two sets where the member of the new set is in either set or both sets.

   You can either use the `|` operator or call the `union()` function on one of the sets.

   ```python
   us_flag_colors = set(["red", "white", "blue"])
   switzerland_flag_colors = set(["white", "red"])
   mexico_flag_colors = {"green", "white", "red"}
   germany_flag_colors = {"black", "red", "gold"}

   print(us_flag_colors | germany_flag_colors)    # prints {'black', 'gold', 'white', 'red', 'blue'}
   print(mexico_flag_colors.union(switzerland_flag_colors))  # prints {'white', 'red', 'green'}
   ```

4. *What is the intersection operation on sets, and how do you perform it in Python?*

   Returns a new set with all of the elements that exist in both sets.  Can use `intersection()` or `&`

   ```python
   us_flag_colors = set(["red", "white", "blue"])
   switzerland_flag_colors = set(["white", "red"])
   mexico_flag_colors = {"green", "white", "red"}
   germany_flag_colors = {"black", "red", "gold"}

   print(us_flag_colors & germany_flag_colors)                       # prints {'red'}
   print(mexico_flag_colors.intersection(switzerland_flag_colors))   # prints {'red', 'white'}
   ```

5. *How do you find the difference between two sets in Python?*

   Returns a new set with the elements in the first set that are not in the other set.  Use `difference()` or `-`

   ```python
   us_flag_colors = set(["red", "white", "blue"])
   switzerland_flag_colors = set(["white", "red"])
   mexico_flag_colors = {"green", "white", "red"}
   germany_flag_colors = {"black", "red", "gold"}

   print(us_flag_colors - germany_flag_colors)    # displays {'blue', 'white'}
   print(mexico_flag_colors.difference(switzerland_flag_colors))   # displays {'green'}
   ```


6. *How do you add a new element to an existing set in Python?*

   Use the `add()` function.

7. *How do you remove an element from a set in Python? What happens if the value does not exist?*

   Use `remove()` to delete (remove) an item from a set by the value. If the value does not exist, the Python interpreter raises a `KeyError`. 
   `discard()` will remove a value from a set, but will not raise an error if the value does not exist.

8. *How do you copy a set in Python?*

   Use the method `copy()` on the `set` object. This performs a shallow copy.  
