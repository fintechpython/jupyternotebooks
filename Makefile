# Builds the static version of the notebooks and markdown pages for the Programming Guide for Financial Technology
.PHONY: clean preview book

BROWSER_OPEN=open
ifeq ($(shell uname),Linux)
BROWSER_OPEN=./open.sh
endif

preview: book
	open -a "Google Chrome.app" public/index.html

book: clean
	. venv/bin/activate && export PYDEVD_DISABLE_FILE_VALIDATION=1 && jupyter-book build --all . --builder html
	mv _build/html/* public/
	# jupyter-book doesn't handle <img> tags, so we need to manually move files
	cp -r 0-Preliminaries/windows_media public/0-Preliminaries
	cp -r 0-Preliminaries/macos_media public/0-Preliminaries
	cp -r 1-Core\ Python/images public/1-Core\ Python
	cp -r 6-WebDevelopment/img public/6-WebDevelopment
	cp -r 8-The\ Tools/images public/8-The\ Tools

clean:
	rm -rf public/*

show:
	$(BROWSER_OPEN) -a "Google Chrome.app" public/index.html

push: book
	git add *
	git commit -m "static page updates"
	git push

epub:
	. venv/bin/activate && export PYDEVD_DISABLE_FILE_VALIDATION=1 && jupyter-book build --all . --builder custom --custom-builder epub

