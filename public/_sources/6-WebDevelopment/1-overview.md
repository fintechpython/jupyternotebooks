# Web Development

![](img/cover-img.png)

The Internet has revolutionized a number of industries since its emergence into the public mainstream in the 1990s.  The financial industry is no exception:
- Online banking 
- Financial investing (online brokerage services)
- Business to business transactions
- Research Information
- Blockchain and cryptocurrencies
- Mobile payments
- Crowdfunding
- ...

[Sir Tim Berners-Lee](https://en.wikipedia.org/wiki/Tim_Berners-Lee) developed the
[basic technology behind the World Wide Web](https://www.w3.org/History/1989/proposal.html) while working
at [CERN](https://en.wikipedia.org/wiki/CERN) in 1989.  By October of 1990, he had written three fundamental technologies that remain at
the core of today's  Internet ([brief history](https://www.w3.org/History.html)):
- _Hypertext Markup Language_ (HTML): the markup (formatting) language for the web
- _Hypertext Transfer Protocol_ (HTTP): Protocol to allow clients and servers to send and receive information from each other
- _Uniform Resource Locator_ (URL): an address used to identify resources on the web.

From these core technologies, several additional ones have also been created:
- _JavaScript_: a versatile programming language that powers dynamic web content, enabling interactive and responsive user experiences
- _Cascading Style Sheets_ (CSS): a styling language that defines the visual presentation of web content, enabling consistent and appealing designs which have transformed web aesthetics and usability

One of the most important development areas for creating financial platforms, apps, and services is **web development**. The primary learning areas covered in this section include -

| **Category**             | **Key Learning Areas**  | **Importance in FinTech**    |
|--------------------------|-------------------------|------------------------------|
| **Frontend Development**  | - HTML5, CSS3, JavaScript<br> - UI/UX Design | Vital for creating user interfaces for financial apps, online banking, and financial dashboards.      |
| **Backend Development**   | - Flask<br> - RESTful APIs | Handles the logic, database connections, and real-time transaction updates crucial in FinTech apps.    |
| **Web Security**          | - HTTPS, SSL/TLS<br> - OAuth2, JWT for Authentication/Authorization<br> - Secure Data Transmission | Securing sensitive financial data (personal info, transactions) is fundamental in FinTech.|
| **Data Visualization**    | - Charting APIs (e.g. [plotly](https://plotly.com/graphing-libraries/)<br> - Real-time Data Dashboards<br> - Financial Analytics | Important for displaying key financial metrics and real-time data insights on web platforms.           |
| **APIs & Financial Integration** | - Open Banking APIs<br> - Integration with financial services (Plaid, Yodlee, etc.) | APIs are key for accessing external financial data and services, integrating seamlessly into FinTech products. |




## Clients and Servers
Web development is the process of implementing (programming) websites and applications that users can access over the Internet. However, the Internet is a network involving _many_ different computers, all communicating with each other. These computers can be roughly divided into two distinct groups: **servers** store ("host") content and provide ("serve") it to other computers, while **clients** request that content from the servers and then present it to the human users. Generally, servers are computers owned and managed by large organizations, while clients are individual devices (laptops, phones, etc) owned (operated) by individual users.

How do clients and servers interact? Consider the process of viewing a basic web page, such as [the Wikipedia entry on Informatics](https://en.wikipedia.org/wiki/Informatics). To view this page, the user types the web address (`https://en.wikipedia.org/wiki/Informatics`) into the address bar or clicks on a link to access the page. In either case, the user's computer is the **client**, and their browser takes that address or link and uses it to create an **HTTP Request** &mdash; a _request_ for data sent following the _**H**yper**T**ext **T**ransfer **P**rotocol_. This request is like a letter asking for information and is sent to a different computer: the **web server** that contains that information.

![A diagram of client/server communication.](img/client-side/client-server-comm.png)

The web server receives this request and, based on the request's content (including the details of the web address), decides what information to send as a **response** to the client. In general, these responses consist of many different files: the text content of the web page, styling information (font, color) for how it should look, instructions for responding to user interaction (button clicks), images or other assets to show, and so forth. Under the covers, the overall page request generates multiple responses: the first being the overall web page and then all of the resources (images, styling files, scripts) required by that web page.

The client's web browser uses these different files in the response and **renders** (create/present) the web page for the user to see: it will determine what text to show, what font and color to make that text, where to put the images, and is ready to do something else when the user clicks on one of those images. Essentially, a web browser is just a computer program that does two things: (1) sends HTTP requests on behalf of the user and (2) renders the resulting response from the server.

Given this interaction, **client-side web development** involves implementing programs (writing code) that are interpreted by the _browser_, and are executed by the _client_. It is authoring the code sent in the server's response. This code specifies how websites appear and how users interact with them. On the other hand, **server-side web development** involves implementing programs that the _server_ uses to determine which client-side code is delivered. For example, a server-side program contains the logic to determine which cat picture should be sent along with the request, while a client-side program contains the logic about where and how that picture should appear on the page.

This section first covers client-side web development over the next 10 chapters followed by presenting server-side web development using Python and the Flask library.

## URLs and Web Files
Whether the user types an address into the browser's address bar or clicks on a link (which navigates to a different address), the server determines which content to send to the browser to render based on the URL in the address bar. The **URL** (<strong><em>U</em>niform <em>R</em>esource <em>L</em>ocator</strong>) is a lot like the address on a postal letter sent within a large organization such as a university: you indicate the business address as well as the department and the person.  You will get a different response (and different data) from Alice in Accounting than from Sally in Sales. The URL acts as an _identifier_ for the page you want to see, while the page or data you want to access is the _resource_ being identified/located. More properly, URLs are a specialized form of _URI_ (_Uniform Resource Identifier_), though the two terms are often used interchangeably.

Like postal letter addresses, URLs have a very specific format:

![Diagram of the format (schema) of a URL.](img/client-side/uri-schema.png)

A URL is generally made up of the following parts (though not all need to be included):

- **`scheme`** (also **`protocol`**): the "language" that the computer will use to send the request for the resource (file). On the web, this is commonly `http` (using the HyperText Transfer protocol). The better practice is to use `https` (**s**ecure HTTP), which encrypts the communication. _Don't use insecure `http`!_   

	Other protocols exist as well. If you double-click on a web file from your local computer,
    it will open that file with the `file` protocol, meaning that the computer is accessing the
    resource from the file system.

	Web page _hyperlinks_ often include URIs with the [`mailto`](https://css-tricks.com/snippets/html/mailto-links/)
    protocol for email links, or the [`tel`](https://developers.google.com/web/fundamentals/device-access/click-to-call/click-to-call)
    protocol for phone numbers.

	When working with web systems, you may also encounter `ftp` (File Transfer Protocol) addresses,
    which are used when uploading files to some webhosts. When using `git`, you may encounter `ssh`
    (Secure Shell protocol) addressess. And there are many more network protocols as well, though
    in the web we'll usually just use `https`.

- **`domain`** (also **`host`**): the address of the web server to request information from. You can think of this as the recipient of the request letter. These are `.com` addresses you usually think of with a website, such as `google.com`.  A multitude of [top-level domains](https://en.wikipedia.org/wiki/List_of_Internet_top-level_domains) exist.

	It's also possible to directly write the domain in a URL using an [IP address](https://en.wikipedia.org/wiki/IP_address), though that isn't common in web programming. Underneath the covers, the domain name is translated to an IP address using the [Domain Name System (DNS)](https://en.wikipedia.org/wiki/Domain_Name_System)

	The host can include multiple _subdomains_, which are written before each `.`. So `pratt.duke.edu` refers to the `pratt` subdomain of the `duke.edu` domain.

	The `localhost` domain (IP `127.0.0.1`) is a special domain name that refers to the address of the _current computer_; you will use this when accessing pages on local development servers.

    ![No Place Like 127.0.0.1](img/noPlaceLikeHome.png)

- **`port`** (_optional_): used to determine where to connect to the web server. By default, web requests use port `80`, but some web servers accept connections on other ports &mdash; e.g., `8080`, `8000`, and `3000` are all common on development servers, described below.

- **`path`**: which resource on that web server you wish to access. For many web servers, you can think of this as the _relative path_ to the file starting from that server's "root" folder (usually not the computer's root folder). In the above diagram, the path `/example/info/page.html` refers to the `page.html` file in the `info` folder in the `example` folder inside the web server's root.

	If you specify a path to a folder rather than a file (including `/` as the "root" folder), most web servers will serve the file named `index.html` from that folder (i.e., the path "defaults" to `index.html`). So, `https://google.com` actually is the same as `https://google.com/index.html`. As such, `index.html` is the traditional name for the file containing a website's home page.

	While the path can be best understood as being a file path, it doesn't necessarily correspond to a server's file structure &mdash; indeed later in the course you will learn to specify _routes_ for the path that don't reflect files at all!

	```{warning}
    As in any computer program, you should always use **relative** paths in web programming, 
    and these paths are frequently (but not always!) relative to the web server's _root folder_.
    ```

- **`query`** (_optional_): extra **parameters** (arguments) included in the request about what resource to access. The leading `?` starts the query, followed by key-value pairs of parameters separated by `&` symbols. The query parameters are commonly used to access things like "search queries" when using a search engine.

- **`fragment`** (_optional_): indicates which part ("fragment") of the resource to access. These fragments allow the user to  "jump" to the middle of a web page. The leading `#` is part of the fragment.

From a historical perspective, developers initially favored `http` for performance reasons and the cost of SSL certificates.  Now, the performance cost of `https` is negligible, and that protocol offers much greater security benefits than `http`.  Services such as [Let's Encrypt](https://letsencrypt.org/) offer free SSL certificates.



### Client-Side File Types 
The web browser interprets and renders the source code files sent by a server as part of an HTTP response. As a client-side web programmer, you write this source code for the browser to interpret. Multiple different types of source code files exist:

- **`.html`** files containing code written in HTML (HyperText Markup Language). This code will specify the textual and _semantic_ content of the web page. See the chapter [HTML Fundamentals](3-html-fundamentals.md) for details on HTML. Note that opening one of these files through your file system (e.g., by double-clicking on it) will open the web page it represents in a browser.

- **`.css`** files containing code written in CSS (Cascading Style Sheets). This code is used to specify styling and _visual appearance_ properties (e.g., color and font) for the HTML content. See the chapter [CSS Fundamentals](4-css-fundamentals.md) for details on CSS.

- **`.js`** files containing code written in JavaScript. This code is used to specify _interactive behaviors_ that the website will perform &mdash; for example, what should change when the user clicks a button. Note that JavaScript files are "programs" sent over by the web server as part of the response, but are _executed_ by the browser on the client's computer. See the chapter [JavaScript Fundamentals](7-javascript.md) for details on JavaScript.

HTTP responses may include additional **asset** files, such as images (`.png`, `.jpg`, `.gif`, etc), fonts, video or music files, etc.

Modern web browsers can _render_ (interpret and display) all of these types of files, combining them into the modern, interactive web pages you use every day. You can open up almost any file inside a web browser, such as by right-clicking on the file and selecting "Open With", or dragging the file into the browser program. HTML files act as the basis for web pages, so you can open a `.html` file inside your web browser by double-clicking on it (the same way you would open a `.docx` file in MS Word):


## Servers and Hosting
It's impossible to do web development work without interacting with a server somehow. As noted above, a **server** is a computer that "serves" (provides) the resources, files, and data requested by a browser.

These files are stored on the server before being served &mdash; this is called **hosting** the resource (the resource gets to "live" on the server). Even when focusing on client-side web development, the files you want the browser to render need to be hosted (live) somewhere. You may be doing "local" development and testing, in which the files stay on your computer rather than being on a remote machine elsewhere on the internet. In this case, it is possible to have your computer's file system act as the "host" or to use a [development web server (see below)](#development-servers).

When you wish to make your website available to others (to "publish" it, called putting the site into _production_), you will need to have a server that to host those files at a URL that others can access. A wide variety of **web hosting services** are available on the Internet. Some of them may be free of cost (often providing limited storage or bandwidth &mdash; the amount of data it can serve and number of requests it can respond to), while others may charge a monthly or yearly fee. There is no standard price; you must shop for what service fits your needs and budget. The availability and quality of web hosting services change frequently; recommending hosts are outside the scope of this text &mdash; though see [Hosting with GitHub Pages](#hosting-with-github-pages) for one simple free hosting method.  This book uses an equivalent service provided by Duke University's instance of [GitLab](https://about.gitlab.com/).

Note that in addition to _hosting_ the files on a particular server, making a website available also requires having a URL at which others can access it. Most hosting services will provide such a URL &mdash; but if you want a particular name for the domain part of the URL (e.g., your own ".com"), you must purchase and register that **domain name**. As with web hosting, multiple services facilitate the buying and registering of domain names. When you register a domain, you can set it to refer to the location (IP) of your hosting server so that users who go to that domain URL will be directed to your server, which can then serve the files. Thus, if you want to make your own website, there are often two things to purchase: web hosting (space and bandwidth) and a domain name. Purchasing a domain name is not necessary for hosting a website if you're willing to accept the limits of the host's naming schema &mdash; for example, this book is hosted at `fintechpython.pages.oit.duke.edu`, which is provided for free by Duke's Information Technology group for the university community.


### Hosting with GitHub Pages
As an easy and free way to make websites available, GitHub offers free web hosting through a service called [Github Pages](https://pages.github.com/). Because GitHub repositories already store code online, it is simple to make that code available as a resource to browsers (to host it on the web). You can host a site for a user or organization account, or you can make multiple sites for different projects (repos). Project websites will be available at `https://username.github.io/reponame` &mdash; this will serve as the root folder of the repo (in particular, the `index.html` file found there).

It is possible to configure GitHub Pages for a particular repo through the Settings for that repo. However, one of the best ways to publish a web project with GitHub Pages is to use _branching_: in particular, any content on a branch named **`gh-pages`** will be automatically served as a hosted web page. This is a unique feature GitHub provides for branches with this particular name.

Using a separate branch has the benefit of helping to distinguish between your _development code_ (the `main` branch &mdash; what is currently being worked on and may contain bugs, errors, or unfinished parts) and the _production code_ (the `gh-pages` branch &mdash; a "working" version that can be shared with others). By keeping these branches separate, you can do development and make potentially breaking changes without breaking the website for users accessing it. While it is possible to serve the `main` branch directly, it is best practice in software engineering to keep development and production versions separate. Never make changes in production!

To publish a website whose code is in a GitHub repository, create a _new branch_ for the repo called exactly `gh-pages` (using `git checkout -b gh-pages`). This is your production branch &mdash; any code in this branch can be _pushed_ to GitHub and thus made available to users. Note that you will need to push the branch to GitHub for it to be served (using `git push origin gh-pages` while on that branch).

````{warning} 
Never make changes on the production `gh-pages` branch! You always want to make any edits to your code on the `main` branch and then `merge` those changes into the `gh-pages` branch. Don't stay on the `gh-pages` branch longer than it takes to merge and push. Thus, your workflow should look like:

```bash
## do all your coding on `main`!

# switch to gh-pages to publish
git checkout gh-pages

# merge the changes from main
git merge main

# push to GitHub to publish
git push origin gh-pages

# IMMEDIATELY switch back to main branch for more coding
git checkout main
```

Note that if you publish to production (push to `gh-pages`) and find a bug or problem in your site, you must fix that on the `main` branch following the above process. Yes, this can seem awkward and tedious, but that's why you must thoroughly test your work in development (on the `main` branch) before pushing to production. This is a crucial software engineering practice!
````

Duke University offers [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/).


### Development Servers
As noted above, it is possible to request a `.html` file (open a web page) using the `file` protocol by simply opening that file directly in the browser. This works fine for testing many client-side programs. However, there are a few client-side interactions that for [security reasons](https://en.wikipedia.org/wiki/Cross-origin_resource_sharing) only work if a web page is requested from a web server (e.g., via the `http` or `https` protocol).

In these situations, you must develop client-side web applications using a **local development web server**. This is a web server you run from your computer &mdash; your machine acts as a web server, and you use the browser to have your computer send a request _to itself_ for the webpage. Think of it as mailing yourself a letter. Development web servers can help get around cross-origin request restrictions, as well as offer additional benefits to speed development &mdash; such as automatically reloading the web browser when the source code changes.

Several simple development servers that you can use:
- The Python [**`http.server`**] module can run a simple web server. You can then use the command `python -m http.server` to serve the current folder's contents by default at <http://localhost:8080>.
- 
- [**`live-server`**](https://github.com/tapio/live-server) is a Node package that runs a simple web server on the command line. You can run the program and "serve" files from the current folder using the command `npx live-server .` (the `.` referring to the current folder). This will open up your `index.html` file in a browser (if not, you can view the page at <http://localhost:8080> by default). It will also _automatically_ refresh the page whenever you change a file in the folder!

- Multiple VS Code extensions provide this functionality, including [`LiveServer`](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer) and [`vscode-previw-server`](https://marketplace.visualstudio.com/items?itemName=yuichinukiyama.vscode-preview-server).

When running a local web server, remember that the address `127.0.0.1` is the IP address for `localhost`, which is the domain of your local machine (the "local host"). Most development servers, when started, will tell you the URL for the server's root directory.

Most commonly, you will want to start the web server from the root directory of your _project_, so that the relative path `index.html` finds the file you expect.

You can usually stop a command line development server with the universal `ctrl + c` cancel command. Otherwise, you'll want to leave the server running in a background terminal as long as you work on your project.


## Web Standards
In client-side development, the web files (`.html`, `.css`, and `.js` files) you author will be delivered to clients upon request and then _interpreted_ and rendered by the web browser. Indeed, a web browser is any software capable of rendering these files (and sending HTTP requests to fetch them in the first place). And there are a lot of different web browsers in the world! The below chart from StatCounter shows the relative popularity of different browsers:

<div id="all-browser-ww-monthly-202301-202401" width="600" height="400" style="width:600px; height: 400px;"></div><!-- You may change the values of width and height above to resize the chart --><p class="caption">Source: <a href="https://gs.statcounter.com/browser-market-share">StatCounter Global Stats - Browser Market Share. See also [caniuse.com's usage table](http://caniuse.com/usage-table) for information on mobile and older browsers.</a></p><script type="text/javascript" src="https://www.statcounter.com/js/fusioncharts.js"></script><script type="text/javascript" src="https://gs.statcounter.com/chart.php?all-browser-ww-monthly-202301-202401&chartWidth=600"></script>

These web browsers are all created by different developers working for different (often rival!) organizations. And while there may be some clear "winners" in terms of browser popularity, you cannot dismiss less popular browsers. For example, even if only 0.1% of users use a particular browser (such as Internet Explorer), that's still more than 5 million people worldwide! So in order to make sure that they are all able to render the same served resources, they all attempt to follow a set of web standards.

**Web Standards** are agreed-upon specifications for how the browser should render web page source code. Web standards detail both the language syntax (e.g., how to write HTML elements) and the language semantics (e.g., which HTML elements to use) so that it can be understood by any browser that follows (agrees to) that standard. And since, as a developer, you want your pages to render the same across all browsers, web standards give the requirements for writing your code so that your pages render correctly.

Modern web standards are created and maintained by a large group of stakeholders known as the [**World Wide Web Consortium (W3C)**](https://www.w3.org/), which includes the major browser developers such as Google, Apple, and Mozilla. However, this group has no enforcement powers, so browsers often deviate from the published standards! A browser may ignore a standard to "help out" developers (e.g., making a "best guess" to render malformed HTML content) or to introduce new features (e.g., a new CSS property that produces some special effect). Some browsers are better at conforming to the established standards than others. 

Getting so many people to agree on a standard of communication takes time; thus, web standards change relatively slowly: the HTML 4 standard was adopted in 1997, but the **HTML 5** standard (that this course teaches) wasn't finalized [until 2014](https://en.wikipedia.org/wiki/HTML#History). The **CSS 3** standard is broken up into more than 50 different _modules_ that are developed and introduced independently, and so is continuously being adopted piece-wise.

```{note}
When introducing new or experimental CSS properties, browsers historically used [vendor prefixes](https://developer.mozilla.org/en-US/docs/Glossary/Vendor_Prefix) in naming the properties. As standards are being decided upon, each browser may treat that property in a slightly different way, thus forcing developers who want to use the "latest and greatest" to provide a different definition or value for each vendor. Prefixes are a naming convention that will cause the property only to be understood and applied by a particular browser; e.g., <span style="white-space: nowrap">`-webkit-hyphens`</span> would use the [Webkit](https://en.wikipedia.org/wiki/WebKit) version of the `hyphens` property (for controlling word breaks), while <span style="white-space: nowrap">`-ms-hyphens`</span> would use the IE (Microsoft) version. This practice is currently discouraged (with browsers using internal flags to manage experimental features) and being phased out, though prefixes may be required when supporting older browsers. Tools such as [Autoprefixer](https://github.com/postcss/autoprefixer) can help automatically manage prefixes.
```

Writing "correct" code in web development means writing code that conforms to web standards. So, even if the content seems to render correctly on your browser, if it doesn't follow the standard, it may not work on someone else's computer (or on your computer after a software update)! Thus, the requirement for code to be correct in web development is that it not only looks correct when rendered but follows the syntactic and semantic specifications of the web standards. If your website conforms to the web standards, it will render correctly on all "_modern browsers_" (Chrome, Safari, Edge, Firefox, etc.). However, differences may still exist between browsers or operating systems.

To check what features are available on a particular browser, you can use a tool such as [**caniuse.com**](https://caniuse.com/). This site lets you search for web features (e.g., "flexbox") and then shows which browsers &mdash; and which versions of those browsers &mdash; support that feature. Green boxes indicate that the feature is supported, yellow boxes indicate partial support, and brown boxes indicate unsupported. It is probably the best source of information about browser support for features. https://compat-table.github.io/compat-table/es2016plus/ has a similar table for the latest JavaScript features, and the documentation provided by the [Mozilla Developer Network (MDN)](https://developer.mozilla.org/en-US/docs/Web) also lists browser compatibility at the end of each listing.

```{tip}
Always develop for other people's browsers, not just your own! Test your code against the standards, not just that it looks okay on a single browser.
```

One way to check that the code you write follows the standards is to **validate** it using an automated tool. The W3C provides online tools that can help validate code:
- [W3C HTML Validation Service](https://validator.w3.org/nu/)
- [W3C CSS Validation Service](http://jigsaw.w3.org/css-validator/)
- [W3C Developer Tools](http://w3c.github.io/developers/tools/) for a complete list of validators


To use these services, enter your web page's publicly accessible URL (or copy and paste the contents of your `.html` or `.css` files) and then run the validation. You will need to fix any _errors_ you get. _Warnings_ should be considered; however, it is possible to get false positives. Be sure to read the warning carefully and consider whether or not it is a "bug" in your code!

This text does not include specifics needed to ensure that web pages work on every browser &mdash; instead, the focus is on introducing standards-compliant web development techniques. Supporting older browsers that are not standards-compliant (and ensuring that _everyone_ can access your website) is left as an exercise to the reader.


## Web Accessibility
Web standards do more than establish the correct syntax for web code. Consider the following hypothetical webpage user:

> Tracy is a 19-year-old college student who was born blind. Through high school, she did as well as she could, relying on audio tapes and books and the support of tutors, so she never bothered to learn Braille. She is interested in English literature and fond of short stories; she dreams of becoming an audiobook author.
Tracy uses the Internet to share her writing and to connect with other writers through social networks. She owns a laptop and uses a screen reader called [JAWS](http://www.freedomscientific.com/Products/Blindness/JAWS): a computer program that reads her screen out loud to her in an artificial voice.
<cite><small>(Adapted from [here](http://scidok.sulb.uni-saarland.de/volltexte/2007/1098/pdf/personas_access.pdf))</small></i>

One of the most commonly overlooked limitations on a website's usability is whether or not it can be used by people with some form of disability. Many different forms of disability or impairments may affect whether or not a person can access a web page, including:

- _Vision Impairments_: About 2% of the population is blind, so use alternate mediums for reading web pages. Farsightedness and other vision problems (particularly among older adults) are also very common, requiring larger and clearer text. Additionally, about 4.5% the population is color-blind.
- _Motor Impairments_: Arthritis occurs in about 1% of the population and can restrict people's ease at using a mouse, keyboard, or touch screen. Other impairments, such as tremors, motor neuron conditions, and paralysis, may further impact people's access.
- _Cognitive Impairments_: Autism, dyslexia, and language barriers may cause people to be excluded from using your website.

If you fail to make your website accessible, you are locking out 2% or more of users, reducing the availability and use of your site. Indeed, even web companies with their capitalist worldview see this population as an important but excluded market; for example, Meta has an [entire team](https://www.facebook.com/accessibility) devoted to accessibility and supporting users with disabilities. _"Accessibility Engineers"_ have good job prospects.

Supporting users with disabilities is not just the morally correct thing to do, it's also _the law_. US Courts have [ruled](https://www.forbes.com/sites/legalnewsline/2017/06/13/first-of-its-kind-trial-goes-plaintiffs-way-winn-dixie-must-update-website-for-the-blind/#c4b8ce61b38a) that public websites are subject to [Title III of the Americans with Disabilities Act (ADA)](https://www.ada.gov/ada_title_III.htm), meaning that is a possible and not uncommon occurrence for large organizations to be sued for discrimination if their websites are not accessible. So far, "accessibility" has legally meant complying with the W3C's [Web Content Accessibility Guidelines (WCAG)](https://www.w3.org/WAI/intro/wcag) (see below), a web standard that is not overly arduous to follow if you consider accessibility from the get-go.

Finally, designing your website (or any system) with accessibility in mind will not just make it more usable for those with disabilities &mdash; it will make it more usable for _everyone_. This is the principle behind [**Universal Design**](https://en.wikipedia.org/wiki/Universal_design) (a.k.a. _universal usability_): designing for _accessibility_ &mdash; being usable by all people no matter their ability (physical or otherwise) &mdash; benefits not just those with some form of limitation or disability, but everyone. The classic real-world example of universal design is [curb cuts](https://en.wikipedia.org/wiki/Curb_cut): the "slopes" built into curbs to accommodate those in wheelchairs. However, these cuts make life better for everyone: people with roller bags, strollers, temporary injuries, small children learning to ride a bicycle, etc.

Universal design applies to websites as well:

- If you support people who can't see, then you may also support people who can't see _right now_ (e.g., because of a bad glare on their screen).
- If you support people with motor impairments, then you may also support people trying to use your website without a mouse (e.g., from a laptop while on a bumpy bus).
- If you support people with cognitive impairments, then you may also support people who are temporarily impaired (e.g., inebriated or lacking sleep).

If you ensure that your web page is well-structured and navigable by those with screen readers, it will ensure that it is navigable by _other_ machines, such as search engine indexers.

Thus, supporting accessibility in client-side web development is essential for helping a population often overlooked (a form of social justice) and supporting new technologies and systems. This fact is [increasingly being acknowledged](http://www.fastcodesign.com/3054927/the-big-idea/microsofts-inspiring-bet-on-a-radical-new-type-of-design-thinking) by companies as key to usability, and thus, you must apply it to your own design and web work.

```{note}
In addition to individual capabilities, people rely on a large amount of existing **infrastructure** to ensure that they have an internet connection and that their requests can reach your web server. The lack of such access is often tied to economic or social inequalities, forming the [**digital divide**](https://en.wikipedia.org/wiki/Digital_divide). Considering the availability of network access and other infrastructural needs is vitally important when developing information technologies. In client-side development, this may include, for example, considerations such as the amount of data being served (e.g., the size of images used). See also [Responsive Design](#responsive-css) for design considerations for devices with connectivity limitations.</p>
```

### Supporting Accessibility
Making web pages accessible is essential. This text will primarily discuss supporting accessibility for users with visual impairments, such as those using screen readers. A [**screen reader**](https://en.wikipedia.org/wiki/Screen_reader) is a piece of software that can synthesize and "read" content on a computer's screen out loud through the speakers so that users can navigate and control the computer without needing to see the screen. Screen readers are often combined with _keyboard controls_, so users just use the keyboard to control the computer and not the mouse (almost like a command line interface!).

Several different screen reader software packages are available:

- Macs have had [VoiceOver](https://help.apple.com/voiceover/info/guide) built into the operating system since 2005, though it has been refined with each new OS version.
- Windows has a built-in screen reader called [Microsoft Narrator](https://support.microsoft.com/en-us/windows/complete-guide-to-narrator-e4397a0d-ef4f-b386-d8ae-c172f109bdb1) since Windows 10. But the most popular screenreaders remain more established applications: [JAWS](http://www.freedomscientific.com/Products/Blindness/JAWS) and [NDVA](https://www.nvaccess.org/).

```{tip}
You should try out this software! Follow the above links to learn how to turn on the screen reader for your computer, and then try using it to browse the internet _without looking at the screen_. This will give you a feel for what it is like using a computer while blind.
```

Screen readers are just software that interpret the HTML of a website to allow the user to hear and navigate the content &mdash; they are basically _non-visual web browsers_. As such, supporting screen readers means implementing your website so it works with this browser. You can do this by ensuring that your site follows the web standards; &mdash; and in particular conforms to the [**Web Accessibility Content Guidelines (WCAG)**](https://www.w3.org/WAI/intro/wcag). This is a [list of principles and techniques](https://www.w3.org/WAI/WCAG20/quickref/) to use when authoring web documents in order to ensure that they are accessible. Four main principles drive the guidelines:

1. **Perceivable**: Information and user interface components must be presentable to users in ways they can perceive.
2. **Operable**: User interface components and navigation must be operable.
3. **Understandable**: Information and the operation of user interfaces must be understandable.
4. **Robust**: Content must be robust enough to be interpreted reliably by various user agents, including assistive technologies.

More concretely, accessible web pages can be **navigated** by screen readers (so people can quickly get to the information they care about) and have content that can be **perceived** by screen readers (so is not just presented visually).

Throughout this text (and particularly in [Semantic HTML](5-html-semantics.md), you will learn simple, specific implementation steps that you can use to follow these principles and ensure that your websites are accessible to all users.

## HTTP Protocol
As mentioned above, the [HTTP protocol](https://en.wikipedia.org/wiki/HTTP) is a foundational element of the modern web, enabling communication between clients, such as web browsers, and servers hosting web content. Operating as a request-response protocol, HTTP allows clients to request resources, such as HTML pages, images, or APIs, and servers to respond with the requested content or appropriate status codes. Initially designed as a stateless protocol, where each request is independent of others, HTTP has evolved to include features like persistent connections and secure communication through HTTPS (HTTP over SSL/TLS). 

The HTTP protocol is largely text-based, which means its communication relies on human-readable text for formatting requests and responses. An HTTP request typically includes a method (e.g., GET, POST), a target resource path (e.g., /index.html), and version information (e.g., HTTP/1.1), followed by headers and optional body data. For example, a simple request might look like this: (connecting to http://example.com)
```html
GET / HTTP/1.1
Host: example.com
User-Agent: curl/8.7.1
Accept: */*

```
The response then would look like this:
```html
HTTP/1.1 200 OK
Age: 570990
Cache-Control: max-age=604800
Content-Type: text/html; charset=UTF-8
Date: Tue, 14 Jan 2025 15:32:31 GMT
Etag: "3147526947+gzip+ident"
Expires: Tue, 21 Jan 2025 15:32:31 GMT
Last-Modified: Thu, 17 Oct 2019 07:18:26 GMT
Server: ECAcc (dcd/7D47)
Vary: Accept-Encoding
X-Cache: HIT
Content-Length: 1256

<!doctype html>
<html>
<head>
    <title>Example Domain</title>

    <meta charset="utf-8" />
    <style type="text/css">
    body {
        background-color: #f0f0f2;
        margin: 0;
        padding: 0;
        font-family: -apple-system, system-ui, BlinkMacSystemFont, "Segoe UI", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
        
    }
    div {
        width: 600px;
        margin: 5em auto;
        padding: 2em;
        background-color: #fdfdff;
    }
    </style>    
</head>

<body>
<div>
    <h1>Example Domain</h1>
    <p>This domain is for use in illustrative examples in documents. You may use this
    domain in literature without prior coordination or asking for permission.</p>
    <p><a href="https://www.iana.org/domains/example">More information...</a></p>
</div>
</body>
</html>
```

From a command line, you can view the HTTP request and HTTP response with [`curl`](https://curl.se/):
```bash
> curl -v http://example.com
```
```properties
* Host example.com:80 was resolved.
* IPv6: (none)
* IPv4: 93.184.215.14
*   Trying 93.184.215.14:80...
* Connected to example.com (93.184.215.14) port 80
> GET / HTTP/1.1
> Host: example.com
> User-Agent: curl/8.7.1
> Accept: */*
> 
* Request completely sent off
< HTTP/1.1 200 OK
< Age: 434690
< Cache-Control: max-age=604800
...
```
You can also view the HTTP request and response with the developer tools in the web browser:
![Developer tools screenshot](img/intro/developer_tools_http.png)


## Original Info 340 Acknowledgements
This chapter was originally authored by [Joel Ross](https://faculty.washington.edu/joelross/) and [Mike Freeman](http://mfviz.com/#/). Some content was originally adapted from [tutorials](https://drstearns.github.io/tutorials/) by [David Stearns](https://www.linkedin.com/in/david-stearns-09a27319/). Some structure and explanations inspired by [Learning Web Design](https://learningwebdesign.com/) by Jennifer Robbins. Updates made by John Slankas.
