{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "cd2adfff-eb99-4a61-9ed9-91008279f950",
   "metadata": {},
   "source": [
    "# Software and Errors\n",
    "\n",
    "Unfortunately, errors can be found in almost every software system. As mentioned in the second notebook, proving the absence of errors\n",
    "is an undecidable problem in computer science. Regardless, we still have a professional obligation to minimize/avoid such errors and\n",
    "to reduce(mitigate) impacts from those errors.\n",
    "\n",
    "From an academic standpoint, the programs that you create for your coursework: (Adapted from [1])\n",
    "> 1. Should produce the correct results for all legal inputs\n",
    "> 2. Should provide reasonable error messages and error codes for all illegal inputs\n",
    "> 3. May terminate after finding an error (unless other specified)\n",
    "> 4. Do not need to handle errors arising from issues in the environment (e.g., operating system, hardware, or other systems)\n",
    "\n",
    "As Bjarne Bjarne Stroustrup stated, the first two items relate to [ethics and professsional conduct](https://www.acm.org/code-of-ethics)\n",
    "expected of computing professionals. \n",
    "At Duke University and, more specifically, the FinTech Program, we expect this be be upheld to the maximum extent possible.\n",
    "\n",
    "Handling issues arising from the fourth point can be difficult to handlle, but absolutely required for certain systems."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3babe1f3",
   "metadata": {},
   "source": [
    "## Sources of Errors\n",
    "Errors arise from many different sources during the [software development process](https://en.wikipedia.org/wiki/Software_development_process):\n",
    "\n",
    "### Human Factors\n",
    "Human error is probably the top source of software defects. Many of the other categories below can be tied or placed into this overarching category. How well we document \n",
    "and communicate [software requirements](https://en.wikipedia.org/wiki/Software_requirements) can be challenging. Differences in education, background, culture, and environment affect how\n",
    "people interpret requirements. In the exercises for [formatting strings](09-FormattingStrings.ipynb#exercises), the reader is asked to create a function to print a receipt. However,\n",
    "those outside the United States may not be familiar with gratuities. Humans also introduce errors through design and coding mistakes. Additionally, developers often \n",
    "perform poorly when testing their own code.\n",
    "\n",
    "### Complexity\n",
    "As software systems grow in complexity to meet evolving user needs and technological advancements, so does the potential for errors. Managing intricate interactions between different components increases the likelihood of bugs and glitches.\n",
    "\n",
    "### Time and Resource Constraints\n",
    "Just as students often perform poorly when submitting homework and studying at the last possible moment, \n",
    "pressing deadlines and resource limitations often force developers to prioritize speed over thoroughness.\n",
    "Rushed development cycles can lead to insufficient testing and oversight, amplifying the risk of errors slipping through the cracks. \n",
    "Often, developers release incomplete programs as what has been completed provides needed functionality to users.\n",
    "\n",
    "### Changing Requirements\n",
    "It's almost impossible to envision and document a system completely at the start; requirements frequently change or evolve. \n",
    " While certain processes ([agile](https://en.wikipedia.org/wiki/Agile_software_development)) are designed to both mitigate and embrace the impacts of change.\n",
    "Failure to adapt code accordingly can result in inconsistencies and errors as the software evolves.\n",
    "\n",
    "### Unexpected Inputs\n",
    "Typically, programs take inputs from some source, process the input, and then produce output. Programmers frequently make assumptions about such input: \n",
    "- what's the data type?\n",
    "- expected number of arguments\n",
    "- legal values\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "767b59c8",
   "metadata": {
    "vscode": {
     "languageId": "plaintext"
    }
   },
   "source": [
    "## Types of Errors\n",
    "Errors in software development manifest in various forms, including:\n",
    "\n",
    "- **Syntax Errors**: Basic errors that violate a programming language's rules, such as missing semicolons or mismatched parentheses, leading to compilation failures.\n",
    "- **Logic Errors**: Logic errors occur when the code does not produce the intended output due to flaws in the algorithm or reasoning behind the implementation. These errors can be subtle and challenging to detect through testing alone.\n",
    "- **Runtime Errors**: Runtime errors occur during the program's execution and often result in crashes or abnormal terminations. Common examples include null pointer exceptions, division by zero, and stack overflow.\n",
    "- **Security Vulnerabilities**: Security errors encompass a broad range of issues, including injection attacks, authentication bypasses, and data breaches, which jeopardize the confidentiality, integrity, and availability of the software system.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6f7fd661",
   "metadata": {},
   "source": [
    "## Minimizing Errors\n",
    "\n",
    "Through several of the upcoming notebooks, we will present various ways to minimize errors in software:\n",
    "- Input validation\n",
    "- Handling runtime errors through exception handling\n",
    "- Testing\n",
    "\n",
    "We also have the ability to minimize errors through the software we produce. One way to do this is through organization - how we structure our programs into different parts.\n",
    "Within Computer Science, [separation of concerns](https://en.wikipedia.org/wiki/Separation_of_concerns) is a design principle that advocates dividing a software system into distinct sections, each addressing a separate aspect or responsibility. By separating code into distinct functions, the codebase can become easier to understand and maintain. Also, such functions are typically easier to test.\n",
    "\n",
    "\n",
    "We can also organize software into appropriate \n",
    "[modules](21-Modules.ipynb) and [classes](25-ClassesAndObjects.ipynb), we can such benefits as - \n",
    "1. **Encapsulation**: Modules encapsulate functionality into cohesive units with well-defined interfaces. By hiding implementation details and exposing only essential functionalities through well-defined interfaces, modules reduce the likelihood of unintended interactions and dependencies between different parts of the codebase. This encapsulation helps contain errors within specific modules, making isolating and debugging issues easier without affecting the entire system.\n",
    "2. **Abstraction**: Modules abstract complex functionalities into manageable units, allowing developers to focus on high-level concepts without getting bogged down in implementation details. By providing clear interfaces and hiding internal complexities, modules facilitate better understanding and maintenance of the codebase, reducing the risk of errors resulting from misunderstandings or misinterpretations of the code.\n",
    "3. **Modularity**: Modular design breaks down software systems into smaller, independent components that can be developed, tested, and maintained separately. This modular architecture enables teams to work on different modules concurrently, reducing development time and minimizing the risk of errors introduced by overlapping changes or conflicting modifications. Additionally, modular systems are easier to scale, extend, and evolve over time, as new functionalities can be added or modified without disrupting existing code.\n",
    "4. **Reuse**: Modules promote code reuse by encapsulating reusable functionalities into standalone components that can be easily integrated into different parts of the software system. By leveraging existing modules instead of reinventing the wheel, developers can reduce the likelihood of errors and ensure consistency and reliability across the codebase. Moreover, modular design encourages the development of libraries, frameworks, and APIs that adhere to standardized interfaces, further enhancing code reuse and interoperability.\n",
    "\n",
    "While we haven't yet introduced modules and classes, as you read those sections, think back to this section and how those concepts help create less error-prone software.\n",
    "\n",
    "```\n",
    "  ______________________________________\n",
    "/ Everything should be made as simple as \\\\\n",
    "\\\\ possible, but no simpler               /\n",
    "  --------------------------------------\n",
    "         \\   ^__^ \n",
    "          \\  (oo)\\_______\n",
    "             (__)\\       )\\/\\\\\n",
    "                 ||----w |\n",
    "                 ||     ||\n",
    "```\n",
    "While the above quote is often [attributed to Albert Einstein](https://quoteinvestigator.com/2011/05/13/einstein-simple/#google_vignette), the actual source is unknown.\n",
    "What's important, though, is how this relates to software development.  We must avoid unnecessary complexity and functions; needlessly complicating software often leads to more errors."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "af55717f",
   "metadata": {},
   "source": [
    "## Suggested LLM Prompts\n",
    "- Is proving the absence of software defects an unsolvable(intractable) problem in computer science?  How does this relate to the halting problem?\n",
    "- Why is a developer the worst person to test his or her own code?\n",
    "- Provide a Python example using financial technology as the domain to show how separation of concerns can reduce software errors.\n",
    "- Explain the importance of effective error reporting and logging in software development. \n",
    "  Discuss best practices for logging errors, such as using appropriate log levels, providing \n",
    "  contextual information, and separating log files for different components or environments. \n",
    "  Introduce techniques for aggregating and analyzing log data to identify and resolve errors more effectively.\n",
    "\n",
    "The following prompts explore different types of errors that may arise in programs. Many of these prompts introduce techniques that will be \n",
    "covered in future notebooks:\n",
    "- *Syntax Errors*: Explain what syntax errors are and provide examples of common syntax errors in \n",
    "  programming languages like Python. Discuss how to read and interpret syntax \n",
    "  error messages, and how to fix them by correcting the code structure or syntax.\n",
    "- *Runtime Errors and Exceptions*: Introduce the concept of runtime errors and exceptions. Explain the \n",
    "  difference between syntax errors and runtime errors. Provide examples of common runtime errors, such as \n",
    "  TypeError, ValueError, IndexError, and ZeroDivisionError. Discuss how to handle exceptions using\n",
    "  try-except blocks and how to raise custom exceptions.   \n",
    "- *Logical Errors*: Explain what logical errors are and how they differ from syntax and runtime errors. \n",
    "  Provide examples of logical errors, such as infinite loops, incorrect conditions, or incorrect algorithm \n",
    "  implementations. Discuss techniques for debugging and identifying logical errors, such as adding \n",
    "  print statements, using a debugger, or writing unit tests. \n",
    "- *Resource Handling Errors*: Introduce errors related to resource handling, such as file I/O errors, \n",
    "  network errors, or database errors. Explain common error scenarios, such as attempting to read from \n",
    "  a non-existent file, losing network connectivity, or accessing a locked database record. Discuss \n",
    "  error handling strategies, such as checking for file existence, retrying network operations, or implementing locking mechanisms.\n",
    "- *Numerical Errors*: Discuss numerical errors, such as overflow, underflow, and precision loss. Provide \n",
    "   examples of how these errors can occur when working with floating-point numbers or large integers. \n",
    "   Explain techniques for mitigating numerical errors, such as using appropriate data types, \n",
    "   checking for boundary conditions, and implementing error handling.\n",
    "- *Memory Management Errors*: Explain memory management errors, such as null pointer dereferences, dangling pointers, \n",
    "  and memory leaks. Provide examples of these errors in languages like C or C++, and discuss how they can lead to \n",
    "  crashes or unstable behavior. Introduce strategies for avoiding memory management errors, such as using \n",
    "  smart pointers, automatic memory management (e.g., garbage collection), or manual memory allocation and deallocation.\n",
    "- *Security Errors*: Introduce security-related errors, such as injection attacks (e.g., SQL injection, \n",
    "  command injection), cross-site scripting (XSS), and cross-site request forgery (CSRF). Explain how these \n",
    "  errors can occur and their potential consequences. Discuss best practices for preventing security errors, \n",
    "  such as input validation, sanitization, and following secure coding principles.\n",
    "- *Performance and Scalability Errors*: Introduce errors related to performance and scalability, such as \n",
    "  inefficient algorithms, excessive memory usage, or poor resource utilization. Provide examples of how \n",
    "  these errors can impact application performance and scalability. Discuss techniques for identifying and \n",
    "  addressing performance issues, such as profiling, optimizing algorithms, and implementing caching or load balancing strategies."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a495dc38",
   "metadata": {},
   "source": [
    "## Review Questions\n",
    "1. Early in this notebook, it was stated that your programs \"Do not need to handle errors arising from \n",
    "   issues in the environment (e.g., operating system, hardware, or other systems)\". Why is this the case for classwork?\n",
    "2. From an industrial perspective, discuss why we should handle such errors.  Provide an example of what could occur if such errors were\n",
    "   not appropriately handled.  It may help to look at \n",
    "   [cyber-physical systems](https://en.wikipedia.org/wiki/Cyber%E2%80%93physical_system).\n",
    "3. Revisit some of your recent programs.  How can the program be restructured into more functions?  Does this make your program easier to understand?\n",
    "\n",
    "[answers](answers/rq-19-answers.md)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7e882fe0",
   "metadata": {},
   "source": [
    "## References\n",
    "1. Bjarne Stroustrup. 2014. _Programming: Principles and Practice Using C++_, Second Edition. Addison-Wesley, USA. [O'Reilly](https://learning.oreilly.com/library/view/programming-principles-and/9780133796759/)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
