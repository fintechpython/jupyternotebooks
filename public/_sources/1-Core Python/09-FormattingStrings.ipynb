{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "6c815fb4",
   "metadata": {},
   "source": [
    "<div class=\"pagebreak\"></div>\n",
    "\n",
    "# Formatting Strings\n",
    "Before this notebook, we used string concatenation and multiple arguments to print() to format strings. Now, we will use [string interpolation](https://en.wikipedia.org/wiki/String_interpolation) to put values into strings in various formats.\n",
    "\n",
    "Python offers three different ways to format strings:\n",
    "- old style (all versions of Pythons) - see reference material if interested, not covered/tested\n",
    "- new style (Python 2.6 +)\n",
    "- f-strings (Python 3.6 +) - see reference material if interested, not covered/tested\n",
    "\n",
    "The f-string style provides an easy convenience to put variables into strings but at the cost of requiring a literal (hard-coded) value to be used as the format string in the code.  This limitation makes it harder to develop international-based applications.\n",
    "\n",
    "This notebook directly prints the formatting results, but the formatted string could be assigned to a variable or used as an expression (e.g., as an argument to a function)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3a94da08",
   "metadata": {},
   "source": [
    "## New style: {} and format()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "dcdb0336",
   "metadata": {},
   "source": [
    "This format uses the form of _format_string_.format(_data_). Within each string are one or more placeholder fields identified by curly braces `{}` that values will replace.\n",
    "\n",
    "By default, the {} are replaced by data by matching order of the arguments to data - the indexing starts at zero.  Addtionally, we can also specify name arguments to the placeholders."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "53f7e9af",
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"{}\".format(\"Hello World!\"))\n",
    "print(\"It was the {} of {}.\".format(\"best\",\"times\"))\n",
    "print(\"It was the {1} of {0}.\".format(\"times\", \"worst\"))\n",
    "print(\"It was the {time} of {arg2}.\\nIt was the {time} of {arg3} ...\".format(time='age',arg2='wisdom',arg3=\"foolishness\"))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b9e0609e",
   "metadata": {},
   "source": [
    "We can also utilize a dictionary of key value pairs.  The `0` in the code specifies the positional argument."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "98eba045",
   "metadata": {},
   "outputs": [],
   "source": [
    "d  = {'time':'age', 'arg2':'wisdom', 'arg3':'foolishness'}\n",
    "print(\"It was the {0[time]} of {0[arg2]}.\\nIt was the {0[time]} of {0[arg3]} ...\".format(d))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d9ae1453",
   "metadata": {},
   "source": [
    "To format the values placed into the strings, you will need to apply special formatting codes.  These codes follow this syntax: `{[name]:[fill][alignment][sign][minwidth][group][.][maxchars]type}`\n",
    "- An optional name that can either specify the position order or a named argument. \n",
    "- An initial colon (':') that is required if any formatting codes exist.\n",
    "- An optional fill character (default ' ') to pad the value string if it is shorter than minwidth.\n",
    "- An optional alignment character:\n",
    "  - '<' left (the default)\n",
    "  - '>' right\n",
    "  - '^' center\n",
    "- An optional sign for numbers. If `+` is specified, a plus symbol will be added to positive numbers.  Negative numbers always have a minus sign.\n",
    "- An optional minwidth. \n",
    "- An optional group that can contain either a `,` or an `_` to separate thousands for numbers\n",
    "- An optional period ('.') to separate minwidth and maxchars if both values are defined.  If only one is defined, maxchars is assumed.\n",
    "- An optional maxchars.  If this is a float type, then this specifies the precision.\n",
    "- the type, which values are the same as the old style\n",
    "\n",
    "\n",
    "| Type | Description |\n",
    "|------|:-------|\n",
    "| s   | string |\n",
    "| d   | integer |\n",
    "| x   | hexadecimal integer |\n",
    "| o   | octal integer |\n",
    "| f   | float |\n",
    "| e   | float in exponential format |\n",
    "| g   | decimal or exponential float |\n",
    "\n",
    "\n",
    "[Official Python Documentation for Format Strings](https://docs.python.org/3/library/string.html#formatstrings)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "62866a4b",
   "metadata": {},
   "outputs": [],
   "source": [
    "alphabet = \"abcdefghijklmnopqrstuvwxyz\"\n",
    "print(\"123456789012345678901234567890\")\n",
    "print(\"{}\".format(alphabet))\n",
    "print(\"{:s}\".format(alphabet))\n",
    "print(\"{:X<30s}\".format(alphabet))\n",
    "print(\"{:X>30s}\".format(alphabet))\n",
    "print(\"{:X^30s}\".format(alphabet))\n",
    "print(\"{:30.5s}\".format(alphabet))\n",
    "print(\"{:>30.5s}\".format(alphabet))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bfd46c03",
   "metadata": {},
   "outputs": [],
   "source": [
    "year = 1892\n",
    "print(\"123456789012345678901234567890\")\n",
    "print(\"{:d}\".format(year))\n",
    "print(\"{:20d}\".format(year))\n",
    "print(\"{:+20d}\".format(year))\n",
    "print(\"{:0>20d}\".format(year))  # can't specify minwidth and max chars as we could in the old-style\n",
    "print(\"{:0^8d}\".format(year))\n",
    "print(\"{:^20d}\".format(year))\n",
    "print(\"{:x}\".format(year))\n",
    "print(\"{:o}\".format(year))\n",
    "print(\"{:x^8d}\".format(year))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "dd441095",
   "metadata": {},
   "outputs": [],
   "source": [
    "gain = 64.98712\n",
    "print(\"123456789012345678901234567890\")\n",
    "print(\"{:f}\".format( gain))\n",
    "print(\"{:20f}\".format( gain))\n",
    "print(\"{:+20f}\".format( gain))\n",
    "print(\"{:020f}\".format( gain))\n",
    "print(\"{:20.8f}\".format( gain))\n",
    "print(\"{:20.2f}\".format( gain))\n",
    "print(\"{:<20.6f}\".format( gain))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a08f2965",
   "metadata": {},
   "source": [
    "Notice in the above example, the default justification is left, unless minwidth or the precision is specified where the numbers become right justified.\n",
    "\n",
    "You can use variables in the formatting placeholder by surrounding them with `{}`.  See below where we have used w and d."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e305e744",
   "metadata": {},
   "outputs": [],
   "source": [
    "gain = 64.98712\n",
    "print(\"123456789012345678901234567890\")\n",
    "print(\"{:{w}.{d}f}\".format( gain, w=15,d=2))\n",
    "print(\"{:15.2f}\".format( gain))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bcb91210",
   "metadata": {},
   "source": [
    "## Suggested  LLM Prompts\n",
    "- Explain the concept of new-style string formatting in Python, also known as the \"str.format()\" method. Discuss its advantages over older formatting techniques like %-formatting and string concatenation.\n",
    "- Describe the syntax for using placeholders in new-style string formatting. Provide examples of how to use positional arguments, keyword arguments, and complex field values.\n",
    "- Demonstrate how to format integers and floating-point numbers using the new-style string formatting. Explore options for specifying the minimum field width, alignment, padding characters, precision, and alternative forms (e.g., scientific notation).\n",
    "- Compare and contrast the different methods for string formatting in Python. Provide examples when each method should be used.\n",
    "- Discuss techniques for formatting strings using new-style formatting. Cover truncating long strings, padding with characters, aligning text in fields, and converting case. Provide examples of formatting strings for different purposes, such as generating reports or displaying user input.\n",
    "- Provide examples of how Python's \"new style\" string formatting is used in real-world applications, such as generating reports, formatting log messages, or building user interfaces. Discuss best practices for choosing the appropriate formatting method based on the requirements and readability concerns. Avoid mention of \"f-strings\"."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8a5d845a",
   "metadata": {},
   "source": [
    "## Review Questions\n",
    "1. What is the syntax for using new-style string formatting in Python?\n",
    "2. What is the difference between using positional arguments and keyword arguments in string formatting?\n",
    "3. How do you insert a value into a string using the format() method?\n",
    "\n",
    "[answers](answers/rq-09-answers.md)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3530119c",
   "metadata": {},
   "source": [
    "## Drill\n",
    "For each of the steps with formatting, you should use the new style formatting syntax to produce the correct string for display.\n",
    "1. Define a variable `year` with a value of 2024\n",
    "2. Display the string \"2024 is the current year\".\n",
    "3. Display the string \"xx2024xx\". \n",
    "4. Display the year value as hex.\n",
    "5. Display the string \"0002024\".\n",
    "6. Define a variable `pi` with a value of 3.14159.\n",
    "7. Display the string \"3.1416 is an approxiamate value of pi\".\n",
    "8. Display the string \"xxx3.14xxx\".\n",
    "9. Display the string \"0003.14159\".\n",
    "10. Display the string \"  3.14159000\".\n",
    "\n",
    "[answers](answers/rq-09-answers.md)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "511526ad",
   "metadata": {},
   "source": [
    "## Exercises\n",
    "\n",
    "1. Develop a business format letter with the following specifications:\n",
    "   - It should be in a function `produce_letter` that takes any arguments needed to produce your letter.\n",
    "   - The function should return a formatted string.\n",
    "   - The string should be a multi-line string. \n",
    "   - You should perform at least 5 formatting substitutions.\n",
    "\n",
    "\n",
    "2. Write function format_check_amount(value) that takes a float arguments and then returns a string in the format of \\\\$\\*\\*\\*XX.XX, where the leading asterisks are used to make the length of the output 10 characters and the precision is two digits.\n",
    "\n",
    "\n",
    "3. Write a function with the following declaration - <code>def print_receipt(subtotal, tax_rate, tip_rate)</code><br>That prints the following:\n",
    "```\n",
    "    Subtotal: $     10.00\n",
    "         Tax: $      0.50\n",
    "         Tip: $      2.00\n",
    "               ==========\n",
    "       Total: $     12.50\n",
    "```\n",
    "with the call: `print_receipt(10.00, .05, .2)`\n",
    "<br>\n",
    "Use formatting for all of the numbers and equal signs."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.1"
  },
  "vscode": {
   "interpreter": {
    "hash": "44a9cdcbdccbf05a880e90d2e6fe72470baab4d1b82472d890be0596ed887a6b"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
