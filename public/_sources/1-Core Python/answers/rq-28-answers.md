# Core Python / Dates and Times

1. *What is the difference between the `datetime` and `date` classes in Python's `datetime` module?*
  
   The `datetime` module in Python provides classes for manipulating dates and times. 
   Two of the primary classes in this module are `datetime` and `date`. The `datetime` class is used to work with
   both dates and times. It includes information down to the microsecond and allows for more complex manipulations, 
   including time zone handling. The `date` class is used to work with dates only. It includes year, month, and day, but no time information.


   | Feature       | `datetime` Class                          | `date` Class                  |
   |---------------|-------------------------------------------|-------------------------------|
   | Components    | Year, month, day, hour, minute, second, microsecond, and optional `tzinfo` | Year, month, day             |
   | Time Handling | Includes time information                 | No time information           |
   | Time Zones    | Can handle time zones                     | No time zone support          |
   | Use Case      | When both date and time are needed        | When only date is needed      |
   | Methods       | More methods for time manipulations       | Simpler, focused on date only |

   - Use the `datetime` class when you need to work with both dates and times, or when you need time zone handling.
   - Use the `date` class when you only need to work with dates, which simplifies the code and avoids unnecessary complexity.

2. *How would you create a `datetime` object representing the current date and time?*

   To create a `datetime` object representing the current date and time in Python, 
   you can use the `now()` method of the `datetime` class from the `datetime` module. 
   This method returns a `datetime` object with the current local date and time, but no timezone by default.

   ```python
   from datetime import datetime 

   current_datetime = datetime.now()
   print(current_datetime)  
   ```
   Output:
   ```
   2024-06-05 16:01:04.269456
   ```

   If you need the current date and time with time zone awareness, you can use the `datetime.now()` 
   method with the `timezone` argument from the `datetime` module.

   ```python
   from datetime import datetime, timezone

   current_datetime_utc = datetime.now(timezone.utc)
   print(current_datetime_utc)  # This will print the current date and time in UTC
   ```
   Output:
   ```
   2024-06-06 00:02:55.079759+00:00
   ```
   Question: In what timezone was the author when this part was written?

3. *Explain the concept of time zones in Python's `datetime` module and how to handle them.*

   Time zones in Python's `datetime` module are managed using the `timezone` class and related functions. 
   Handling time zones correctly is essential for working with dates and times in applications that operate across multiple regions.

   To create aware `datetime` objects, you use the `timezone` class along with `timedelta` to specify the time zone offset.

   ```python
   from datetime import datetime, timezone, timedelta

   # Define a time zone with a UTC offset of +2 hours
   tzinfo = timezone(timedelta(hours=2))

   # Create an aware datetime object with the specified time zone
   aware_datetime = datetime(2023, 6, 5, 14, 30, 45, tzinfo=tzinfo)

   print(aware_datetime)  # Output: 2023-06-05 14:30:45+02:00
   ```

   To get the current time in a specific time zone, you can use the `datetime.now()` method with the `timezone` argument. 
   

   ```python
   from datetime import datetime, timezone, timedelta

   # Define a time zone with a UTC offset of +2 hours
   tzinfo = timezone(timedelta(hours=2))

   # Get the current time in the specified time zone
   current_aware_datetime = datetime.now(tzinfo)

   print(current_aware_datetime)
   ```

   To convert between time zones, you can use the `astimezone()` method of a `datetime` object.

   ```python
   from datetime import datetime, timezone, timedelta

   # Define two time zones
   tz_utc = timezone.utc
   tz_pacific = timezone(timedelta(hours=-8))

   # Create an aware datetime object in UTC
   utc_datetime = datetime(2023, 6, 5, 14, 30, 45, tzinfo=tz_utc)

   # Convert the UTC datetime to Pacific time
   pacific_datetime = utc_datetime.astimezone(tz_pacific)

   print(utc_datetime)       # Output: 2023-06-05 14:30:45+00:00
   print(pacific_datetime)   # Output: 2023-06-05 06:30:45-08:00
   ```

   As you can see, the `timezone` class lacks some expressiveness.  As a result, the third-party library,
   `pytz` was developed to overcome the limitations. 
   - robust library that supports all modern time zones, including those with DST transitions and historical time zone data.
   - provides a large database of time zones, including their past and future transitions.
   - provides a tzinfo implementation that can be used with Python's datetime objects to create time zone-aware datetime instances.

   ```python
   from datetime import datetime
   import pytz

   # Define time zones using pytz
   utc = pytz.utc
   eastern = pytz.timezone('US/Eastern')
   pacific = pytz.timezone('US/Pacific')

   # Get the current time in UTC
   now_utc = datetime.now(utc)

   # Convert UTC time to Eastern and Pacific time
   now_eastern = now_utc.astimezone(eastern)
   now_pacific = now_utc.astimezone(pacific)

   print(now_utc)      # Output: 2023-06-05 14:30:45+00:00
   print(now_eastern)  # Output: 2023-06-05 10:30:45-04:00
   print(now_pacific)  # Output: 2023-06-05 07:30:45-07:00
   ```

4. *Explain the difference between aware and naive `datetime` objects in Python.*

   - *Naive datetime objects*: Do not contain any time zone information. They are treated as "local time" but lack context about what "local" means.
   - *Aware datetime objects*: Contain time zone information, making them unambiguous about the specific moment in time they represent.

   Generally speaking, you should strive to use aware `datetime` objects.

5. *Explain the difference between the `strftime()` and `strptime()` methods in the `datetime` module.*

   The `strftime()` formats `datetime` objects into strings while `strptime()` parses strings into `datetime` objects. Both methods
   take a *format* string that consists of directives corresponding to various date and time components.

   ```python
   from datetime import datetime

   # format a string
   dt = datetime(2023, 6, 5, 14, 30, 45)
   formatted_string = dt.strftime('%Y-%m-%d %H:%M:%S')
   print(formatted_string)  # Output: '2023-06-05 14:30:45'

   # Parse a string into a datetime object
   date_string = '2023-06-05 14:30:45'
   dt = datetime.strptime(date_string, '%Y-%m-%d %H:%M:%S')
   print(dt)  # Output: 2023-06-05 14:30:45
   ```

   [Formatting Codes](https://docs.python.org/3/library/datetime.html#strftime-and-strptime-format-codes)

6. *How would you calculate the number of days between two `date` objects?*

   To calculate the number of days between two `date` objects, subtract one `date` object from the other. 
   This will give you a `timedelta` object representing the difference in days between the two dates. 
   Then, access the `days` attribute of the `timedelta` object to get the total number of days.

   ```python
   from datetime import date

   # Define two date objects
   date1 = date(2023, 6, 5)
   date2 = date(2023, 6, 15)

   difference = date2 - date1
   num_days = difference.days

   print(num_days)  # Output: 10
   ```

7. *What is the purpose of the `pendulum` library in Python, and how does it differ from the built-in `datetime` module?*

   The `pendulum` library in Python is a third-party library that provides enhanced date and time functionality compared 
   to the built-in `datetime` module. Its purpose is to offer more features, better handling of time zones, and 
   simpler syntax for working with dates and times. `pendulum` is a "drop-in" replacement for `datetime` in that its
   class extend the `datetime` classes.

   1. **Enhanced Features:** `pendulum` provides additional features not available in the built-in `datetime` module, 
      such as better support for time zones, localization, and manipulation of dates and times.
   2. **Improved Time Zone Handling:** `pendulum` offers improved time zone support, including automatic time zone 
      detection and conversion, support for time zone database updates, and easier handling of time zone offsets and transitions.
   3. **Simpler Syntax:** `pendulum` aims to provide a simpler and more intuitive syntax for common date and time operations,
      making it easier for developers to work with dates and times in their applications.


   ```python
   from datetime import datetime, timedelta, timezone

   utc_now = datetime.utcnow()
   eastern_time = utc_now.astimezone(timezone(timedelta(hours=-5)))
   ```

   ```python
   import pendulum

   utc_now = pendulum.now('UTC')
   eastern_time = utc_now.in_timezone('America/New_York')
   ```

8. *Compare and contrast the different representations of a date and time value by using a Unix Epoch value versus ISO 8601?*

   Both Unix Epoch time and ISO 8601 are standards used to represent date and time values, but they differ in their formats and granularity.

   Unix Epoch time is represented as the number of seconds that have elapsed since the Unix Epoch, which is midnight on January 1, 1970, UTC. 
   Alternate versions are expressed in milliseconds or microseconds.

   The Unix Epoch time for January 1, 2023, at midnight would be `1672531200`. Unix Epoch time is commonly used in computer systems, 
   especially in Unix-like operating systems, as it provides a simple and consistent way to represent time.

   ISO 8601 is an international standard for representing date and time values in a human-readable format. 
   It follows the format `YYYY-MM-DDTHH:MM:SS±hh:mm`, where `T` separates the date and time components, and `±hh:mm` 
   represents the time zone offset from UTC. ISO-8601 can represent time with sub-second precision, including 
   milliseconds, microseconds, or even nanoseconds, depending on the specific implementation. ISO 8601 is widely used in various contexts, including communication between systems, data interchange, and human-readable displays of date and time values.
