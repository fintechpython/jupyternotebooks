# Core Python / Introduction
1. *How do problem-solving skills, domain knowledge, and programming skills combine together for effective programming?  What does each bring?*

   Domain knowledge is critical to understand what is needed and how to proceed to solve a given problem.  Problem-solving skills allow us
   to break the problem down into the necessary steps.  Programming skills allow us to turn those steps into code.

2. *Besides your laptop and smartphone, where is software present in your daily life? Where is software not present?*

   Software can be embedded into almost everything now (e.g., [internet of things](https://en.wikipedia.org/wiki/Internet_of_things)).
   Even items not directly using software most likely have software involved in the production at some point.

3. *Identify the commonalities observed among the three programs.*

   They all have some version of user input (although the traveling saleperson was hard coded), processing, and then output.
   Each used existing libraries for additional functionality.

4. *Explain the importance of abstraction and encapsulation in programming, as discussed in the text. Provide examples to illustrate how these principles are applied in real-life programming scenarios.*
   
   With abstraction, we look to find the core aspects of that something that needs to be the model.  
   These aspects can include both data (information about something) and behavior (things that 
   something can do).  It's impossible to fully model any complex object/activity - thus we
   need to decide upon those parts that matter. Encapsulation allows to group that data and behavior together.

   If we model a stock market/exchange, what are some of the key things that need to be tracked?  For a trade,
   we would be concerned about when it occurred, which stock, number of shares the price, the buyer, the seller.

5. *Which is a key aspect of programming?*

   Understanding what needs to occur.  This involves figuring out the different parts of the task to accomplish and breaking those
   down into appropriate steps.

6. *What is the primary purpose of abstraction in programming?*

   To simplify a concept by distilling it to its fundamental parts

7. *What is the primary definition of a program?*

   A series of instructions designed to automate tasks
