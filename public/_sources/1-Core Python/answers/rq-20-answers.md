# Core Python / Recursion

1. *What is recursion, and how does it differ from iteration?*
   
   Recursion is a programming technique where a function calls itself to solve a problem. This self-referential approach involves breaking down a problem into smaller, more manageable subproblems that resemble the original problem.

   Iteration uses looping constructs to repeat a block of code until a condition is met. 
   
   Recursion typically involves multiple stack frames and can be more intuitive for problems defined in terms of themselves, whereas iteration maintains a single stack frame and is often more straightforward and efficient for repetitive tasks.

2. *What are the two essential components of a recursive function?*

   - Base Case
   - Recursive Case

3. *Explain the concept of a base case in a recursive function.*

   The base case is a condition that stops the recursion from continuing indefinitely. It defines the simplest or smallest instance of the problem that can be solved directly without further recursion. When the base case is reached, the function returns a value or performs a specific action, effectively terminating the recursive calls.

4. *What is the role of the recursive case in a recursive function?*

   The recursive case is the part of the function that defines the problem in terms of a smaller or simpler version of itself. It breaks down the problem into smaller subproblems, and calls itself with these smaller subproblems as arguments. The recursive case typically involves one or more recursive calls to the same function, with different arguments that represent the subproblems.

5. *How would you go about showing that your recursive algorithm always terminates?*

   Two conditions need to be met:
   - Show that every recursive call passes an argument which is "smaller than" the argument received by the current call under the ordering you have chosen.
   - Show that you have a base case in your algorithm.

6. *Explain indirect recursion.*

   Indirect recursion occurs when a function calls another function, which in turn calls the original function, creating a recursive loop. This form of recursion involves multiple functions calling each other in a circular manner, rather than a single function calling itself directly.

7. *What is the purpose of a wrapper function?*

   A wrapper function simplifies a recursive function's interface, typically providing a more user-friendly entry point.  The function can:
   - validate parameters
   - initialize parameters to the recursive function
   - Call a recursive function
   - handling errors

8. *Explain the difference between head and tail recursion.*

   Head recursion occurs when a recursive function makes its recursive call at the beginning, processing the recursive case before handling any operations after the call. In contrast, tail recursion makes the recursive call at the end, with no further operations needed after the call, allowing for potential optimizations like tail call optimization (TCO) to reuse the current function's stack frame, making it more memory efficient.
   Python does not implement TCO.

9. *Explain the difference between numerical and structural recursion. Provide an example of each.*

    Numerical recursion involves functions that operate primarily on numeric values, usually performing operations like incrementing or decrementing a number to progress towards a base case. The recursion typically involves arithmetic calculations and is often used for problems like factorial calculation or Fibonacci sequence generation.

    ```python
    def factorial(n):
        if n == 0:  # Base case
            return 1
        else:
            return n * factorial(n - 1)  # Recursive case

    print(factorial(5))  # Output: 120
    ```

    Structural recursion involves functions that operate on data structures such as lists, trees, or other recursive data structures. These functions typically decompose the structure into smaller parts and recursively process these parts. Structural recursion is often used for tasks like traversing trees or processing linked lists.

    ```python
    def sum_list(lst):
        if not lst:  # Base case: empty list
            return 0
        else:
            return lst[0] + sum_list(lst[1:])  # Recursive case: head + sum of the tail

    print(sum_list([1, 2, 3, 4]))  # Output: 10
    ```

10. *Explain three possible risks or disadvantages of recursion.*
    
    Recursive functions, while powerful and often elegant, come with several risks and disadvantages that developers need to consider:
    1. *Stack Overflow*: Each recursive call adds a new frame to the call stack. If the recursion depth becomes too large, it can exceed the stack limit, leading to a stack overflow error.
    2. *Performance Overhead*: Recursive function calls involve overhead due to maintaining multiple call stack frames, which can be significantly more expensive than iterative solutions. 
    3. *Memory Consumption*: Each recursive call consumes stack memory, which can quickly add up, especially if the base case is far from the initial call. High memory consumption can be problematic in environments with limited resources or when processing large datasets.
    4. *Complexity in Understanding and Debugging*: Recursive logic can be harder to follow, particularly for those new to the concept or when dealing with complex recursion. This can make the code harder to understand, maintain, and debug, especially when compared to more straightforward iterative solutions.
