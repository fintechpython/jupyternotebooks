# Core Python / Booleans, Numbers, and Operations

1. *What are the exact literal values of the Boolean data type?*

   `True` and `False`.  Note the capitalization and lack of quotes.

2. *How do you convert other data types to Boolean?*

   Use the built-in function, `bool()`.  Any zero / empty value is `False`. Any other value is considered `True`.

3. *What is the difference between true division and floor division?*

   True division `/` always returns a floating-point number, even if the result is an integer. It calculates the quotient and includes any remainder as a fractional part. Floor division `//` truncates any fractional part of the result. The resulting data type depends upon the 
   [operands](https://en.wikipedia.org/wiki/Operand).

4. *What is the data type of the following expressions: *
   - `5 / 2`: float
   - `5 // 2`: int
   - `5 / 2.5`: float
   - `7 // 2.5`: float

5. *Explain the precedence rules in Python. Provide an example with multiple operators.*
   
   Python follows the "PEMDAS" precedence rules.  Expressions within parenthesis are evaluated first, followed by exponents, then multiplication or division,
   and then, finally, addition or subtraction. Use parenthesis to avoid any possible confusion. Programming is not a contest to fool others.

   For the expression, `(5+5) * 3 / 6`, `5+5` is evaluated first to `10`. Then `10` is multiplied by `3`. Finally, `30` is divided by `6` to produce 5.0, which
   is a float data type for the result.

6. *What are the minimum and maximum values for integers in Python? For Floats?*

   Theoretically, integers in Python 3 do not have minimum or maximum values.

   You can find the min and values for floats by executing
   ```python
   import sys
   sys.float_info
   ```
   See the min and max attributes below.
   ```
   sys.float_info(max=1.7976931348623157e+308, max_exp=1024, max_10_exp=308, min=2.2250738585072014e-308, min_exp=-1021, min_10_exp=-307, dig=15, mant_dig=53, epsilon=2.220446049250313e-16, radix=2, rounds=1)
   ```

7. *What error occurs if you try to convert the literal value `"Python"` to an integer?*

   A `ValueError` occurs:
   ```
   ValueError: invalid literal for int() with base 10: 'Python'
   ```
8. *Fragments of code that calculate new values are called:*

   expressions
9. *What does "hex" mean?*

   Base 16

10. *Which of the following are represented numerically?*

    Ultimately, all data is represented by numbers.

11. *What is the type of this expression? `5 + 2 + 34 + 4/2`*

    float due to the division

12. *What is the type of this expression? `5+ 8//2`*

    int