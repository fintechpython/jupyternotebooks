# Core Python / Validation, Exceptions and Error Handling


1. *What is input validation, and why is it important in software?*

   Input validation is the process of verifying that the data provided by users or external sources meets
   the expected format, type, and constraints before it is processed by a software application. Input validation helps
   prevent errors, security vulnerabilities (such as SQL injection and cross-site scripting), and ensures the application 
   functions as intended. Proper input validation safeguards the system against malicious or unintended inputs that could 
   compromise data integrity and system stability.

2. *What is the difference between syntax errors, runtime errors, and logical errors? Provide examples of each.*

   - *Syntax Errors*: These occur when the Python interpreter encounters code that doesn't conform to the language's syntax rules. They are detected during the parsing phase, before execution starts.
     ```python
     if True print("Hello")
     ```
     This code will raise a `SyntaxError` because the syntax for the if statement is incorrect (missing a colon).

     In compiled languages such as C, C++, and Java, the compiler detects and reports upon these errors.  

   - *Runtime Errors*: These occur during the execution of a program. They are typically caused by operations that are not 
     possible to perform, such as dividing by zero or accessing a non-existent variable.
     ```python
     x = 10 / 0
     ```
     This code will raise a `ZeroDivisionError` at runtime because dividing by zero is not allowed.

   - *Logical Errors*: These occur when the code runs without crashing, but the output or behavior is incorrect due to flaws in the program's logic. 
     These are often the most difficult to detect and debug.
     ```python
     def calculate_area(length, width):
         return length + width

     area = calculate_area(5, 10)
     print(area)
     ```
     This code runs without errors but contains a logical error. The function calculate_area incorrectly adds the length and width instead of multiplying them to find the area.

3. *What is the purpose of try-except blocks or similar constructs in programming languages?*

   The purpose of try-except blocks (or similar constructs) in programming languages is to handle exceptions and errors gracefully during the execution of a program. These constructs allow developers to:
   1. *Catch and Manage Errors*: They enable the program to catch errors or exceptions that occur in a specific block of code and handle them without crashing the entire application. This allows for the implementation of custom error-handling logic. Developers can choose between handling the error immediately or propagating to a caller.
   2. *Maintain Program Flow*: By catching exceptions, try-except blocks help ensure that the program can continue running or terminate gracefully, even if an error occurs. This is essential for creating robust and user-friendly applications.
   3. *Debugging and Logging*: These constructs allow for the logging of error details, which can be useful for debugging and diagnosing issues. They can also be used to provide meaningful error messages to users.
   ```python
   try:
       result = 10 / 0
   except ZeroDivisionError as e:
       print(f"Error occurred: {e}")
       result = None
   print("Program continues running.")
   ```
   In this example, the try block contains code that may raise a ZeroDivisionError. When this exception occurs, the except block catches it, handles the error by printing a message, and assigns `None` to the result, allowing the program to continue running smoothly.

4. *Discuss the importance of providing clear and informative error messages to users and developers.*

   Providing clear and informative error messages to users and developers is crucial for several reasons:
   1. *User Experience*: For end-users, clear error messages enhance the user experience by helping them understand what went wrong and how to resolve the issue. Vague or technical error messages can frustrate users, leading to a poor user experience and potentially driving them away from the product.
   2. *Effective Troubleshooting*: For developers, informative error messages are vital for diagnosing and fixing issues efficiently. Detailed error messages that specify the nature of the error, the context in which it occurred, and possible causes or solutions can significantly reduce debugging time and effort.
   3. *System Security*: While detailed error messages are helpful, they should be crafted carefully to avoid revealing sensitive information that could be exploited by attackers. Properly designed error messages can inform users and developers without compromising security.
   4. *Error Logging*: Clear error messages are also beneficial for logging purposes. Well-logged errors can provide insights into recurring issues, system health, and user behavior patterns, which are invaluable for maintaining and improving the software.

   Example:
   - *Poor Error Message*: "Error 1234"
   - *Clear Error Message*: "Error: Unable to connect to the database. Please check your network connection and try again. If the problem persists, contact support with error code DB_CONN_ERR_01."

   The latter message clearly states the problem, offers a potential solution, and provides a specific code for further assistance, thereby improving both the user and developer experience.

5. *What is error propagation, and when might it be appropriate to propagate errors up the call stack? How does this work with exceptions?*

   Error propagation is the process of passing an error from the point where it occurs up the call stack to higher 
   levels of the application. This allows the error to be handled by the appropriate level of the application, 
   often where more context is available to determine the best course of action.

   **When to Propagate Errors**:
   - *Lack of Context*: When the current function or method doesn't have enough context (i.e., lacks information) to handle the error properly. 
     Higher-level functions might have more information or resources to handle the error effectively.
   - *Centralized Error Handling*: When an application uses a centralized error handling mechanism to log errors, 
     clean up resources, or notify users in a consistent manner.
   - *Resource Management*: When specific resources need to be released or rolled back in response to an error, 
     propagating the error up can ensure these actions are taken at a level that manages such resources.

   **How Error Propagation Works with Exceptions**:
   
   In many programming languages, exceptions are used to propagate errors up the call stack. When an exception is thrown (or raised), 
   it can be caught by an exception handler at a higher level in the call stack. If not caught, the exception continues to 
   propagate up the stack until it is handled or the program terminates.
   ```python
   def read_file(file_path):
       try:
           with open(file_path, 'r') as file:
               return file.read()
       except FileNotFoundError as e:
           print(f"Error: {e}")
           raise  # Propagate the error

   def process_file(file_path):
       try:
           content = read_file(file_path)
           print(content)
       except FileNotFoundError:
           print("File not found. Please provide a valid file path.")
           # Handle the error or further propagate it if necessary

   process_file('nonexistent_file.txt')
   ```

   Explanation:
   - read_file Function: Tries to open and read a file. If the file is not found, it catches the `FileNotFoundError` and logs an error message, then re-raises the exception to propagate it up the stack.
   - process_file Function: Calls read_file and catches the propagated FileNotFoundError. It then provides a user-friendly message or takes further action.

   This approach ensures that errors are handled at the appropriate level, with each level having the opportunity to add context or take specific actions
   before potentially passing the error further up the stack.

6. *Explain the role of assertions in software development, and how they can be used to validate program state and enforce invariants.*

   Assertions play a crucial role in software development by providing a way to check assumptions made by the code and validate the 
   program's state during execution. They are primarily used as a debugging aid and to enforce invariants, which are conditions that 
   should always hold true during the execution of a program. 
   - *Debugging*: Assertions help identify bugs by verifying that the code is working as expected at specific points. If an assertion fails, it raises an error, indicating that there's a bug in the code.
   - *Documentation*: They serve as a form of documentation for developers, showing the intended behavior and expected state of the program at various stages.
   - *Enforcing Invariants*: Invariants are conditions that must always be true during the execution of a program. Assertions can be used to enforce these conditions, 
     ensuring that the program state remains valid.
   - *Error Detection*: By catching conditions that should never happen, assertions can help detect errors early in the development process, making it easier to diagnose and fix issues.

7. *Differentiate between preconditions, postconditions, and invariants in the context of assertions.*

   In the context of assertions, preconditions, postconditions, and invariants are types of conditions used to ensure the correctness of a program at various stages of execution. Here’s a detailed differentiation:

   *Preconditions* are conditions that must be true before a function or method starts executing. They define the 
   state or conditions that must be met for the function to execute correctly. Preconditions ensure that the function 
   is called with valid arguments and that the program state is appropriate for the function's operation.
   ```python
   def calculate_square_root(x):
       assert x >= 0, "Precondition: x must be non-negative"
       return x ** 0.5

   # Usage
   result = calculate_square_root(9)  # Valid input
   result = calculate_square_root(-1)  # This will raise an AssertionError
   ```

   In this example, the *precondition* is that x must be non-negative before calculating the square root.

   *Postconditions are conditions that must be true after a function or method has finished executing. They define the 
   expected state or result after the function's execution. 
   ```python
   def add(a, b):
       result = a + b
       assert result >= a and result >= b, "Postcondition: result should be at least as large as both inputs"
       return result

   # Usage
   sum_result = add(3, 4)  # Valid operation
   ```
   In this example, the postcondition checks that the result of the addition is at least as large as both input values, ensuring the correctness of the addition.

   *Invariants* are conditions that must always be true during the entire lifecycle of an object or throughout the execution of a program. 
   They are often used to ensure the consistency and correctness of data structures.

Purpose: Invariants maintain the integrity of the program state and ensure that certain conditions are preserved throughout execution, especially after any modification.
```python
class BankAccount:
    def __init__(self, balance):
        self.balance = balance
        assert self.balance >= 0, "Invariant: balance must be non-negative"

    def deposit(self, amount):
        assert amount > 0, "Precondition: deposit amount must be positive"
        self.balance += amount
        self._check_invariants()

    def withdraw(self, amount):
        assert amount > 0, "Precondition: withdrawal amount must be positive"
        assert amount <= self.balance, "Precondition: insufficient funds"
        self.balance -= amount
        self._check_invariants()

    def _check_invariants(self):
        assert self.balance >= 0, "Invariant: balance must be non-negative"

# Usage
account = BankAccount(100)
account.deposit(50)  # Valid operation
account.withdraw(200)  # This will raise an AssertionError due to precondition
```
In this example, the invariant is that the balance must always be non-negative. This invariant is checked after every modification to ensure the integrity of the BankAccount object.

Note: These examples use assertions to validate parameters. This may or may not be the most appropriate choice.

8. *What is the WORST possible way to deal with an error?*
   - Print an error message: while this is important, you may need additional processing/logic to correct the issue, including possible input from the user.
   - Retry the operation: depending upon the cause of an error (e.g., network failure), this may be sufficient. Otherwise, one definition of insanity is 
     doing the same thing over and over and expecting different results.
   - Crash the program: for an academic classwork, this can suffice. However, in real-world systems, simply stopping the program lacks sufficient robustness
   - Fail silently: this is the worst way as no clue exists as to what occurred.

9. Reliable software requires -
   - error prevention - yes.  Systems need to prevent error by validating inputs and program state
   - error detection - yes. When issues do arise, they need to be detected so they can then be handled appropriately
   - error recovery - yes. The program needs to return to a good "state" or otherwise halt.
   - defect-free code - not really. While we strive for defect-free code, it's not necessarily possible.
   - assert statements - no. While `assert` statements can detect issues, they are not the only means of doing so.

10. Should input validation be provided for internal sources (e.g., a file that was previously saved by the system)? Why or why not?

    1. *Data Integrity*: Even though the data originated from the system itself, the possibility exists of tampering (intentional or accidental).
       Input validation prevents potential issues caused by corrupted or malformed data.
    2. *Security Considerations*: Internal data sources can be a potential attack vector for malicious actors. 
       Without proper input validation, an attacker could potentially modify or inject malicious data into these internal sources.
    3. *Consistency and Robustness*: By consistently validating all input sources, including internal ones, 
       the system maintains a consistent approach to data handling and input validation.

    For data sources, such as a database system, where that source provides some integrity constraints and is highly trusted, input validation
    may be unnecessary overhead.

11. How do you initiate a specific exception in Python? Provide a code example.

    You can initiate a specific exception using the raise statement. This allows you to create and throw an exception, typically when a particular 
    error condition or unusual situation is encountered in your code. You can raise both built-in exceptions and custom exceptions.

    **Raising a Built-in Exception:** Suppose you want to raise a ValueError if a function receives an invalid argument:
    ```python
    def calculate_square_root(x):
        if x < 0:
            raise ValueError("Cannot calculate the square root of a negative number")
        return x ** 0.5

    # Usage
    try:
        result = calculate_square_root(-1)
    except ValueError as e:
        print(f"Error: {e}")
    ```
    The calculate_square_root function raises a `ValueError` if the input `x` is negative. The try block catches this exception and prints an error message.

    **Raising a Custom Exception:** You can also define and raise your own custom exceptions. 

    Define a Custom Exception Class:
    ```python
    class NegativeNumberError(Exception):
        """Exception raised for errors in the input, if the number is negative."""
        def __init__(self, value):
            self.value = value
            self.message = f"NegativeNumberError: {value} is not a valid input. Input must be non-negative."
            super().__init__(self.message)
    ```
    Raise the Custom Exception:
    ```python
    def calculate_square_root(x):
        if x < 0:
            raise NegativeNumberError(x)
        return x ** 0.5

    # Usage
    try:
        result = calculate_square_root(-1)
    except NegativeNumberError as e:
        print(f"Error: {e}")
    ```
    A custom exception `NegativeNumberError` is defined, inheriting from Python's base `Exception` class.
    The `calculate_square_root` function raises this custom exception if the input `x` is negative.
    The try block catches the `NegativeNumberError` and prints the error message, which includes the invalid input value.
    
    Using specific exceptions helps make your error handling more precise and meaningful, improving the robustness and maintainability of your code.

12. In what order does that call stack appear in a Traceback?

    The call stack in a traceback appears in a "bottom-up" order. This means that the traceback starts with the most recent call (where the exception was raised) and goes backwards through the stack to the original call.

13. Should assertions be used to validate input parameters?  Discuss the pros and cons of this approach.

    While input should also be validated - especially from outside of a [trust boundary](https://en.wikipedia.org/wiki/Trust_boundary), 
    `assert` statements are not the most appropriate choice:
    - Assertions may be turned off (perhaps inadvertently) by setting `__debug__ == False` or using the `-O` or using the `-OO`.
    - Assertions in Python raise `AssertionError` when a condition fails, which may not be the most appropriate or 
      informative error for all input validation scenarios. Custom exceptions or error messages may provide more context and flexibility for error handling
    - Assertions should be used for internal consistency checks and invariant conditions, not for general input validation. 
      Overusing assertions for input validation can make the code harder to read and maintain.

14. For multiple `except` blocks under a single `try`, in what order should the blocks be listed?

    When multiple `except` blocks are used under a single `try` block to handle different types of exceptions, 
    they should be listed in the order from most specific to least specific. This ensures that exceptions are 
    caught and handled correctly, with more specific exceptions being caught before more general ones.