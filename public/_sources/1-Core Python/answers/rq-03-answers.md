# Core Python / Data: Types, Values, Variables, and Names

1. *For each of the following literal values, what is the corresponding type?*
   <ol type="a">
     <li> 1000: int
     <li> -42: int
     <li> 4.2: float
     <li> 'a': str
     <li> "alpha": str
   </ol>
   Note: Python does not contain a data type to represent a single character

2. *Explain the naming rules for variables.*

   As you name a variable, you should create names that represent the purpose of that variable. The variable name must also follow these rules:
   - can only contain lowercase letters (a-z), uppercase letters (A-Z), digits (0-9), and an underscore(_)
   - cannot begin with a digit
   - cannot be one of Python's reserved keywords.

   Variable names are case-sensitive.

3. *What guidelines should you follow in naming variables?*

   Use informative names for variables to assist yourself and others in reading your code.

4. *What do all objects contain?*

   - All objects maintain some sort of *state* - the data they object tracks.  If there is no state present, then its really just a collection of functions.
   - Objects all have *behavior* - what functions/messages can be called.  Without behavior, an object is simply a data structure.
   - Objects all contain an *identity* - something to uniquely distinguish an object. The identity may be part of an object's state, 
     but can also be just a reference or an interpreter-determined unique ID.

5. *What built-in function displays the value of variables or literals on the screen (console)?*

   `print`

6. *Within the Python interpreter, how can we get more information about a particular keyword?*

   Use the built-in function, [`help`](https://docs.python.org/3/library/functions.html#help). Pass the keyword name as an argument to `help()`.


7. *In Python, how does a variable determine its type?  Can this type ever change?*

   The interpreter automatically determines the type of a variable based on the result of an 
   [expression](https://docs.python.org/3/reference/expressions.html) on the right-hand side of an assignment state.

   As Python is a dynamically typed language, the type associated with each variable can change based
   on the result of the right-hand expression in an assignment statement.

8. *Are variable names case-sensitive?*

   Yes, variables are case-sensitive. However, you should avoid using variables that only differ in case.  Avoiding similar names is also a good practice.
   Confusion only leads to problems.
