# Core Python / Dictionaries

1. *What is a dictionary in Python, and how is it different from other data structures like lists and tuples?*

   A dictionary is a data structure that stores values indexed by unique keys. Generally speaking, you should
   treat dictionaries as an unordered collection. In Python versions greater than 3.7, the entries are ordered
   based on their insertion order.

2. *How do you create an empty dictionary in Python?*

   You can create an empty dictionary with the `{ }` literal or the `dict()` with no arguments.

3. *What are the two main components of a dictionary in Python? Explain with an example.*

   The two main components are a *key* that has an associated *value*.

   ```python
   adict = { "university":"Duke", "major":"Financial Technology"}
   adict["university"]
   'Duke'
   ```

4. *How do you access the value associated with a specific key in a dictionary?*

   Use the `[]` operator with the key object between the brackets. You
   can also use the `get()` function.

5. *What happens if you try to access a key that doesn't exist in a dictionary?*

   Using the `[]` operator, a `KeyError` occurs if the value does not exist.

   With `get()`, `None` is returned if the value does not exist. You can provide a
   default value that will be returned if the key does not exist.
   ```python
   adict = { "university":"Duke", "major":"Financial Technology"}
   label = adict.get("instructor","unknown")
   print(label)    # display "unknown" to the console
   ```

6. *How do you add a new key-value pair to an existing dictionary?*

   Use the `[]` operator with the assignment operator `=`.
   ```python
   adict = { "university":"Duke", "major":"Financial Technology"}
   adict["instructor"] = "Slankas"
   instr = adict.get("instructor","unknown")
   print(instr)    # display "Slankas" to the console
   ```

7. *How do you remove a key-value pair from a dictionary?*

   Use the `del` statement (keyword) with the correct key.

   ```python
   del adict["instructor"]
   adict["instructor"]     # Raises a KeyError
   help("del")             # Displays the help text associated with the del statement
   ```

   To remove all entries from a dictionary, use the `clear()`.  You can also just assign an empty dictionary to the variable.

8. *How do you check if a specific key exists in a dictionary?*

   Use the `in` operator.
   ```python
   adict = { "university":"Duke", "major":"Financial Technology"}
   "university" in adict      # evaluates to True
   "instructor" in adict      # evaluates to False
   ```   

9. *How do you get a list of all keys or all values in a dictionary?*

   Use the functions `keys()` and `values()`. Both of these functions return an iterator which we can then iterate over
   using a `for` loop. You can also immediately convert the iterator to a list. 

   ```python
   adict = { "university":"Duke", "major":"Financial Technology"}
   for v in adict.values():      # prints "Duke" and "Financial Technology" on separate lines
       print(v)
   key_list = list(adict.keys())
   print(key_list)               # displays ['university', 'major'] to the console
   ```

10. *What function returns the number of elements in a dictionary?*

    `len()`   Note: This is the same function for strings, lists, and tuples.

11. *For a given key, how many entries may exist for that key in a Python dictionary? What happens if you try to add a duplicate key to a dictionary?*

    Just one. If multiple values are needed, then the value needs to be a container (e.g., list) that can hold the multiple values.
    If try to add a duplicate key, the existing entry is replaced.

12. *What are the requirements(limitations) for a dictionary key?*

    Keys can be any Python object (remember, everything in Python is an object) that is immutable.

13. *Are dictionary entries unique?*

    No.  Only keys are guaranteed to be unique.

14. *How do you iterate over key-value pairs in a dictionary?*

    Use the `items()` function. The returns a tuple containing the key and value.  We can also use tuple unpacking to
    directly assign the tuple entries to variables.
   
    ```python
    adict = { "university":"Duke", "major":"Financial Technology"}
    
    for item in adict.items():
        print("Key: {}, Name: {}".format(item[0], item[1]))

    for key, value in adict.items():
        print("{}: {}".format(key, value))
    ```

15. *How do you sort a dictionary by keys or values in Python?*

    By keys:
    ```python
    stock_prices = {'PG': 141.96, 'AXP': 154.42, 'AAPL': 137.13,'CSCO':43.49, 'HD':289.24, 'AMZN': 2538.23}
    for key in sorted(stock_prices):
        print("{}: Stock price: ${:,.2f}".format(key,stock_prices[key]))
    ```

    By values:
    ```python
    stock_prices = {'PG': 141.96, 'AXP': 154.42, 'AAPL': 137.13,'CSCO':43.49, 'HD':289.24, 'AMZN': 2538.23}
    for key in sorted(stock_prices.values):
        print("{}: Stock price: ${:,.2f}".format(key,stock_prices[key]))
    ```

    See the associated notebook (page) to reverse the sorted order or to provide a custom comparator.

16. *How do you copy a dictionary in Python?*

    To copy a dictionary to a new object, use the `copy()` method, which creates a shallow copy that copies reference values, but not necessarily the objects to which those references point. 
    ```python
    s1 = {'AXP': 'American Express', 'AMGN': 'Amgen', 'AAPL': 'Apple'}
    s2 = s1.copy()
    s2['IBM'] = "International Business Machines"
    print(s1)   # displays {'AXP': 'American Express', 'AMGN': 'Amgen', 'AAPL': 'Apple'}
    print(s2)   # displays {'AXP': 'American Express', 'AMGN': 'Amgen', 'AAPL': 'Apple', 'IBM': 'International Business Machines'}
    s1.clear()
    print(s1)   # displays {}
    print(s2)   # displays {'AXP': 'American Express', 'AMGN': 'Amgen', 'AAPL': 'Apple', 'IBM': 'International Business Machines'}
    ```

17. *What is the get() method in Python dictionaries, and how is it different from indexing with square brackets?*

    The `get()` retrieves values from dictionaries. Rather than raising a `KeyError` if a key does not exist,
    `None` is returned. An optional argument can also be specified to return a default value if a key does not exist.

18. *Can you have a dictionary as a value in another dictionary? If so, how would you access the nested dictionary?*

    Yes. This occurs quite frequently.  Use repeating square bracket operators to access the nested dictionary.

    ```python
    company = {"symbol": "IBM", "name": "International Business Machines", "year_founded": 2140}
    companies = { "IBM": company}
    companies   # evalutes to {'IBM': {'symbol': 'IBM', 'name': 'International Business Machines', 'year_founded': 2140}}
    companies["IBM"]["name"]  # returns 'International Business Machines'
    companies["IBM"]          # returns {'symbol': 'IBM', 'name': 'International Business Machines', 'year_founded': 2140}
    ```
