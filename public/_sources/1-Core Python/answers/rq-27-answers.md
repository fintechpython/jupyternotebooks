# Core Python / Comprehensions

1. *How does a list comprehension differ from a traditional for loop?*

   A list comprehension in Python is a concise way to create lists, often in a more readable and expressive than a traditional for loop.
   Comprehesions are particularly when the goal is to create a new list by applying an expression to each item in an existing iterable.
 
   List Comprehension allow a programmer to create a list in a single line of code while a `for` loop
   typically requires multiple lines of code to achieve the same result. For simple operations, list comprehensions are more readable
   as the transformation and iteration are expressed in a compact form. Programmer transitioning from other languages may
   consider a `for` loop as more readable, but that's generally due to their experience and background.  For complex operations,
   `for` loops may be more readable for complex operations because each step is explicitly stated and can be commented on separately.

   From a performance standpoint, comprehensions tend to be slightly faster. However, readability and maintainability should be 
   the primary factors.

   ```python
   # Traditional for loop to create a list of squares
   squares = []
   for x in range(10):
       squares.append(x**2)

   print(squares)

   # List comprehension to create a list of squares
   squares = [x**2 for x in range(10)]

   print(squares)
   ```
   Output:
   ```
   [0, 1, 4, 9, 16, 25, 36, 49, 64, 81]
   [0, 1, 4, 9, 16, 25, 36, 49, 64, 81]
   ```


   List comprehensions can also include conditional logic, making them more powerful for certain tasks.
   ```python
   # List comprehension to create a list of even squares
   even_squares = [x**2 for x in range(10) if x % 2 == 0]

   print(even_squares)
   ```
   Output:
   ```
   [0, 4, 16, 36, 64]
   ```

   Choosing between a list comprehension and a traditional for loop depends on the specific task, readability, 
   and personal or team coding style preferences. For straightforward list transformations, list comprehensions 
   are usually preferred, while more complex logic might benefit from the clarity of traditional `for` loops.

2. *What is the syntax for a list comprehension in Python?*

   Basic Syntax
   ```python
   [expression for item in iterable]
   ```
   - `expression`: The expression that defines the elements of the new list.
   - `item`: The variable representing each element in the iterable.
   - `iterable`: Any iterable (e.g., list, range, string) that you want to iterate over.

   With Conditional Filtering

   You can add an `if` clause to include only elements that meet a certain condition.
   ```python
   [expression for item in iterable if condition]
   ```

   Nested List Comprehensions

   List comprehensions can also be nested for creating lists from nested iterables.

   ```python
   # flatten a 2d list
   matrix = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
   flat_list = [num for row in matrix for num in row]
   print(flat_list)
   ```
   Output:
   ```
   [1, 2, 3, 4, 5, 6, 7, 8, 9]
   ```

3. *What is the purpose of the conditional expression in a list comprehension?*
  
   The purpose of the conditional expression in a list comprehension is to filter (only choose) elements 
   from the iterable that satisfy a specified condition.

4. *How would you use a dictionary comprehension to create a dictionary from two lists?*

   You can use a dictionary comprehension in Python to create a dictionary from two lists by pairing 
   elements from each list as key-value pairs. The syntax for a dictionary comprehension is similar 
   to that of a list comprehension but uses curly braces `{}` instead of square brackets `[]`.

   ```python
   keys = ['a', 'b', 'c']
   values = [1, 2, 3]

   dictionary = {key: value for key, value in zip(keys, values)}

   print(dictionary)
   ```
   Output
   ```
   {'a': 1, 'b': 2, 'c': 3}
   ```
   
   Note: `zip(keys, values)` pairs each element from `keys` with the corresponding element from `values`. `zip()`
   stops generating values once it reaches the end of the shortest list.

5. *What are some best practices for writing readable and maintainable comprehensions in Python?*
   
   Writing readable and maintainable comprehensions in Python is essential for ensuring that your code is easy to understand and work with. Here are some best practices to follow:

   1. **Keep Comprehensions Simple**: Comprehensions should be simple and concise. 
      If a comprehension becomes too complex, it can be difficult to read and understand. 
      In such cases, it's better to use a traditional for loop.
   2. **Use Meaningful Variable Names**: Use descriptive names for the variables within the comprehension to make the code more readable.
   3. **Avoid Multiple Levels of Nesting**: Multiple nested comprehensions can be hard to read. If you need to nest comprehensions, 
      consider breaking the logic into multiple comprehensions or using loops.
   4. **Use Conditional Logic Sparingly**: While comprehensions support conditional logic, overuse can make 
      them difficult to follow. Ensure that any conditions are simple and necessary.
   5. **Abstract Complex Logic**: If the logic inside the comprehension is complex, consider breaking it down 
      into separate functions to improve readability.