# Core Python / Misc. Topics

## Review Questions
1. *Provide the O() runtime for the following operations:*
   <ol type="a">
     <li> checking if a value exists in a list: O(n)
     <li> inserting an element at the start of a list: O(n)
     <li> removing an element at the start of a list: O(n)
     <li> removing an element at the end of a list: O(1)
     <li> checking if a key exists in a dictionary: O(1)
     <li> checking if a value exists in a dictionary: O(n)
   </ol>

2. *Which of those operations executes the fastest?*

   The operations with O(1). However, this does ignore "constant" factors that may exist such 
   as computing the hash of a key.

3. *What is the primary focus of the field called "analysis of algorithms"?*

   To find the amount of resources (typically time and space/memory) needed to execute a particular solution or algorithm for a class of problems

4. *Explain the RAM (Random Access Machine) model of computation and its key assumptions.*
    
    The Random Access Machine (RAM) model is a hypothetical computer model used to perform machine-independent analysis of algorithms. In the RAM model, the following key assumptions are made:

    - Each simple operation (like an arithmetic operation, variable assignment, conditional statement, or function call) takes exactly one time step to execute.
    - Loops and function calls are composed of many single-step operations. The time cost of a loop or function depends on the number of iterations or operations performed within it.
    - Each memory access (reading from or writing to memory) takes exactly one time step, regardless of the memory location. The amount of available memory is considered unlimited.

   The RAM model provides a simplified and idealized abstraction of a computer, ignoring low-level hardware details and complexities. This allows for a machine-independent analysis of algorithms, where the runtime can be determined by counting the number of time steps required for a given input size.

   The key assumption is that all basic operations take a constant amount of time, regardless of the size of the operands or the memory locations involved. This assumption may not hold true for real machines, but it provides a reasonable approximation for analyzing algorithms and their time complexities.

   By using the RAM model, algorithms can be analyzed and compared based on their theoretical time and space requirements, independent of the specific hardware implementation.


5. *How does the RAM model treat the time cost of loops and functions?*

   Loops and function calls are composed of many single-step operations. The time cost of a loop or function depends on the number of iterations and operations performed within each iteration for function.

6. *Explain the concept of asymptotic analysis and its purpose in analyzing algorithms.*

   Asymptotic analysis is a method used in algorithm analysis to study and characterize the behavior of an algorithm's resource consumption (typically time or space complexity) as the input size grows towards infinity. The purpose of asymptotic analysis is to provide a simplified and concise way to describe and compare the efficiency of different algorithms, especially when dealing with large input sizes. We concentrate on the overall growth rate or the limiting behavior of the resource consumption function.

7. *Why is it useful to discard constants and focus on the growth rate in Big O notation?*

   Discarding constants and focus on the growth rate in Big O notation is useful for several reasons:
   - *Scalability analysis*: When analyzing the performance of algorithms for large input sizes, the constant factors become less significant compared to the dominant term that determines the growth rate. By ignoring constants, we can focus on how the algorithm's resource consumption scales with increasing input sizes, which is more important for understanding its behavior and efficiency.
   - *Simplification and generalization*: Discarding constants in Big O notation simplifies the mathematical expression, making it easier to understand and compare the asymptotic behavior of algorithms. It also allows for a more general analysis, as the same Big O notation can represent multiple algorithms with different constant factors.
   - *Emphasis on worst-case scenarios*: Big O notation typically represents the upper bound or the worst-case scenario for an algorithm's resource consumption. By discarding constants, we can concentrate on the most significant factor that determines the algorithm's performance in the worst case, which is important for ensuring correct and efficient operation under all circumstances.
   - *Comparison and algorithm selection*: When comparing algorithms or selecting the most appropriate one for a task, the growth rate represented by Big O notation is often more important than the exact resource consumption for specific input sizes. Algorithms with the same Big O notation but different constant factors may perform differently for small input sizes, but their relative performance becomes less significant as the input size grows.
   - *Machine independence*: Constants in algorithms can vary depending on the hardware, compiler, and other implementation details. By discarding constants, Big O notation provides a machine-independent way to analyze and compare algorithms, focusing on the underlying computational complexity rather than hardware-specific factors.

8. *Why is it important to choose the right data structure for a given problem?*

   Choosing the right data structure for a given problem is important for several reasons:
   - *Performance*: Different data structures have different time complexities for common operations like insertion, deletion, searching, and traversal. Selecting an appropriate data structure can significantly impact the performance and efficiency of your program, especially when dealing with large amounts of data or time-critical applications.
   - *Memory usage*: Data structures have varying memory footprints and memory allocation strategies. Using an inefficient data structure can lead to excessive memory consumption, which can be problematic in resource-constrained environments or when dealing with large datasets.
   - *Ease of implementation*: Some data structures are more suitable for certain types of problems than others. Using an appropriate data structure can simplify the implementation and make the code more readable, maintainable, and less error-prone.
   - *Algorithmic requirements*: Certain algorithms are designed to work efficiently with specific data structures. For example, breadth-first search is typically implemented using a queue, while depth-first search is often implemented using a stack. Choosing the right data structure can make it easier to implement these algorithms correctly and efficiently.
   - *Code organization*: Data structures can help organize and structure data in a way that aligns with the problem's requirements. For instance, if you need to store key-value pairs, a dictionary or a hash table would be a better choice than a list or an array.
   - *Future modifications*: The choice of data structure can also impact the ease of making future modifications or extensions to your code. Using an appropriate data structure can make it easier to add new features or handle changes in requirements.

9. *What is the purpose of reference counting in Python's memory management?*

   The purpose of reference counting in Python's memory management is to keep track of the number of active references pointing to an object in memory, and automatically free (deallocate) the memory occupied by that object when there are no remaining references to it.

10. *Explain the concept of aliases in Python and how they can lead to unexpected side effects.*

    In Python, an alias occurs when multiple variables refer to the same object in memory. This means that these variables are essentially different names for the same underlying object. They can also lead to unexpected side effects if not handled properly as modifying the object through one alias will affect all other aliases that refer to the same object.

    To avoid these side effects, it's important to be aware of aliases and take appropriate precautions, such as:
    - Creating copies of mutable objects when needed, instead of working with aliases.
    - Passing mutable objects as arguments to functions only when necessary, and being mindful of potential side effects.
    - Using defensive copying or deep copying techniques when working with nested mutable objects.
    - Being explicit about whether a function modifies its arguments or returns new objects.

## Exercises
1. Sorting Comparison

   Several web pages provide visualizations of various sorting algorithms - 
   - https://www.sortvisualizer.com/
   - https://sorting-algorithm-jet.vercel.app/select%20algorithm
   - https://www.toptal.com/developers/sorting-algorithms
   - https://visualgo.net/en/sorting?slide=1
   - https://math.hws.edu/eck/js/sorting/xSortLab.html

2. Asymptotic Analysis

   For `sum_list()`, the time complexity of this function is O(n), where n is the length of the input list. This is because the for loop iterates through each element of the list exactly once, performing a constant-time operation (addition) for each element. Therefore, the time complexity grows linearly with the size of the input list.

   For `is_prime()`, the time complexity of this function is O(sqrt(n)). This is because the for loop iterates from 2 up to the square root of n, performing a constant-time operation (modulus and comparison) for each iteration. Since the square root function grows more slowly than the linear function, the time complexity is O(sqrt(n)).

   For `liner_search()`, the time complexity of this function is O(n), where n is the length of the input list. This is because, in the worst case, the for loop needs to iterate through all elements of the list to determine whether the target element is present or not. The number of iterations is directly proportional to the size of the input list.

   For `binary_search()`, the time complexity of this function is O(log n), where n is the length of the input list. This is because, in each iteration of the while loop, the search space is reduced by half. The number of iterations required is proportional to the logarithm of the size of the input list.
