{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "30aa7b5c",
   "metadata": {},
   "source": [
    "<div class=\"pagebreak\"></div>\n",
    "\n",
    "# Classes: Inheritance\n",
    "With inheritance, we can create a new class by extending an existing class. The new class inherits the existing class's attributes and methods (the members). We can then add attributes and/or methods. We can also modify existing attributes and methods to change functionality.\n",
    "\n",
    "As we look around the world, we can find many instances of hierarchy in which specialized classes exist.  For example, the animal taxonomy serves to classify animals into different classes ([taxonomic ranks](https://en.wikipedia.org/wiki/Taxonomic_rank) based upon the different attributes that animals possess. Planes, cars, and trains are specialized cases of vehicles.  Circles, triangles, and rectangles are specialized cases of shapes. You can also view these examples using the phrase \"<i>is-a</i>\".  A train is a vehicle.  A circle is a shape. A dog is a mammal.\n",
    "\n",
    "In object-oriented programming, inheritance creates this \"_is-a_\" relationship among classes. We build a new class from an existing class. The existing (original) class is called a parent, superclass, or base class. The superclass is the more general class. The new class is called a child, subclass, or derived class. This new class is the more specialized class. The subclass becomes specialized by adding attributes and/or methods. The subclass can also become more specialized by modifying the existing state or behavior. \n",
    "\n",
    "\n",
    "In the previous notebook, we defined a BankAccount.  However, many different types of bank accounts exist: checking, savings, money market, brokerage, etc.  We also have transactions that can belong to those accounts.  Again, though, we have different transactions types: deposit, withdrawal, transfer, purchase, etc.  Then under deposit transactions, we have additional specialties: check, cash, ACH, etc.  Similar specialties will also exist for the other transaction types.\n",
    "\n",
    "As another example, consider different types of employees within a corporation. Corporations can have hourly employees, paid a fixed amount per hour and a specific multiplier for specialty shifts (holiday, weekend, nights). Corporations also can have salaried employees who earn a particular amount per pay period and commission employees who make a base salary per pay period plus a percentage of the gross sales they generate.\n",
    "\n",
    "The below diagram is a Unified Modeling Language(UML) class diagram. These diagrams document classes, their relationships to each other, and each class's members (attribute and methods). Each box represents a class. The top line contains that class name. The second area in the box contains the attributes, and the third area contains the methods.\n",
    "\n",
    "![](images/EmployeeClassHierarchy.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a46c3c10",
   "metadata": {},
   "source": [
    "Objects of the `Employee` class have three attributes - an ID, a name, and a job title - and no methods are defined. The `HourlyEmployee` class extends the `Employee` class by adding attributes for the employee's hourly rate and the hours that the employee worked during the current pay period. The `HourlyEmployee` also adds behavior - a method to compute their pay. The `SalariedEmployee` class extends the `Employee` class by adding an attribute for the employee's periodic salary amount. For example, if an employee earns $\\$$120,000 annually and is paid twice a month, the periodic pay would be $\\$$5,000. The `SalariedEmployee` class also defines a method to compute pay, but this differs from the HourlyEmployee's behavior. `CommissionedEmployee` inherits the behavior and attributes of `SalariedEmployee` but adds an attribute to track their sales over the past pay period. Their pay calculation will \"override\" the `SalariedEmployee` class's pay calculation as they receive a salary plus a percentage of their gross sales."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c696bf2e",
   "metadata": {},
   "source": [
    "The following code cell defines the `Employee` class, the superclass for the three other classes.  This code should be familiar based on the previous notebook. This code does create a `calculate_pay()` method, but any call to that will raise an exception. By defining a method here, we establish that any subclass must implement a `calculate_pay()` method. Similarly, the `employee_type` property produces \"unknown\" as we should not have any objects created as Employee."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3cf47068",
   "metadata": {},
   "outputs": [],
   "source": [
    "class Employee:\n",
    "    \"\"\"Employee\"\"\"\n",
    "    __id = 100  # must change name otherwise a recursive overflow error occurs\n",
    "    \n",
    "    def __init__(self, name, job_title):\n",
    "        self.__id = Employee.__id\n",
    "        Employee.__id += 1\n",
    "        self.__name = name\n",
    "        self.__job_title = job_title\n",
    "        \n",
    "    def __str__(self):\n",
    "        return \"ID #{:d}: {:s}({:s}, {:s})\".format(self.id,self.name, self.job_title, self.employee_type)\n",
    "    \n",
    "    def __repr__(self):\n",
    "        return str({ \"id\": self.id, \"name\": self.name, \"job_title\":self.job_title })\n",
    "    \n",
    "    @property\n",
    "    def id(self):\n",
    "        return self.__id\n",
    "\n",
    "    @property\n",
    "    def name(self):\n",
    "        return self.__name\n",
    "    \n",
    "    @property\n",
    "    def job_title(self):\n",
    "        return self.__job_title\n",
    "    \n",
    "    \n",
    "    @job_title.setter\n",
    "    def job_title(self, new_title):\n",
    "        self.__job_title = new_title\n",
    "    \n",
    "    @property\n",
    "    def employee_type(self):\n",
    "        return \"unknown\"\n",
    "        \n",
    "    \n",
    "    def calculate_pay(self):\n",
    "        raise  NotImplementedError(\"Employee subclasses must implement calculate_pay()\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a47e13a6",
   "metadata": {},
   "source": [
    "Some sample code to perform ad-hoc testing that the class works."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e63be7be",
   "metadata": {},
   "outputs": [],
   "source": [
    "a = Employee(\"Steve\",\"Programmer\")\n",
    "b = Employee(\"Christine\",\"Project Manager\")\n",
    "print(a)\n",
    "repr(b)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7d1abdc3",
   "metadata": {},
   "source": [
    "Now, define a more robust set of unit tests for `Employee`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e6aa872a",
   "metadata": {},
   "outputs": [],
   "source": [
    "import unittest\n",
    "\n",
    "class TestEmployee(unittest.TestCase):\n",
    "    def setUp(self):\n",
    "        Employee._Employee__id = 100\n",
    "        \n",
    "    def test_create(self):\n",
    "        import ast\n",
    "        \n",
    "        a = Employee(\"Steve\",\"Programmer\")\n",
    "        b = Employee(\"Christine\",\"Project Manager\")\n",
    "        self.assertNotEqual(a.id,b.id,\"IDs are not unique\")\n",
    "        self.assertEqual(a.name,\"Steve\")\n",
    "        self.assertEqual(b.name,\"Christine\")\n",
    "        self.assertEqual(a.job_title,\"Programmer\")\n",
    "        self.assertEqual(b.job_title,\"Project Manager\")\n",
    "        self.assertEqual(str(a),\"ID #100: Steve(Programmer, unknown)\")\n",
    "        self.assertEqual(ast.literal_eval(repr(b)),{'id': 101, 'name': 'Christine', 'job_title': 'Project Manager'})\n",
    "        \n",
    "    def test_calculate_pay_not_implemented(self):\n",
    "        a = Employee(\"Steve\",\"Programmer\")\n",
    "        with self.assertRaises(Exception) as context:\n",
    "            a.calculate_pay()\n",
    "        \n",
    "        self.assertTrue(type(context.exception) == NotImplementedError)\n",
    "        self.assertTrue(\"must implement\" in context.exception.args[0])\n",
    "        \n",
    "    def test_change_job_title(self):\n",
    "        a = Employee(\"Steve\",\"Programmer\")\n",
    "        a.job_title = 'Senior Programmer'\n",
    "        self.assertEqual(a.job_title,'Senior Programmer')\n",
    "        self.assertEqual(str(a),\"ID #100: Steve(Senior Programmer, unknown)\")\n",
    "        \n",
    "unittest.main(argv=['unittest','TestEmployee'], verbosity=2, exit=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "75503610",
   "metadata": {},
   "source": [
    "At this point, we have implemented our base `Employee` class and have a robust set of test cases for it.\n",
    "\n",
    "Next, let's look at defining the `HourlyEmployee` class. The following code block adds a few new details:\n",
    "1. We define `HourlyEmployee` as a subclass of `Employee`. Subclasses are defined similarly to other classes, except we add the parent class name inside of parenthesis at the end.  Syntax -\n",
    "   <pre>\n",
    "   class <i>ClassName</i>(<i>ParentClassName</i>):\n",
    "   </pre>\n",
    "2. In the initializer, we make a call to the parent class with `super()`.  As we have defined `__init__` in the child class, the interpreter does not automatically call the corresponding method in the parent class. Therefore, we explicitly call the initializer with reference to the superclass (`super()`). This call ensures our code performs the steps to initialize the base `Employee` type properly.  The `__init__` method then continues with setting the attributes specific to instances of `HourlyEmployee`.\n",
    "3. Note that in the initializer, we explicitly set `__hours_worked` to `None`. This statement defines that attribute. In `calculate_pay()`, we add a sanity check to our code to ensure `hours_worked` has a valid value with the `assert` statement.  Programmers can place a conditional expression within an `assert` statement. If the expression evaluates to `True`, processing continues normally. If the expression evaluates to `False`, the interpreter raises an exception.\n",
    "4. The `HourlyEmployee` class adds additional methods to support the `hourly_rate` and `hours_worked` attributes.\n",
    "5. In addition to the `__init__` method, the `HourlyEmployee` class also overrides the methods for `employee_type` and `calculate_pay`.  By overriding methods, objects of type `HourlyEmployee` will use the behavior for the methods defined within  `HourlyEmployee` itself. Any behavior defined in the parent class will not be performed unless explicitly called through the `super()` reference.\n",
    "6. Notice that the `HourlyEmployee` class did not change the `__str__` or `__repr__` methods.  In the test code in the following block, notice that when the `__str__` method gets the employee type, it calls the method based on the actual class. i.e., an instance of `Employee` returns \"unknown\" while an instance of `HourlyEmployee` returns \"hourly\"."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "dbd8a170",
   "metadata": {},
   "outputs": [],
   "source": [
    "from decimal import Decimal\n",
    "\n",
    "class HourlyEmployee(Employee):\n",
    "    \"\"\"Hourly Employee\"\"\"\n",
    "    \n",
    "    \n",
    "    def __init__(self, name, job_title,hourly_rate):\n",
    "        super().__init__(name,job_title)\n",
    "        self.__hourly_rate = Decimal(hourly_rate)\n",
    "        self.__hours_worked = None        \n",
    "        \n",
    "    @property\n",
    "    def employee_type(self):\n",
    "        return \"hourly\"\n",
    "        \n",
    "    @property\n",
    "    def hourly_rate(self):\n",
    "        return self.__hourly_rate\n",
    "    \n",
    "    \n",
    "    @hourly_rate.setter\n",
    "    def hourly_rate(self, new_rate):\n",
    "        self.__hourly_rate= new_rate        \n",
    "        \n",
    "\n",
    "    @property\n",
    "    def hours_worked(self):\n",
    "        return self.__hours_worked\n",
    "    \n",
    "    \n",
    "    @hours_worked.setter\n",
    "    def hours_worked(self, new_hours):\n",
    "        self.__hours_worked = Decimal(new_hours)               \n",
    "        \n",
    "    def calculate_pay(self):\n",
    "        assert type(self.hours_worked) is Decimal, \"Hours worked not established\"\n",
    "        hours = self.hours_worked\n",
    "        overtime_hours = Decimal(0)\n",
    "        if hours > 40:\n",
    "            overtime_hours = hours - Decimal(40.0)\n",
    "            hours = Decimal(40.0)\n",
    "        return hours * self.hourly_rate + overtime_hours * self.hourly_rate* Decimal(1.5)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "32a38ad0",
   "metadata": {},
   "source": [
    "Run some code to see how the `HourlyEmployee` class works."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "01ef50ce",
   "metadata": {},
   "outputs": [],
   "source": [
    "c = HourlyEmployee(\"Max\",\"System Administrator\",\"54.76\")\n",
    "print(c)\n",
    "print(c.name)\n",
    "print(c.hours_worked)\n",
    "print(c.calculate_pay())   # will cause an assertion error as hours_worked not set."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f1894be6",
   "metadata": {},
   "source": [
    "Now define some additional test cases. First, we check that the parent functionality still works.\n",
    "We also check that the `calculate_pay()` method checks that hours_worked has a valid numerical value. We also check several equivalence  classes for `hours_worked` in `compute_pay()` to cover amounts < 40 hours, amounts equal to 40 hours, and amounts greater than 60 hours. Finally, we check that the employee type value is correct for the different types."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "69e23d68",
   "metadata": {},
   "outputs": [],
   "source": [
    "import unittest\n",
    "\n",
    "class TestHourlyEmployee(unittest.TestCase):\n",
    "    def setUp(self):\n",
    "        Employee._Employee__id = 100\n",
    "        \n",
    "    def test_create(self):\n",
    "        a = HourlyEmployee(\"Max\",\"System Administrator\",65.0)\n",
    "        self.assertEqual(a.name,\"Max\")\n",
    "        self.assertEqual(a.job_title,\"System Administrator\")\n",
    "        self.assertEqual(str(a),\"ID #100: Max(System Administrator, hourly)\")\n",
    "\n",
    "    def test_compute_pay_no_hours(self):\n",
    "        a = HourlyEmployee(\"Max\",\"System Administrator\",65.0)\n",
    "        with self.assertRaises(Exception) as context:\n",
    "            a.calculate_pay()\n",
    "        \n",
    "        self.assertTrue(type(context.exception) in [TypeError,AssertionError])\n",
    "\n",
    "    def test_compute_pay(self):\n",
    "        a = HourlyEmployee(\"Max\",\"System Administrator\",65.0)\n",
    "        a.hours_worked = 20\n",
    "        self.assertEqual(a.calculate_pay(), Decimal(1300.0), \"Pay not correct\")\n",
    "        a.hours_worked = 40\n",
    "        self.assertEqual(a.calculate_pay(), Decimal(2600.0), \"Pay not correct\")\n",
    "        \n",
    "    def test_compute_pay_with_overtime(self):\n",
    "        a = HourlyEmployee(\"Max\",\"System Administrator\",65.0)\n",
    "        a.hours_worked = 60\n",
    "        self.assertEqual(a.calculate_pay(), Decimal(4550.0), \"Pay not correct\")\n",
    "        \n",
    "    def test_employee_types(self):\n",
    "        a = HourlyEmployee(\"Max\",\"System Administrator\",65.0)\n",
    "        b = Employee(\"Cindy\", \"Sales Manager\")\n",
    "        self.assertEqual(a.employee_type,\"hourly\")\n",
    "        self.assertNotEqual(a.employee_type,b.employee_type)\n",
    "        \n",
    "unittest.main(argv=['unittest','TestHourlyEmployee'], verbosity=2, exit=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "27e1a317-2760-422b-a00d-80051a394f2e",
   "metadata": {},
   "source": [
    "Many programming languages support the concept of an abstract class. Such a class cannot be instantiated on its own and is meant to serve as a base class for other classes.  Within the base class, we can declare common behavior and properties for all its subclasses.  We can also define behavior that should be implemented by the subclasses such as with `calculate_pay()`. To formally define an abstract base class in Python, use the [`abc` module](https://docs.python.org/3/library/abc.html)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "03f756c0",
   "metadata": {},
   "source": [
    "## Multiple Inheritance\n",
    "Multiple inheritance is the ability of a class to inherit from two or more superclasses. \n",
    "\n",
    "The primary drawback to multiple inheritance is the diamond problem. In the below diagram, suppose classes A, B, and C have all defined a particular method while D has not. Then, when that method is called on an object of class D, which version of the method is used? A's? B's? C's?\n",
    "\n",
    "![](images/DiamondProblem.png)\n",
    "\n",
    "Python solves this problem by defining a specific method resolution order. When looking for a method or attribute, Python performs the following search: the object itself, the object's class, the first parent class, the second parent class, the n<sup>th</sup> parent class, and then those parent's in order.\n",
    "\n",
    "For example, consider the following set of classes:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3d510ee4",
   "metadata": {},
   "outputs": [],
   "source": [
    "class SkilledEmployee:\n",
    "    def beverage(self): return \"water\"\n",
    "    def skill(self):    return \"Works hard\"\n",
    "\n",
    "class Programmer(SkilledEmployee):\n",
    "    def skill(self):    return \"Writes Code\"\n",
    "\n",
    "class Statistician(SkilledEmployee):\n",
    "    def skill(self):    return \"statistical analysis\"\n",
    "    \n",
    "class StoryTeller(SkilledEmployee):\n",
    "    def beverage(self): return \"beer\"\n",
    "    def skill(self):    return \"tells stories\"\n",
    "    \n",
    "class ComputationalDataScientist(Programmer,Statistician):\n",
    "    def beverage(self): return \"Mountain Dew\"\n",
    "\n",
    "class StatisticalAnalyst(Statistician, Programmer):\n",
    "    def beverage(self): return \"tea\"\n",
    "\n",
    "class Presenter(StoryTeller, Programmer, Statistician):\n",
    "    pass\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a8e5bdcc",
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"Skills - \")\n",
    "print(\"ComputationalDataScientist:\", ComputationalDataScientist().skill())\n",
    "print(\"StatisticalAnalyst:\", StatisticalAnalyst().skill())\n",
    "print(\"Presenter:\", Presenter().skill())\n",
    "print(\"\\nBeverages - \")\n",
    "print(\"ComputationalDataScientist:\", ComputationalDataScientist().beverage())\n",
    "print(\"StatisticalAnalyst:\", StatisticalAnalyst().beverage())\n",
    "print(\"Presenter:\", Presenter().beverage())"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "323f4a2f",
   "metadata": {},
   "source": [
    "Each Python class contains a method `mro()` that returns the list of classes to search to find a particular attribute or method for an object of that class."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f0da888a",
   "metadata": {},
   "outputs": [],
   "source": [
    "ComputationalDataScientist.mro()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "febc864c",
   "metadata": {},
   "source": [
    "As we look for a skill for a `ComputationalDataScientist`, the interpreter checks these class definitions\n",
    "1. The object itself  \n",
    "2. The object's class   (class and static methods)\n",
    "3. The class's first parent class - `Programmer`\n",
    "4. The class's second parent class - `Statistician`\n",
    "5. The interpreter then continues to check the parent's superclasses in a similar order.\n",
    "\n",
    "Finding the `skill()` implementation for `StatisticalAnalyst` follows the same logic, but its first parent class is `Statistician` and then `Programmer`.\n",
    "\n",
    "`Presenter` shows that we could inherit from 3 parent classes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "97c8b20a",
   "metadata": {},
   "outputs": [],
   "source": [
    "Presenter.mro()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8b2d6922",
   "metadata": {},
   "source": [
    "## Polymorphism and Duck Typing\n",
    "Polymorphism is the ability to call a specific method (send a message) to an object without knowing the receiving object's actual type. If the receiving object implements that method, then it can respond appropriately. A runtime exception is generated if the receiving object does not implement that method.\n",
    "\n",
    "The `Employee` class demonstrates polymorphism within the `__str__` method. That method calls the `employee_type()` method without knowing the exact underlying type of self. The multiple inheritance example demonstrates polymorphism as well.\n",
    "\n",
    "With Polymorphism, Python programmers can apply the same operation(method call) to different types as long as the method's name and the number of arguments exist with the receiving type's definition. \n",
    "\n",
    "Polymorphism allows methods of the same name to have predictable behavior but allows the underlying class to define the specific behavior independently.\n",
    "\n",
    "[Duck typing](https://en.wikipedia.org/wiki/Duck_typing) is a programming concept used primarily in dynamically typed languages, where the type or class of an object is determined by its behavior (methods) and properties rather than its inheritance or class definition. Duck typing allows for more flexible and extensible code by focusing on what an object can do.  \n",
    "\n",
    "The following code demonstrates polymorphism and duck typing. `speak()` is called on the animal parameter in the function `animal_sound()`. We do not know from looking at the code what the `Dog.speak()` or `Duck.speak()` is called - that determination is made at runtime based upon the actual type of `animal` (polymorphism). With duck typing, if an object behaves like a certain type (meaning it has the necessary methods or attributes), it's treated as an instance of that type, regardless of its actual class or type definition. In this example, the presence of the method `speak()` effectively becomes a type."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0043e32f-5203-45cd-a2d3-b55f0889e835",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": [
     "raises-exception"
    ]
   },
   "outputs": [],
   "source": [
    "class Dog:\n",
    "    def speak(self):\n",
    "        return \"Woof!\"\n",
    "\n",
    "class Duck:\n",
    "    def speak(self):\n",
    "        return \"Quack!\"\n",
    "\n",
    "class Fox:\n",
    "    def run(self):\n",
    "        return \"Running!\"\n",
    "\n",
    "def animal_sound(animal):\n",
    "    return animal.speak()\n",
    "\n",
    "d = Dog()\n",
    "duck = Duck()\n",
    "f = Fox()\n",
    "\n",
    "print(animal_sound(d))    # Output: Woof!\n",
    "print(animal_sound(duck)) # Output: Quack!\n",
    "print(animal_sound(f))    # AttributeError "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "54bbfba5-3d46-41e2-8bbf-c00cb7312b28",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "source": [
    "For a fun diversion, [What Does the Fox Say?](https://en.wikipedia.org/w/index.php?title=The_Fox_(What_Does_the_Fox_Say%3F)) - [YouTube](https://www.youtube.com/watch?v=jofNR_WkoCE)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "424f99a8",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "source": [
    "## Mixins \n",
    "A popular use case for multiple inheritance is to inherit from a particular class (a \"mixin\") that defines well-established methods and attributes (features).  Usually, only one feature exists in a \"mixin\" class.  This class does not share methods with any other parent class - this avoids the diamond problem.  The inheritance from \"mixins\" is not an \"is a\" relationship, but rather \"has behavior\".\n",
    "\n",
    "The methods in \"mixin\" classes are typically \"side\" tasks - sometimes generic in nature, such as logging or type conversions. However, the methods can also be specific to the problem domain, adding shared functionality to different classes. For example, a charting library could have mixins to deal with colors and legends.\n",
    "\n",
    "The below example creates `DumpAttributeMixin` to print an object's attributes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e4aea6db",
   "metadata": {},
   "outputs": [],
   "source": [
    "class DumpAttributeMixin:\n",
    "    def dump(self):\n",
    "        import pprint\n",
    "        pprint.pprint(vars(self))\n",
    "        \n",
    "class HourlyDumpEmployee(HourlyEmployee, DumpAttributeMixin):\n",
    "    pass\n",
    "\n",
    "c = HourlyDumpEmployee(\"Max\",\"System Administrator\",\"54.76\")\n",
    "c.dump()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fc919c13",
   "metadata": {},
   "outputs": [],
   "source": [
    "help(object)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4c1a9df0",
   "metadata": {},
   "source": [
    "## Determining Object Types\n",
    "Python has several different ways to test an object's type:\n",
    "\n",
    "The buit-in function`isinstance()` tests if an object is an instance of a particular type.  Notice that since `StoryTeller` inherits from SkilledEmployee, that e is also a SkilledEmployee.  We are maintaining the _is a_ relationship."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d2d59fbb",
   "metadata": {},
   "outputs": [],
   "source": [
    "e = StoryTeller()\n",
    "print(\"isinstance(e,StoryTeller):\", isinstance(e,StoryTeller))\n",
    "print(\"isinstance(e,Programmer):\", isinstance(e,Programmer))\n",
    "print(\"isinstance(e,SkilledEmployee):\", isinstance(e,SkilledEmployee))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c93cc840",
   "metadata": {},
   "source": [
    "We can also use the built-in function type(), which only checks if the object is that exact type (i.e., it does not consider inheritance)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "06f3b4e3",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "e = StoryTeller()\n",
    "print(type(e))\n",
    "print(\"Equality to StoryTeller:\", type(e) == StoryTeller)\n",
    "print(\"Equality to Programmer:\",type(e) == Programmer)\n",
    "print(\"Equality to SkilledEmployee:\",type(e) == SkilledEmployee)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b8ee9643",
   "metadata": {},
   "source": [
    "The built-in function issubclass tests if the class reference is derived from another class or is the same class. The first argument must be a class"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "72d36f79",
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"issubclass(Statistician,Statistician):\",       issubclass(Statistician,Statistician))\n",
    "print(\"issubclass(Statistician,Programmer):\",         issubclass(Statistician,Programmer))\n",
    "print(\"issubclass(Statistician,SkilledEmployee):\",    issubclass(Statistician,SkilledEmployee))\n",
    "print(\"issubclass(Statistician,StatisticalAnalyst):\", issubclass(Statistician,StatisticalAnalyst))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4df3e970",
   "metadata": {},
   "source": [
    "Ideally, we do not want to explicitly check an object's type. We should rely upon Python to use duck typing and polymorphism to take the appropriate behavior. If necessary, we can handle an exception if we call an object that does not implement a particular method."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5ca92dfb",
   "metadata": {},
   "source": [
    "## Note\n",
    "In Python, all objects implicitly inherit from the class `object` if a parent class is not explicitly  defined. This implicit inheritance is `object` appears as the last item in the `mro()` calls above. This inheritance hierarchy allows for shared behavior defined among all created objects in Python - such as the ability to get the string representation."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3385c693",
   "metadata": {},
   "source": [
    "## Suggested LLM Prompts\n",
    "- Explain the concept of inheritance in object orient programming, where a subclass inherits attributes and methods \n",
    "  from a parent or base class. Demonstrate how to create a subclass in Python and how to override \n",
    "  or extend methods from the parent class. Include examples of single and multiple inheritance.\n",
    "- Describe the different types of inheritance in Python (single, multiple, and multilevel inheritance) \n",
    "  with code examples for each type. Discuss the advantages and potential pitfalls of using multiple inheritance.\n",
    "- Explain the concept of method overriding in Python inheritance. Provide an example where a child class \n",
    "  overrides a method from its parent class, and discuss the reasons for overriding methods.\n",
    "- Explain polymorphism, which is the ability of objects to take on many forms. Demonstrate how to \n",
    "  achieve polymorphism in Python through method overriding in subclasses. Show examples of how the \n",
    "  same method can behave differently based on the object's class.\n",
    "- Discuss the use of the `super()` function in Python inheritance. Provide an example where `super()` \n",
    "  is used to call a method in the parent class from a child class. Explain the benefits of using `super()`.\n",
    "- Explain the concept of abstract classes and abstract methods in Python. Discuss the purpose of \n",
    "  using abstract classes and provide an example of how to create an abstract class and abstract methods.\n",
    "- Discuss the tradeoffs between inheritance and composition in Python. Provide examples of scenarios \n",
    "  where inheritance would be preferable and scenarios where composition (using objects as attributes) \n",
    "  would be a better design choice.\n",
    "- Explain the concept of multiple inheritance in Python and discuss the potential issues that can arise, \n",
    "  such as the diamond problem. Provide an example of the diamond problem and discuss possible solutions.\n",
    "- Discuss the use of mixins in Python, which are classes designed to provide specific functionality to \n",
    "  other classes through inheritance. Provide an example of creating and using a mixin class."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a55ce046",
   "metadata": {},
   "source": [
    "## Review Questions\n",
    "\n",
    "1. What is inheritance in the context of object-oriented programming, why is it useful, and how is it implemented in Python?\n",
    "2. How do you create a subclass that inherits from a parent class in Python?  Explain the difference from a regular class.\n",
    "3. What is the purpose of the super() function when working with inheritance in Python?\n",
    "4. What is method overriding, and how is it achieved in Python?\n",
    "5. What is the difference between single inheritance, multiple inheritance, and multilevel inheritance?\n",
    "6. What is polymorphism in object-oriented programming, and how is it implemented in Python?\n",
    "7. What is an abstract class, and how do you define one in Python?\n",
    "8. What is the purpose of abstract methods in abstract classes?\n",
    "9. What is the diamond problem in multiple inheritance, and how is it resolved in Python?\n",
    "10. What are mixins in Python, and how are they used in inheritance?\n",
    "11. How can you check if an instance is an instance of a particular class or its subclasses?\n",
    "12. Which of these is a common tool that software engineers use to describe the design of their classes?\n",
    "    <ol type=\"a\">\n",
    "      <li>XML inheritance trees.\n",
    "      <li>HTML class refinements.\n",
    "      <li>CDML hierarchies.\n",
    "      <li>UML diagrams.\n",
    "    </ol>\n",
    "13. What will be the output of the following Python code?\n",
    "    ```python\n",
    "    class Test:\n",
    "        def __init__(self):\n",
    "            self.x = 0\n",
    "\n",
    "    class Derived_Test(Test):\n",
    "        def __init__(self):\n",
    "            Test.__init__(self)\n",
    "            self.y = 1\n",
    "\n",
    "    b = Derived_Test()\n",
    "    print(b.x,b.y)\n",
    "    ```\n",
    "    <ol type=\"a\">\n",
    "      <li> Syntax error in the code\n",
    "      <li> The program runs fine but nothing is printed\n",
    "      <li> 1 0\n",
    "      <li> 0 1\n",
    "    </ol>\n",
    "\n",
    "14. What will be the output of the following Python code?\n",
    "    ```python\n",
    "    class A:\n",
    "        def one(self):\n",
    "            return self.two()\n",
    "   \n",
    "        def two(self):\n",
    "            return 'A'\n",
    "   \n",
    "    class B(A):\n",
    "        def two(self):\n",
    "            return 'B'\n",
    "\n",
    "    obj1=A()\n",
    "    obj2=B()\n",
    "    print(obj1.two(),obj2.two())\n",
    "    ```\n",
    "    <ol type=\"a\">\n",
    "      <li> A A\n",
    "      <li> A B\n",
    "      <li> B B \n",
    "      <li> An exception occurs\n",
    "    </ol>\n",
    "\n",
    "15. Within an initializer methods (`__init__`), is it necessary to call the parent's initializer method?\n",
    "16. Which of the following statements is not true about inheritance?\n",
    "    <ol type=\"a\">\n",
    "      <li> Inheritance represents an \"is a\" relationship\n",
    "      <li> Classes can inherit from multiple parent classes in Python.\n",
    "      <li> Inheritance allows us to inherit attributes from a child class.\n",
    "      <li> Inheritance provides another mechanism for code reuse.\n",
    "    </ol>\n",
    "\n",
    "[answers](answers/rq-26-answers.md)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bd25927f",
   "metadata": {},
   "source": [
    "## Exercises\n",
    "1. Complete the Salaried and Commissioned Employee classes. Commissioned employees earn 5% of their sales. Salary should be set when constructing the classes."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
