# Docker Installation

If neither of the previous installations work, you can also use the notebooks 
(and have the associated development environment) through using Docker. 

Docker is an open-source platform designed to automate applications' deployment, scaling, and management 
of lightweight, portable containers. Containers bundle an application with all 
its dependencies, libraries, and configuration files, enabling consistent and efficient deployment 
across different environments, from development to production. Docker's containerization approach 
helps eliminate "it works on my machine" issues by creating isolated, reproducible 
environments that run independently of the underlying operating system.

[Docker 101 Tutorial](https://www.docker.com/101-tutorial/)

Ideally, your laptop should have 16 GB of RAM. However, the bare minimum required is 4 GB.

## Prerequisites
Based on your platform, perform the necessary commands to install the prerequisites.

### MacOS
Install [VSCode](https://code.visualstudio.com/docs/setup/mac).

### Windows 
You'll first need to install Windows Subsystem for Linux. Follow the instructions to step 8 on the 
[Windows Installation](03-Windows_Installation.md) page.


Install [VSCode](https://code.visualstudio.com/docs/setup/windows).

```{admonition} Docker Not Accessible
If you receive the message "Command 'docker' could not be found in this WSL 2 distro" when executing
the steps in the Docker section, take the following steps.

1. Make sure you are using WSL version 2 for your distribution. 
   In Powershell, execute `wsl -l -v`

   If it's not at version 2, execute `wsl --set-version DistributionName 2`

2. Within Docker Desktop, select Settings, then Resources, and then WSL Integration.  Ensure Docker
   is integrated with your distribution.
```


## Docker
Follow the platform-specific install instructions for [Docker Desktop](https://docs.docker.com/desktop/).

It is not necessary to sign in to the Docker Desktop.

### Creating the Docker Container
Open a terminal window (MacOS) or WSL (Windows) to create the initial Docker container.

At the command prompt:
1. Pull (copy to your computer) the Duke Fintech 510 image
   ```bash
   docker pull dukefintech/510:latest
   ```
2. Start the image for the first time 
   ```bash
   docker run -it -p 8888:8888 --name fintech dukefintech/510
   ```

   Within Docker Desktop, you can create the container by selecting images.  Then
   click on the "play" icon in the dukefintech/510 row.

   In the dialog that appears, expand the Option settings.  Use "fintech" as the image name
   and "8888" as the image port:

   ![](images/docker_run.png)

   Then click "Run".


### Stopping
From the command line,
```
docker stop fintech
```

Within Docker Desktop, under Containers, click on the associated "Stop" button for the "fintech" container.

### Starting
Once the container has been created, you can restart the container at the command line using
```
docker start fintech
```
Within Docker Desktop, under Containers, click on the associated "Stop" button for the "fintech" container.

## Jupyter Labs / Notebooks
Once the container is running, open a web browser and access [http://localhost:8888/lab](http://localhost:8888/lab).

You can also update the notebooks with the latest content by opening a terminal window in Jupyter Lab.  Then execute:
```bash
cd /repo/guide
git reset --hard
git pull
```


## VSCode
With the `fintech` container running:
1. Start VS Code
2. Ensure the Remote Development Extension Pack is installed. Click on the Extensions tab along the left-hand side.
   Then search for "Remote Development".  Install if necessary.
3. On MacOS, press <span class="keys"><kbd class="key-shift">Command(⌘)</kbd><span>+</span><kbd class="key-shift">Shift</kbd><span>+</span><kbd class="key-p">p</kbd></span>.
   For Windows, press <span class="keys"><kbd class="key-shift">Ctrl</kbd><span>+</span><kbd class="key-shift">Shift</kbd><span>+</span><kbd class="key-p">p</kbd></span>.  This opens the Command Palette.  Start typing "Dev Containers".
4. Select "Dev Containers: Attach to running containers and then select the "fintech' container.
5. In the new VSCode window that appears, click on the Extensions tab.  Now, install the following extensions into
   the container:
   - Python (from Microsoft)
   - Python Extension Pack (from Don Jayamanne)
   - C/C++ (from Microsoft)
   - C/C++ Extension (from Microsft)
   
6. Select the File Explorer tab along the left-hand side.  Click "Open Folder".  Select "/repo/" in the dialog.
7. If you need to clone additional materials into the container, run the appropriate command within the VSCode terminal.
   For example, to clone the Duke FinTech 510 course assignments execute:
   ```bash
   cd /repo
   git clone https://gitlab.oit.duke.edu/fintech-510/fintech510-studentrepository.git
   ```

8. To update the guide (i.e., these notebooks within the docker environment):
   1. Open the terminal window within VSCode
   2. Execute
      ```bash
      cd /repo/guide
      git reset --hard
      git pull
      ```
      The second command removes any changes to the notebooks, while the third downloads any updates.
