{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Functions\n",
    "As with most other programming languages, C++ also has the concept of _functions_, which are named reusable code blocks. They can take any number of parameters to send data into the function. Functions can then return a result to the calling block. Functions exist to perform a specific task within the construct of a larger program.\n",
    "As you use functions within your programs, two fundamental concepts exist:\n",
    "1. Defining (declaring) the function. This provides the function's definition: the name, parameters, and functionality. Unlike Python, you will need to explicitly define functionʼs return type and parameter.&nbsp; A function's name and the parameter types determine the function's *signature*. Many consider the four parts of a function to be the name, return type, parameters, and the function body.\n",
    "2. Calling the function. As we call (execute) a function, we pass a specific list of arguments as the values for a function's parameters.\n",
    "C++ uses the follow syntax to define a function:\n",
    "```c++\n",
    "returnType functionName(parameterList) {\n",
    "    functionBody\n",
    "}\n",
    "```\n",
    "Unlike Python, you must define both the parameter type and the name. Parameters are separated by commas. In C++, if your function does not return a value, you should declare the function to return void. Otherwise, this is considered a compiler warning. If a function has no parameters, you can either place the keyword `void` between the parenthesis or leave that space blank."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "cpp"
    }
   },
   "outputs": [],
   "source": [
    "//filename: function.cpp\n",
    "//compile: g++ -std=c++17 -o function function.cpp\n",
    "//execute: ./function\n",
    "#include <iostream>\n",
    "using std::cout;\n",
    "\n",
    "int max(int num1, int num2) {\n",
    "   if (num1 > num2) {\n",
    "       return num1;\n",
    "   }\n",
    "   else {\n",
    "       return num2;\n",
    "   }\n",
    "}\n",
    "\n",
    "int main(int argc, char *argv[]) {\n",
    "    int num1 = 122, num2 = 22;\n",
    "    int result = max(num1,num2);\n",
    "    cout << \"Maximum of (\" << num1 << \", \" << num2 << \"): \" << result << \"\\n\";\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a href=\"https://pythontutor.com/render.html#code=%23include%20%3Ciostream%3E%0Ausing%20std%3A%3Acout,%20std%3A%3Aendl%3B%0A%0Aint%20max%28int%20num1,%20int%20num2%29%20%7B%0A%20%20%20if%20%28num1%20%3E%20num2%29%20%7B%0A%20%20%20%20%20%20%20return%20num1%3B%0A%20%20%20%7D%0A%20%20%20else%20%7B%0A%20%20%20%20%20%20%20return%20num2%3B%0A%20%20%20%7D%0A%7D%0A%0Aint%20main%28int%20argc,%20char%20*argv%5B%5D%29%20%7B%0A%20%20%20%20int%20num1%20%3D%20122,%20num2%20%3D%2022%3B%0A%20%20%20%20int%20result%20%3D%20max%28num1,num2%29%3B%0A%20%20%20%20cout%20%3C%3C%20%22Maximum%20of%20%28%22%20%3C%3C%20num1%20%3C%3C%20%22,%20%22%20%3C%3C%20num2%20%3C%3C%20%22%29%3A%20%22%20%3C%3C%20result%20%3C%3C%20endl%3B%0A%7D&amp;cumulative=false&amp;curInstr=0&amp;heapPrimitives=nevernest&amp;mode=display&amp;origin=opt-frontend.js&amp;py=cpp_g%2B%2B9.3.0&amp;rawInputLstJSON=%5B%5D&amp;textReferences=false\">Visualize execution</a>\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Passing Parameters\n",
    "By default, C++ passes parameters to functions by value. In other words, a copy of a value is placed in the stack frame for the called object. Without using pointers or references, any changes made to a parameter are limited to the scope of that function. The following incorrect implementation of `swap` demonstrates *call by value* in that `a` and `b` remain unchanged after the call to `swap`.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "cpp"
    }
   },
   "outputs": [],
   "source": [
    "//filename: badswap.cpp\n",
    "//compile: g++ -std=c++17 -o badswap badswap.cpp\n",
    "//execute: ./badswap\n",
    "#include <iostream>\n",
    "using std::cout;\n",
    "\n",
    "void swap(int a, int b) {\n",
    "    int t = a;\n",
    "    a = b;\n",
    "    b = t;\n",
    "    cout << \"in swap: (a, b) = (\" << a << \", \" << b << \")\" << \"\\n\";\n",
    "}\n",
    "\n",
    "int main() {\n",
    "    int a = 10;\n",
    "    int b = 15;\n",
    "    cout << \"before swap: (a, b) = (\" << a << \", \" << b << \")\" << \"\\n\";\n",
    "    swap(a,b);\n",
    "    cout << \"after swap: (a, b) = (\" << a << \", \" << b << \")\" << \"\\n\";\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following code prints the address of the variable `a` - this demonstrates that `a` is a different object is a different object between `main` and bad implementation of `swap`. Remember, that in C++, an object is \"a region of data storage in the execution environment, the contents of which can represent values\". The address points (refers) to that data storage in memory. The unary operator &amp; is called the \"address-of\" operator. This operator provides the address of a variable in memory address. These addresses returned by the address-of operator are known as pointers because they “point” to the variable in memory.The address-of operator can only be applied to lvalues."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "cpp"
    }
   },
   "outputs": [],
   "source": [
    "//filename: swap_address.cpp\n",
    "//compile: g++ -std=c++17 -o swap_address swap_address.cpp\n",
    "//execute: ./swap_address\n",
    "#include <iostream>\n",
    "using std::cout;\n",
    "\n",
    "void swap(int a, int b) {\n",
    "    int t = a;\n",
    "    a = b;\n",
    "    b = t;\n",
    "    cout << \"in swap: a address \" << &a << \"\\n\";\n",
    "}\n",
    "\n",
    "int main(int argc, char *argv[]) {\n",
    "    int a = 10;\n",
    "    int b = 15;\n",
    "    cout << \"before swap: a address \" << &a << \"\\n\";\n",
    "    swap(a,b);\n",
    "    cout << \"after swap: a address \" << &a << \"\\n\";\n",
    "}\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Reference Variables\n",
    "A reference variable in C++ is an alias for another variable. Once a reference is \n",
    "initialized to a variable, it cannot be changed to refer to another variable. \n",
    "References allow a program to access the original variable directly without creating a \n",
    "copy, enabling more efficient code and allowing functions to modify variables passed to them.\n",
    "\n",
    "Some general rules:\n",
    "- *Must be initialized:* References must be initialized when declared.\n",
    "- *Must have a value:* Given the previous point, references always refer to a valid \"object\".\n",
    "\n",
    "Syntax:\n",
    "```c++\n",
    "type &referenceName = existingVariable;\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "cpp"
    }
   },
   "outputs": [],
   "source": [
    "//filename: references.cpp\n",
    "//compile: g++ -std=c++17 -o references references.cpp\n",
    "//execute: ./references\n",
    "#include <iostream>\n",
    "\n",
    "int main() {\n",
    "    int original = 10;\n",
    "    int &ref = original; // ref is a reference to original\n",
    "\n",
    "    std::cout << \"Original: \" << original << \": \" << &original << '\\n';\n",
    "    std::cout << \"Reference: \" << ref <<  \": \" << &ref << '\\n';   \n",
    "\n",
    "    // Modifying the reference modifies the original variable\n",
    "    ref = 20;\n",
    "\n",
    "    std::cout << \"After modification:\" << std::endl;\n",
    "    std::cout << \"Original: \" << original << std::endl; // Outputs: 20\n",
    "    std::cout << \"Reference: \" << ref << std::endl;     // Outputs: 20\n",
    "\n",
    "    return 0;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Passing Parameters by Reference\n",
    "Within the same function, references are not useful - who needs an alias to an existing variable? However, we can also use reference variables as parameters in C++.\n",
    "\n",
    "Below, we demonstrate a working version of the `swap` function using C++'s _pass-by reference_ mechanism. Rather than passing the parameter value (such as 10 for `a`), C++ passes the value of `a`'s address. We can then use the variable as we normally would, and updates are applied to `a`'s original address - in other words, `a`is the same object in memory for both `main` and for `swap`.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "cpp"
    }
   },
   "outputs": [],
   "source": [
    "//filename: swap_references.cpp\n",
    "//compile: g++ -std=c++17 -o swap_references swap_references.cpp\n",
    "//execute: ./swap_references\n",
    "#include <iostream>\n",
    "using std::cout;\n",
    "\n",
    "void swap(int &a, int &b) {\n",
    "    int t = a;\n",
    "    a = b;\n",
    "    b = t;\n",
    "\n",
    "    cout << \"in swap: a address \" << &a << \"\\n\";\n",
    "    cout << \"in swap: (a, b) = (\" << a << \", \" << b << \")\" << \"\\n\";\n",
    "}\n",
    "\n",
    "int main() {\n",
    "    int a = 10;\n",
    "    int b = 15;\n",
    "\n",
    "    cout << \"before swap: a address \" << &a << \"\\n\";\n",
    "    cout << \"before swap: (a, b) = (\" << a << \", \" << b << \")\" << \"\\n\";\n",
    "\n",
    "    swap(a,b);\n",
    "\n",
    "    cout << \"after swap: a address \" << &a << \"\\n\";\n",
    "    cout << \"after swap: (a, b) = (\" << a << \", \" << b << \")\" << \"\\n\";\n",
    "\n",
    "}\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following code demonstrates the C++ use of _call by value_ and _return by value_ with classes. You can see that C++ copies the return value from `squarePoint` into `p2`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "cpp"
    }
   },
   "outputs": [],
   "source": [
    "//filename: class_return.cpp\n",
    "//compile: g++ -std=c++17 -o class_return class_return.cpp\n",
    "//execute: ./class_return\n",
    "#include <iostream>\n",
    "using std::cout;\n",
    "\n",
    "class point {\n",
    "public:\n",
    "    int x;\n",
    "    int y;\n",
    "};\n",
    "\n",
    "point squarePoint(point t) {\n",
    "    cout << \"in call: t address \" << &t << \"\\n\";\n",
    "    point result;\n",
    "    result.x = t.x * t.x;\n",
    "    result.y = t.y * t.y;\n",
    "    cout << \"in call: result address \" << &result << \"\\n\";\n",
    "    return result;\n",
    "}\n",
    "\n",
    "int main(int argc, char *argv[]) {\n",
    "    point p;\n",
    "    \n",
    "    p.x = 1;\n",
    "    p.y = 5;\n",
    "\n",
    "    cout << \"(x,y): (\" << p.x << \", \" << p.y << \")\" << \"\\n\";\n",
    "    point p2;\n",
    "    cout << \"before call: p2 address \" <<  &p2 << \"\\n\";\n",
    "    p2 = squarePoint(p);\n",
    "    cout << \"(x,y): (\" << p2.x << \", \" << p2.y << \")\" << \"\\n\";\n",
    "    cout << \"after call: p2 address \" << &p2 << \"\\n\";\n",
    "\n",
    "    return EXIT_SUCCESS;\n",
    "}\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To avoid copying large objects, we can also use the reference mechanism, but apply the `const` keyword to prevent any changes to the original object. Often, this can be a good practice to prevent unintended side effects. You should try altering `t` in `squarePoint` (e.g., add the line`    t.x = 10;`) to see how the compiler handles this situation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "cpp"
    }
   },
   "outputs": [],
   "source": [
    "//filename: point_const.cpp\n",
    "//compile: g++ -std=c++17 -o point_const point_const.cpp\n",
    "//execute: ./point_const\n",
    "#include <iostream>\n",
    "using std::cout;\n",
    "\n",
    "class point {\n",
    "public:\n",
    "    int x;\n",
    "    int y;\n",
    "};\n",
    "\n",
    "point squarePoint(const point &t) {\n",
    "    cout << \"in call: t address \" << &t << \"\\n\";\n",
    "    point result;\n",
    "    result.x = t.x * t.x;\n",
    "    result.y = t.y * t.y;\n",
    "    cout << \"in call: result address \" << &result << \"\\n\";\n",
    "    return result;\n",
    "}\n",
    "\n",
    "int main(int argc, char *argv[]) {\n",
    "    point p;\n",
    "    \n",
    "    p.x = 1;\n",
    "    p.y = 5;\n",
    "\n",
    "    cout << \"(x,y): (\" << p.x << \", \" << p.y << \")\" << \"\\n\";\n",
    "    point p2;\n",
    "    cout << \"before call: p address \" <<  &p << \"\\n\";\n",
    "    p2 = squarePoint(p);\n",
    "    cout << \"(x,y): (\" << p2.x << \", \" << p2.y << \")\" << \"\\n\";\n",
    "    cout << \"after call: p2 address \" << &p2 << \"\\n\";\n",
    "\n",
    "    return EXIT_SUCCESS;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note: After attempting to modify t and compiling the program, the compiler should produce the error message \"error: assignment of member ‘point::x’ in read-only object\"."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Scope\n",
    "Scope is pretty much the same between C++ and Python. The biggest difference is that once a code block ends for C++, any variable declared within the scope goes out of scope, even if you are still in the same function. You can think that every `{ }` block forms a level of scope, and you can access variables defined by other blocks that enclose that block."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "cpp"
    }
   },
   "outputs": [],
   "source": [
    "//filename: scope.cpp\n",
    "//compile: g++ -std=c++17 -o scope scope.cpp\n",
    "//execute: ./scope\n",
    "#include <iostream>\n",
    "using std::cout;\n",
    "\n",
    "void swap(int a, int b) {\n",
    "    int t = a;\n",
    "    a = b;\n",
    "    b = t;\n",
    "    cout << \"in swap: (a, b) = (\" << a << \", \" << b << \")\" << \"\\n\";\n",
    "}\n",
    "\n",
    "int main(int argc, char *argv[]) {\n",
    "    int a = 10;\n",
    "    int b = 15;\n",
    "    cout << \"before swap: (a, b) = (\" << a << \", \" << b << \")\" << \"\\n\";\n",
    "    swap(a,b);\n",
    "    cout << \"after swap: (a, b) = (\" << a << \", \" << b << \")\" << \"\\n\";\n",
    "    if (1) {\n",
    "        int a = 33;\n",
    "        int c = 50;\n",
    "        cout << \"Within if: a=\" << a << \"\\n\";\n",
    "        cout << \"Within if: b=\" << b << \"\\n\";\n",
    "        cout << \"Within if: c=\" << c << \"\\n\";\n",
    "    }\n",
    "    cout << \"After if: a=\" << a << \"\\n\";\n",
    "    cout << \"Value of c: \" << c << \"\\n\";  // This line causes a compilation error. \n",
    "                                          // Try running to see the error, comment out the line, and run again \n",
    "                                          // Note: This is an exeption, to always work top-down with errors\n",
    "                                          //       However, note that the first message is a warning, not an error.\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "## Other Notes\n",
    "- C++ does support function overloading as long as the parameter types differ. With Python, the second function definition replaces the first definition.\n",
    "- Function naming rules follow that for variables:\n",
    "  - must start with a letter or underscore followed by any number of letters, numbers, or underscores.\n",
    "  - C++ does not have a universal guideline for function names.\n",
    "  - Both camelCase and underscores (as in Python) are used to distinguish words.\n",
    "- While it will not be covered in this class, functions can have a variable number of arguments in C++.\n",
    "- Functions can take default arguments\n",
    "- Functions must be declared before their use in a file.  You can declare a function, but not fully define the implementation by replacing the function body (including the curly braces with a semi-colon:\n",
    "  ```c++\n",
    "  int add(int a, int b = 0);    // declaration.  b has a default value of 0\n",
    "\n",
    "  // some code\n",
    "\n",
    "  int add(int a, int b = 0) {   // definition\n",
    "      return a + b;\n",
    "  }\n",
    "  ```\n",
    "- Unlike Python, C++ cannot nest functions."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Suggested LLM Prompts\n",
    "\n",
    "* Explain the basic structure and syntax of functions in C++. Cover the function declaration, function definition, function prototypes, and function calls. Discuss the purpose of functions, their role in modular programming, and the benefits of code reusability and organization.\n",
    "* Discuss function parameters and arguments in C++. Explain the difference between formal parameters (in the function declaration) and actual arguments (in the function call). Discuss pass-by-value and pass-by-reference parameter passing mechanisms, and their implications on memory usage and performance.\n",
    "* Explain how it allows multiple functions with the same name to coexist, as long as they have different parameter lists. Discuss the rules and limitations of function overloading, and provide examples of overloaded functions with different parameter types, number of parameters, and parameter order.\n",
    "* Explain how default arguments allow functions to have optional parameters with predetermined values. Discuss the rules and limitations of using default arguments, and provide examples of how they can be combined with function overloading to create flexible and expressive interfaces.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Review Questions\n",
    "1. Compare and contrast functions in Python and C++.\n",
    "2. What are the four different parts of a function?\n",
    "3. Explain how C++ uses call by value. Why did this create issues with the swap function?\n",
    "4. Describe what the line\n",
    "    ```c++\n",
    "    char foo(int x)\n",
    "    ```\n",
    "    means in a function definition:\n",
    "5. How do we syntactically distinguish between a function declaration and a function definition?\n",
    "6. Why is it a good idea to initialize variables as they are declared?\n",
    "7. What is the difference between pass-by-value and pass-by-reference?\n",
    "\n",
    "[answers](answers/rq-05-answers.md)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
