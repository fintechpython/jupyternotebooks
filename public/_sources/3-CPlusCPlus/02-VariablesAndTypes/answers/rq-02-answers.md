# CPlusPlus / Variables And Types
1. *What is a variable?*

   A variable in C++ is a named storage location in the computer's memory where data can be stored, modified, and retrieved. Each variable has a specific type, which determines the size and layout of the memory storage, the range of values that can be stored, and the set of operations that can be applied to the variable.

2. *What is an object? How does this differ from Python?*

   An object is a region of memory with a type that specifies what kind of information can be placed in it. Since Python is dynamically typed, object types are determined at runtime and need not be declared.

3. *What are typical sizes for a char, an int, and a double?*

   In C++, the typical size of a char is 1 byte, an int is usually 4 bytes, and a double is typically 8 bytes.

4. *Why can conversion from double to int be a bad thing?*

   To convert a double to an int a narrowing conversion is required where information can get lost, which describes an unsafe conversion.

5. *What are the main differences between a `struct` and a `class` in C++?*

   `struct` are used in C++ primarily for data structures that mainly contain data and have little or no associated behaviors (methods). In contrast, `class` are used for data structures that encapsulate both data and behaviors, making them suitable for implementing more complex abstractions in object-oriented programming.

6. *In what scenarios might you use a double instead of a float?*

   You might choose to use a double instead of a float in scenarios where the precision of the calculations is critical and the additional memory usage is not a constraint. A double provides roughly double the precision of a float, allowing for more accurate results and less rounding error.
