# Smart Pointers

Managing dynamic memory in C++ can be error-prone, leading to issues like memory leaks, dangling pointers, and undefined behavior. Smart pointers, introduced in C++11 and enhanced in later standards, provide automatic and safer memory management by encapsulating raw pointers with ownership semantics. 

Smart pointers are template classes that provide automatic memory management for dynamically allocated objects. They ensure that resources are properly released when they are no longer needed, thereby reducing memory leaks and other related issues.

**Key Benefits:**
- **Automatic Resource Management:** Automatically deallocate memory when the smart pointer goes out of scope.
- **Exception Safety:** Ensure resources are released even when exceptions are thrown.
- **Ownership Semantics:** Clearly define ownership and sharing of resources.

<a name="types"></a>
## 2. Types of Smart Pointers

C++ provides several smart pointer types, each with distinct ownership and lifetime semantics:

### `std::unique_ptr`

`std::unique_ptr` represents exclusive ownership of a dynamically allocated object. Only one `unique_ptr` can own a particular resource at a time.

**Key Features:**
- Non-copyable but movable.
- Lightweight with no overhead beyond the raw pointer.
- Suitable for implementing RAII (Resource Acquisition Is Initialization) for single ownership.

**Example:**

```cpp
//filename: unique.cpp
//compile: g++ -std=c++20 -o unique unique.cpp
//execute: ./unique
#include <memory>
#include <iostream>

struct Resource {
    Resource() { std::cout << "Resource Acquired\n"; }
    ~Resource() { std::cout << "Resource Destroyed\n"; }
    void sayHello() { std::cout << "Hello from Resource\n"; }
};

int main() {
    // Creating a unique_ptr
    std::unique_ptr<Resource> ptr1 = std::make_unique<Resource>();
    ptr1->sayHello();

    // Transferring ownership
    std::unique_ptr<Resource> ptr2 = std::move(ptr1);
    if (!ptr1) {
        std::cout << "ptr1 is now empty\n";
    }

    return 0;
}
```

**Output:**
```
Resource Acquired
Hello from Resource
ptr1 is now empty
Resource Destroyed
```
As you step through the code, notice that a Resource object is created with a pointer assigned to `ptr1`.  The code then 
uses that resource by calling `sayHello()`. Ownership is then moved to another unique pointer and the original unique pointer
no longer has a valid reference to the object. The object is deallocated when the owner unique pointer (`ptr2`) goes out of 
scope.


## `std::shared_ptr`

`std::shared_ptr` allows multiple pointers to share ownership of a single resource. It maintains a reference count to manage the resource's lifetime, destroying it when the last `shared_ptr` is destroyed or reset.

**Key Features:**
- Copyable and assignable.
- Reference counting overhead.
- Suitable for scenarios where multiple parts of a program need to share access to a resource.

**Example:**

```cpp
//filename: shared.cpp
//compile: g++ -std=c++20 -o shared shared.cpp
//execute: ./shared
#include <memory>
#include <iostream>

struct Resource {
    Resource() { std::cout << "Resource Acquired\n"; }
    ~Resource() { std::cout << "Resource Destroyed\n"; }
    void sayHello() { std::cout << "Hello from Resource\n"; }
};

int main() {
    std::shared_ptr<Resource> ptr1 = std::make_shared<Resource>();
    {
        std::shared_ptr<Resource> ptr2 = ptr1; // Shared ownership
        ptr2->sayHello();
        std::cout << "Reference Count: " << ptr1.use_count() << "\n";
    } // ptr2 goes out of scope

    std::cout << "Reference Count after ptr2 is destroyed: " << ptr1.use_count() << "\n";
    return 0;
}
```

**Output:**
```
Resource Acquired
Hello from Resource
Reference Count: 2
Reference Count after ptr2 is destroyed: 1
Resource Destroyed
```

###  `std::weak_ptr`

`std::weak_ptr` is a non-owning weak reference to a `std::shared_ptr`. It does not affect the reference count, thereby preventing circular dependencies. To access the managed object, `weak_ptr` must be converted to `shared_ptr` using the `lock()` method.

**Key Features:**
- Does not own the resource.
- Prevents reference cycles that can lead to memory leaks.
- Can check if the resource still exists before accessing.

**Example:**

```cpp
//filename: weak.cpp
//compile: g++ -std=c++20 -o weak weak.cpp
//execute: ./weak
#include <memory>
#include <iostream>

struct Resource {
    Resource() { std::cout << "Resource Acquired\n"; }
    ~Resource() { std::cout << "Resource Destroyed\n"; }
    void sayHello() { std::cout << "Hello from Resource\n"; }
};

int main() {
    std::shared_ptr<Resource> ptr1 = std::make_shared<Resource>();
    std::weak_ptr<Resource> weakPtr = ptr1;

    {
        std::shared_ptr<Resource> ptr2 = weakPtr.lock();
        if (ptr2) {
            ptr2->sayHello();
            std::cout << "Reference Count: " << ptr1.use_count() << "\n";
        }
    }

    ptr1.reset(); // Destroy the resource

    std::shared_ptr<Resource> ptr3 = weakPtr.lock();
    if (!ptr3) {
        std::cout << "Resource no longer exists.\n";
    }

    return 0;
}
```

**Output:**
```
Resource Acquired
Hello from Resource
Reference Count: 2
Resource Destroyed
Resource no longer exists.
```

### `std::auto_ptr` (Deprecated)

`std::auto_ptr` was the first smart pointer introduced in C++. The type was deprecated in C++11 and removed in C++17 due to its flawed copy semantics, which can lead to unexpected behavior.

**Note:** Avoid using `std::auto_ptr`. Prefer `std::unique_ptr` instead.


## Best Practices

- **Prefer Smart Pointers Over Raw Pointers:** Use smart pointers to manage dynamic memory instead of raw pointers to leverage automatic resource management.
  
- **Use `std::make_unique` and `std::make_shared`:** These functions provide exception safety and can optimize memory allocation.

- **Choose the Right Smart Pointer:**
  - Use `std::unique_ptr` for exclusive ownership.
  - Use `std::shared_ptr` when multiple owners are necessary.
  - Use `std::weak_ptr` to break reference cycles with `std::shared_ptr`.

- **Avoid Cyclic References with `std::shared_ptr`:** Use `std::weak_ptr` to prevent memory leaks caused by circular dependencies.

- **Understand Ownership Semantics:** Clearly define which part of the code owns the resource to avoid confusion and potential bugs.

- **Avoid Using `std::shared_ptr` When Not Needed:** `std::shared_ptr` incurs overhead due to reference counting. Use `std::unique_ptr` when exclusive ownership suffices.

<a name="pitfalls"></a>
## Common Pitfalls

- **Circular References:**
  - **Issue:** Two `std::shared_ptr` instances referencing each other create a cycle, preventing the reference count from reaching zero.
  - **Solution:** Use `std::weak_ptr` to break the cycle.

  **Example:**

```cpp
//filename: circular.cpp
//compile: g++ -std=c++20 -o circular circular.cpp
//execute: ./circular
#include <memory>
#include <iostream>

struct B; // Forward declaration

struct A {
  std::shared_ptr<B> b_ptr;
  ~A() { std::cout << "A destroyed\n"; }
};

struct B {
  std::shared_ptr<A> a_ptr; // Causes circular reference
  ~B() { std::cout << "B destroyed\n"; }
};

int main() {
  auto a = std::make_shared<A>();
  auto b = std::make_shared<B>();
  a->b_ptr = b;
  b->a_ptr = a; // Circular reference

  // Neither A nor B will be destroyed
  return 0;
}
```

**Solution with `std::weak_ptr`:**

```cpp
//filename: weak_cycle.cpp
//compile: g++ -std=c++20 -o weak_cycle weak_cycle.cpp
//execute: ./weak_cycle
#include <memory>
#include <iostream>

struct B; // Forward declaration

struct A {
  std::shared_ptr<B> b_ptr;
  ~A() { std::cout << "A destroyed\n"; }
};

struct B {
  std::weak_ptr<A> a_ptr; // Breaks the cycle
  ~B() { std::cout << "B destroyed\n"; }
};

int main() {
  auto a = std::make_shared<A>();
  auto b = std::make_shared<B>();
  a->b_ptr = b;
  b->a_ptr = a; // No circular reference

  return 0;
}
```

**Output:**
```
A destroyed
B destroyed
```

- **Dangling `std::weak_ptr`:** Accessing the managed object after it has been destroyed can lead to undefined behavior.
  
  **Solution:** Always check if the `weak_ptr` can be locked before using it.

  **Example:**

  ```cpp
  std::weak_ptr<int> weakPtr;
  {
      auto sharedPtr = std::make_shared<int>(42);
      weakPtr = sharedPtr;
  } // sharedPtr goes out of scope

  if (auto ptr = weakPtr.lock()) {
      std::cout << "Value: " << *ptr << "\n";
  } else {
      std::cout << "Resource no longer exists.\n";
  }
  ```

- **Mixing Smart Pointers and Raw Pointers:**
  - **Issue:** Managing the same resource with both smart and raw pointers can lead to double deletion or memory leaks.
  - **Solution:** Ensure that ownership is clearly defined and avoid sharing ownership between smart and raw pointers.


## Conclusion

Smart pointers are essential tools in modern C++ for managing dynamic memory safely and efficiently. By understanding the different types of smart pointers (`std::unique_ptr`, `std::shared_ptr`, and `std::weak_ptr`), their appropriate use cases, and best practices, you can write more robust and maintainable C++ code. Always prefer smart pointers over raw pointers for dynamic memory management to leverage their automatic resource management capabilities and reduce the risk of memory-related bugs.

## Further Reading

- [C++ Smart Pointers - cppreference.com](https://en.cppreference.com/w/cpp/memory)
- [Effective Modern C++ by Scott Meyers](https://www.oreilly.com/library/view/effective-modern-c/9781491908419/)
- [C++ Core Guidelines](https://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines#Resource-management)