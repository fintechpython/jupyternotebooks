{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Exceptions\n",
    "As introductory students developing programs for class assignments, validating input and terminating upon invalid input or terminating after detecting runtime errors usually suffices.  However, for robust, production-quality code, we need to detect, respond, and recover from a variety of errors. We start to develop robust programs through rigorous input validation, but that validation may not cover (or prevent) the various run-time errors that can occur.  For instance, if a user selects a given input file, we only know it is valid after we complete parsing/processing it. A number of issues may arise along the way and it's nearly impossible to prevent all such problems.\n",
    "Exceptions are run-time errors that occur outside of the normal functioning of a program.  We use exception handling when one part of a program detects a problem it cannot resolve. That part signals (by throwing an exception) that an issue has occurred.  Control will then pass to another part of the program that can handle that exception.  While typical examples will show the two parts co-located in a `try-catch `statement, the detecting error may occur within functions called by the statements in the `try` block. The exception basically jumps to the point in the call stack that can handle the handle - this creates a clean separation between error detection and error recovery/handling.\n",
    "Similar to Python, C++ provides exception handling to deal with unusual conditions during the runtime of a program in a controlled and systematic manner. When an exception occurs, the normal flow of the program is interrupted, and control is transferred to a special code block called a handler. Our goal in these handlers is to return the program to a valid state or to gracefully exit the program if that is impossible.\n",
    "Python and C++ have similar syntax for exception handling. C++ syntax -\n",
    "```c++\n",
    "try {\n",
    "  // code that may throw exception(s)\n",
    "} catch (const ExceptionType1 &ex1) {\n",
    "  // code to handle ExceptionType1\n",
    "} catch (const ExceptionType2 &ex2) {\n",
    "  // code to handle ExceptionType2\n",
    "} // … more catch blocks as needed\n",
    "catch (...) {\n",
    "  // Catch-all handler for other unhandled exceptions\n",
    "  std::cerr << \"Unknown exception caught\" << std::endl;\n",
    "}\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "cpp"
    }
   },
   "outputs": [],
   "source": [
    "//filename: divisionzero.cpp\n",
    "//compile: g++ -std=c++17 -o divisionzero divisionzero.cpp\n",
    "//execute: ./divisionzero\n",
    "#include <iostream>\n",
    "\n",
    "int main(int argc, char *argv[]) {\n",
    "    try {\n",
    "        \n",
    "        int divisor = 0;\n",
    "        if (divisor == 0) {\n",
    "            throw std::runtime_error(\"Division by zero exception\");   // Note: throwing a value object here\n",
    "        }\n",
    "        int result = 10 / divisor;  // this statement never executes.  Creates a \"float-point\" exception, but outside of C++\n",
    "                                    // can not detect this error\n",
    "    }\n",
    "    catch (const std::runtime_error &e) { // note output to cout instead of cerr to display within docable.\n",
    "        std::cout << \"Runtime error: \" << e.what() <<  \"\\n\";\n",
    "        return EXIT_FAILURE;\n",
    "    } catch (...) {\n",
    "        std::cout << \"Unknown exception caught\" <<  \"\\n\";\n",
    "        return EXIT_FAILURE;\n",
    "    }\n",
    "    return EXIT_SUCCESS;\n",
    "}\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "As you can see in the example, when we detect an exceptional condition, we \"throw\" an exception with the `throw` keyword:\n",
    "```c++\n",
    "throw runtime_error(\"A problem occurred.\");\n",
    "```\n",
    "Here, `runtime_error` is a standard exception type provided by the C++ Standard Library. We can throw objects of any data type as exceptions, including built-in, custom, or library types. For example, the C++ STL throws an `out_of_range` exception in the `vector<>:at()` method if the provided index is invalid.\n",
    "\n",
    "One of the nuances when converting string values is that the parser will stop considering input when an invalid character is encountered.  As such, if you want to ensure that the entire string has been processed, we need to check how many characters were processed as compared to the length of the string. Note: With modern C++, we do not need to indicate that a function throws an exception - we do use the keyword `noexcept` if the function is guaranteed not throw an exception.\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "cpp"
    }
   },
   "outputs": [],
   "source": [
    "//filename: convert.cpp\n",
    "//compile: g++ -std=c++17 -o convert conversion.cpp\n",
    "//execute: ./convert\n",
    "#include <iostream>\n",
    "#include <string>\n",
    "\n",
    "int convertInt(std::string s) {\n",
    "    std::size_t idx = 0;\n",
    "    int result = std::stoi(s,&idx);\n",
    "    if (idx != s.size()) {\n",
    "        throw std::invalid_argument(\"unprocessed input: \"+s);\n",
    "    }\n",
    "    return result; \n",
    "}\n",
    "\n",
    "int main() {\n",
    "    try {\n",
    "        std::cout << convertInt(\"100\") << \"\\n\";\n",
    "        std::cout << convertInt(\"100.4\") << \"\\n\";\n",
    "    } catch (const std::invalid_argument& a) {\n",
    "        std::cerr << \"Invalid argument: \" << a.what() << \"\\n\";\n",
    "        return EXIT_FAILURE;\n",
    "    } catch (const std::out_of_range& r) {\n",
    "        std::cerr << \"out of range of double: \" << r.what() << \"\\n\";\n",
    "        return EXIT_FAILURE;\n",
    "    }\n",
    "    return EXIT_SUCCESS;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exception Propagation\n",
    "If an exception is thrown but not caught in a particular scope, the exception propagates up to higher levels of the call stack until it is caught or until it reaches `main`. If it gets to `main` without being caught, the program will terminate. As the exception propagates through the call stack, those corresponding functions exit/go out of scope. Any declared objects within those functions will have their corresponding destructors called. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Standard Exceptions\n",
    "The C++ library defines several exceptions used to report issues within the library.  As from above, the library can detect these situations, but separate code must exist to handle and recover from these exceptions.  Here are some of these defined exceptions:\n",
    "| <b>Exception Class</b>|<b>Purpose</b> |\n",
    "|--|--|\n",
    "|`exception`|base class - most general kind of problem|\n",
    "|`runtime_error`|represents problems that can only be detected at runtime.|\n",
    "|`overflow_error`|Computation overflowed (number too great for the underlying type)|\n",
    "|`underflow_error`|occurs when a floating-point operation results in a value that is closer to zero than the smallest representable positive value for the data type being used, and the value cannot be represented accurately. |\n",
    "|`out_of_range`|thrown when an argument value is out of the valid range.|\n",
    "|`invalid_argument`|thrown when an invalid argument is passed to a function.|\n",
    "\n",
    "These classes are defined in `stdexcept` - you will need to include that header to be able to reference these classes. \n",
    "\n",
    "While these errors have been defined, C++ often leaves it to the program to detect these situations and throw the appropriate exception."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exception Guarantees\n",
    "\n",
    "In C++, exception safety guarantees describe how an operation behaves when an exception occurs, ensuring that the program remains in a valid state after the exception. There are **three main levels** of exception safety guarantees: **basic**, **strong**, and **no-throw**. Each guarantee specifies a level of safety for exception handling, which helps developers write robust code that can handle errors effectively.\n",
    "\n",
    "### Basic Exception Safety Guarantee\n",
    "The **basic guarantee** ensures that:\n",
    "- **Invariants are preserved**: The program remains in a valid state after an exception is thrown, meaning no resource leaks or corruption occurs.\n",
    "- However, **the state of the object may change**, and the operation may have performed some modifications before the exception was thrown.\n",
    "- No resources are leaked (e.g., memory, file handles), and cleanup is done correctly.\n",
    "\n",
    "**Example**:\n",
    "```cpp\n",
    "void some_operation() {\n",
    "    std::vector<int> vec = {1, 2, 3};\n",
    "    try {\n",
    "        vec.push_back(some_function());  // Exception might occur in some_function()\n",
    "    } catch (...) {\n",
    "        // The vector may be in an altered but valid state after the exception\n",
    "        // The object (vec) is still usable\n",
    "    }\n",
    "}\n",
    "```\n",
    "\n",
    "### Strong Exception Safety Guarantee\n",
    "The **strong guarantee** ensures that:\n",
    "- **The operation has no side effects if an exception is thrown**: If an exception occurs, the program will roll back to its original state as if the operation never happened.\n",
    "- The object remains unchanged when an exception is thrown.\n",
    "\n",
    "This level of guarantee is useful when it's important that either the operation succeeds completely, or nothing changes at all (providing **\"commit or rollback\"** behavior).\n",
    "\n",
    "**Example**:\n",
    "```cpp\n",
    "void some_operation() {\n",
    "    std::vector<int> vec = {1, 2, 3};\n",
    "    std::vector<int> backup = vec;  // Create a backup of the original state\n",
    "    try {\n",
    "        vec.push_back(some_function());  // If this throws, vec is unchanged\n",
    "    } catch (...) {\n",
    "        vec = backup;  // Roll back to the original state if an exception occurs\n",
    "    }\n",
    "}\n",
    "```\n",
    "\n",
    "Many STL algorithms, like `std::vector::push_back`, offer the strong guarantee when exceptions are thrown by the copy constructor or assignment operator of the elements.\n",
    "\n",
    "### No-Throw (Nothrow) Guarantee\n",
    "The **no-throw guarantee** ensures that:\n",
    "- The operation is **guaranteed never to throw exceptions**.\n",
    "- This is the strongest guarantee and is crucial for certain operations, such as destructors or `swap` operations, where exceptions should not be thrown under any circumstances.\n",
    "\n",
    "If an operation provides the no-throw guarantee, it ensures that it can be safely used even in contexts where exceptions cannot be handled.\n",
    "\n",
    "**Example**:\n",
    "```cpp\n",
    "void some_operation() noexcept {\n",
    "    std::vector<int> vec = {1, 2, 3};\n",
    "    vec.clear();  // clear() is guaranteed not to throw exceptions\n",
    "}\n",
    "```\n",
    "The **`noexcept`** specifier indicates that a function will not throw exceptions. This is especially important in critical code sections, like destructors, where throwing exceptions could lead to undefined behavior.\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Notes\n",
    "- Do not throw exceptions from destructors, as this can cause unexpected behavior.  (Destructors will be covered in classes.)\n",
    "- Use exceptions for exceptional, non-routine error conditions.\n",
    "- Catch exceptions by reference (preferably `const` reference). This provides a number of benefits:\n",
    "  - allows us to use polymorphic behavior when accessing the exceptions (polymorphism will be covered later)\n",
    "  - signifies that will not change the exception object.\n",
    "  - prevents \"object slicing\" when a copy of the exception is made if the exception is not a reference. \n",
    "    Entering into a catch block functions similarly to calling a function.  Object slicing occurs when an \n",
    "    object of a subclass is assigned to an instance of a base class - we lose access to the state and \n",
    "    behavior defined in the subclass. The copy function in the base class only knows about its state, not those of any subclasses."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Suggested LLM Prompts\n",
    "* Introduce the basics of exception handling in C++. Explain what exceptions are, why they're used, and the syntax for try, catch, and throw. Cover the difference between exceptions and traditional error handling methods. Provide examples of common scenarios where exceptions are useful.\n",
    "* Create a guide on best practices for exception handling in C++. Cover topics such as when to use exceptions versus other error handling methods, how to design exception-safe interfaces, and common pitfalls to avoid. Discuss the performance implications of exceptions and how to minimize their impact. Provide examples of well-designed exception handling in larger programs.\n",
    "* Explain nested exceptions and exception propagation in C++. Discuss how exceptions are propagated up the call stack and how to handle multiple levels of exceptions. Cover the use of std::nested_exception and std::rethrow_if_nested. Provide examples of complex exception scenarios and how to handle them effectively.\n",
    "* Explore exception handling in constructors and destructors in C++. Explain the special considerations for these functions, including the potential for resource leaks and the behavior of partially constructed objects. Discuss techniques for writing exception-safe constructors and destructors. Provide examples of common pitfalls and their solutions.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Review Questions\n",
    "\n",
    "1. Name four major types of errors and briefly define each one.\n",
    "2. What is the purpose of exception handling in C++?\n",
    "3. What happens if an exception is thrown and not caught?\n",
    "4. What is exception propagation?\n",
    "5. What are the three levels of exception safety guarantees?\n",
    "\n",
    "[answers](answers/rq-08-answers.md)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
