# CPlusPlus / Exceptions
1. *Name four major types of errors and briefly define each one.*

   1. Syntax Errors: Syntax errors occur when the code violates the rules of the programming language's syntax. These are typically caught   by the compiler or interpreter before the program runs.
   2. Runtime Errors: Runtime errors occur during program execution. These errors cause the program to terminate abnormally or produce unexpected results.
   3. Logical Errors: Logical errors, also known as semantic errors, occur when the program runs without crashing but produces incorrect results. These errors are due to flaws in the program's logic or algorithm. They can be the most difficult to detect as the program appears to run normally.
   4. Compilation Errors: Compilation errors occur during the compilation process when the source code is being translated into machine code. These are typically caused by violations of the language's rules that the compiler can detect, such as undeclared variables, type mismatches, or incorrect function calls.

2. *What is the purpose of exception handling in C++?*

   To handle runtime errors and exceptional situations in a structured manner, separating error-handling code from normal program logic.

3. *What happens if an exception is thrown and not caught?*

   The program terminates abnormally, calling std::terminate().

4. *What is exception propagation?*

   The process where an exception is passed up the call stack until it's caught or the program terminates.

5. *What are the three levels of exception safety guarantees?*

   Basic guarantee, strong guarantee, and no-throw guarantee.
