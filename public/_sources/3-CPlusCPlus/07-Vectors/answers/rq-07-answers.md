# CPlusPlus / Vectors
1. *Describe what push_back() does to a vector.*

   The operation push_back() adds a new element to a vector. The new element becomes the last element of the vector.

2. *What is the difference between size() and capacity() of a vector?*
   
   size() returns the number of elements in the vector, while capacity() returns the number of elements the vector can hold without reallocation.

3. *What does the reserve() function do?*
   
   It increases the capacity of the vector to a specified amount without adding any elements.

4. *How do you insert an element at a specific position in a vector?*

   Using the insert() method. Example: vec.insert(vec.begin() + position, value);

5. *What's the difference between vec[i] and vec.at(i) when accessing elements?*

   vec[i] doesn't perform bounds checking, while vec.at(i) does and throws an out_of_range exception if i is out of bounds.

6. *What is the iterator and how is it used with vectors?*

   An iterator is like a pointer used to access the elements of a vector. You can use iterators to navigate through the elements, modify them, or pass them to functions that require an iterator range.
