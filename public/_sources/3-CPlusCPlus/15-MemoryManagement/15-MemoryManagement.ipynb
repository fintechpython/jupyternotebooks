{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Memory Management\n",
    "\n",
    "Programmers must develop code to appropriately manage a variety of different resources: memory, files, database queries, etc. \n",
    "\n",
    "In Python, the interpreter handles much of the memory management for developers automatically. As everything is an object, variables are references to the underlying values and storage locations allocated on the heap. The Python Interpreter can detect when objects can no longer be referenced and release the memory used by those objects. \n",
    "\n",
    "However, with C++, Programmers must be aware of the different ways variables may be placed into memory based on how and where the variables are declared.\n",
    "\n",
    "The C++ standard defines these <a href=\"http://en.cppreference.com/w/cpp/language/storage_duration\">storage durations</a>:\n",
    "\n",
    "- __static__:  The storage for the object (aka, variable) is allocated when the program begins and deallocated when the program ends. Only one instance of the object exists. All objects declared at namespace scope (including global namespace) have this storage duration.  Corresponds to the two data regions below.\n",
    "- __automatic:__ The storage for the object is allocated at the beginning of the enclosing code block and deallocated at the end. All local objects have this storage duration except if a keyword such as static or extern specifies otherwise.\n",
    "- __dynamic:__ The storage for the object is allocated and deallocated upon request by using dynamic memory allocation (e.g., `new`, `delete`).\n",
    "- __thread local:__ Allocated when the thread begins and deallocated when the thread ends"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In specific implementation environments, C++ divides a program's memory into 5 regions:\n",
    "\n",
    "1. __Text:__  The text(code) segment contains the executable instructions of a program.  Typically, this is placed below the heap and stack memory errors to prevent overflow issues from overwriting the information contained within it.  However, the segment may also be set as read-only memory to prevent any type of modification.\n",
    "2. __Initialized Data:__ The initialized data segment contains the global and static variables that have been explicitly initialized with the code.  Note: some consider this segment and the following to be the same.\n",
    "3. __Uninitialized Data:__ This segment contains declared variables from the program source code, but have not been initialized.  The operating system (program loader) will initialize data in this segment to zero before the program starts executing.  <a href=\"https://en.wikipedia.org/wiki/.bss\">BSS Details</a>\n",
    "4. __Stack:__ The stack segment contains the program stack.  This structure grows downward in memory as function calls are made.  With each function call, the compiler determines how much space is required to hold the variables defined within the function (i.e., those variables with _automatic _storage duration).  The stack then grows by that space.  If additional space is needed by internal blocks or arrays whose sizes are unknown until execution time (the size of arrays can be determined by variables), the stack frame can be further extended.  Once a function exits, the local memory used by that function is no longer needed and is released - the stack shrinks in space.  This is why we cannot return a locally declared array - the memory reference that pointed to that array is no longer valid.  The command-line arguments are automatically placed into the stack as the `main()`function is called.\n",
    "5. __Heap:__ The heap is where memory is dynamically allocated.  With C, programmers can directly manipulate this memory space with functions such as `malloc()`, `realloc()`, and `free()`.  In C++, we can create objects in this space with the `new` keyword. The compiler then handles allocating the space appropriately based upon the type of the object.\n",
    "\n",
    "![](images/MPVnmHI.png)\n",
    "\n",
    "As mentioned, every variable in Python is reference and the underlying objects are allocated on the heap. \n",
    "\n",
    "In C++ we different types of variables (value, reference, and pointer) as well as different locations where those variables may be placed."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Value Variables\n",
    "\n",
    "Value variables are probably the most commonly used variable type in C++.  The following code demonstrate creating value variables for both normal types (int, float, char, etc) as well as for objects.\n",
    "\n",
    "```c++\n",
    "int i = 5;\n",
    "char c = 'a';\n",
    "Point p;\n",
    "Point p2(4,6);\n",
    "\n",
    "```\n",
    "\n",
    "As the C++ compiler knows the exact size of these variables, the compiler can allocate space in the appropriate location.  Within Python, the interpreter cannot determine the memory needed for an object due to the dynamic nature (e.g., adding additional properties to an object at runtime) of the language and, hence, must place objects on the heap.  Additionally, the Python interpreter must utilize additional data structures to keep track of what's exactly in an object.\n",
    "\n",
    "For these variables declared within a function or code block, C++ will allocate memory on the stack.  For global variables, the compiler allocates space on the data segment.\n",
    "\n",
    "As C++ passes variables by value, a copy of the object is made when function calls are made using the class's copy constructor.  By default, C++ provides a copy constructor that makes a shallow copy of the object as any reference or pointers only have their value copied (and not the underlying object(s)/data. The pass by value also means that for larger objects as substantial amount of work must be done to copy those objects.\n",
    "\n",
    "In Python, the assignment statement just copies the reference value to another variable.\n",
    "\n",
    "In C++, the assignment operator actually uses the _copy constructor_ to perform the work.\n",
    "\n",
    "```c++\n",
    "Point a(1,2);\n",
    "Point b = a;\n",
    "\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Reference Variables\n",
    "\n",
    "Reference variables are declared with an `&` after the type name. The variable provides a new name, but not a new object. The reference variable is an alias to existing object. Once the variable has been assigned, the \"alias\"/value is fixed.  We cannot re-associate with another variable.  Minus the inability to reassign, the variable type is most similar to Python's model. Since the reference is fixed, we cannot assign `nullptr` or `NULL` to a reference variable. Additionally, we cannot have references to references, nor can we manipulate references as we can with pointers - \"reference arithmetic\" does not exist.\n",
    "\n",
    "In the following code, we create the simple Point class and then demonstrate creating two reference variables `c` and `d` to `p`. Unlike pointers, the references are automatically dereferenced (i.e., we do not to need to explicitly use the `*` dereferencing operator)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "cpp"
    }
   },
   "outputs": [],
   "source": [
    "//filename: pointer_reference.cpp\n",
    "//compile: g++ -std=c++17 -o pointer_reference pointer_reference.cpp\n",
    "//execute: ./pointer_reference\n",
    "#include <iostream>\n",
    "using namespace std;\n",
    "\n",
    "class Point {\n",
    "private:\n",
    "    double x, y;\n",
    "public:\n",
    "    Point(double initialX = 0.0, double initialY = 0.0) : x{initialX}, y{initialY} {}\n",
    "    double getX() const { return x; }\n",
    "    double getY() const { return y; }\n",
    "    void setX(double val) { x = val; }\n",
    "    void setY(double val) { y = val; }\n",
    "};\n",
    "\n",
    "int main(int argc, char *argv[]) {\n",
    "    Point p(5,2);\n",
    "    Point& c = p;\n",
    "    Point& d(p);\n",
    "    c.setX(1);\n",
    "\n",
    "    cout << \"p: \" << p.getX() << \",\" << p.getY() << endl;\n",
    "    cout << \"c aka p: \" << c.getX() << \",\" << c.getY() << endl;\n",
    "    cout << \"d aka p: \" << d.getX() << \",\" << d.getY() << endl;\n",
    "    \n",
    "    // Demonstrates that c and d are references / aliases to the original object\n",
    "    cout << \"Memory address - p:\" << &p << endl;\n",
    "    cout << \"Memory address - c:\" << &c << endl;\n",
    "    cout << \"Memory address - d:\" << &d << endl;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Reference variables do not necessarily make sense for local variables as in the previous example.  Confusing to have multiple names for the same object in the same context/scope. \n",
    "\n",
    "Reference variables are important, though, for functions as the provide [pass by reference](https://en.wikipedia.org/wiki/Evaluation_strategy#Call_by_reference) semantics\n",
    "\n",
    "The following example demonstrates uses reference variables for function parameters in the swap functions as well as in overriding the `+` operator when both operands are `Point` objects. Notice that we can use these reference parameters for both classes as well as the standard built-in data types."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "cpp"
    }
   },
   "outputs": [],
   "source": [
    "//filename: pointer_ref_func.cpp\n",
    "//compile: g++ -std=c++17 -o pointer_ref_func pointer_ref_func.cpp\n",
    "//execute: ./pointer_ref_func\n",
    "#include <iostream>\n",
    "using namespace std;\n",
    "\n",
    "class Point {\n",
    "private:\n",
    "    double x, y;\n",
    "public:\n",
    "    Point(double initialX = 0.0, double initialY = 0.0) : x{initialX}, y{initialY} {}\n",
    "    double getX() const { return x; }\n",
    "    double getY() const { return y; }\n",
    "    void setX(double val) { x = val; }\n",
    "    void setY(double val) { y = val; }\n",
    "\n",
    "    Point operator+(const Point& other) const { \n",
    "        return Point( x + other.x, y + other.y);\n",
    "    }\n",
    "    Point& operator+=(const Point& other){ \n",
    "        this->x += other.getX();\n",
    "        this->y += other.getY();\n",
    "        return *this;\n",
    "    }\n",
    "};\n",
    "\n",
    "void swap(int& a, int& b) {\n",
    "    int temp = a;\n",
    "    a = b;\n",
    "    b = temp;\n",
    "}\n",
    "\n",
    "void swap(Point& a, Point&  b) {\n",
    "    Point temp = a;\n",
    "    a = b;\n",
    "    b = temp;\n",
    "}\n",
    "\n",
    "int main(int argc, char *argv[]) {\n",
    "    Point a(5,2);\n",
    "    Point b(-12, 7);\n",
    "\n",
    "    a = a + b;  // a = -7,9 now\n",
    "\n",
    "    swap(a,b);\n",
    "\n",
    "    cout << \"a: \" << a.getX() << \",\" << a.getY() << endl;\n",
    "    cout << \"b: \" << b.getX() << \",\" << b.getY() << endl;\n",
    "\n",
    "    int i = 42;\n",
    "    int j = 92;\n",
    "    swap(i,j);\n",
    "    cout << \"i: \" << i << endl;\n",
    "    cout << \"j: \" << j << endl;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Reference variables as parameters provide two advantages: 1) passing large objects and 2) providing the capability to mutate existing objects. We can add the `const` modify to avoid mutation of the passed object. The `+` method demonstrates this both in making the \"other\" point constant as well as the entire function.  The `const` at the end of the method header informs other programmers that we are not mutating the original object, but rather creating a new object as the result. Anywhere you see `const` at the end of a function, you should read that method as being designated as an accessor in which the object it belongs to will remain unchanged when the method is invoked/called.\n",
    "\n",
    " In the `+=` method, we can't make method `const` as the object itself needs to be mutated and returned.\n",
    "\n",
    "*Note:* In a later page, we will see how to use templates to avoid writing multiple versions of the same function (e.g., swap) that just differ by their type.\n",
    "\n",
    "## Pointer Variables\n",
    "\n",
    "Pointer variables function the same as they do in C.  With pointers, the address can be manipulated (pointer arithmetic) and assigned the `nullptr` value. `nullptr` is a keyword introduced in C++11 that represents a pointer that does not point to any memory location. Prior to C++, programmers would use the value `NULL` or 0.\n",
    "\n",
    "## Comparison between nullptr, NULL, and 0:\n",
    "\n",
    "- `nullptr`:Type `std::nullptr_t` with implicit conversions to any pointer type. Cannot be converted to integral types, except for bool. Type-safe and preferred for representing a null pointer in modern C++.\n",
    "- `NULL`: Macro that typically represents the integer zero. May cause issues in function overloading and template specialization due to being an integer type, not a pointer type.\n",
    "- 0 (Zero): Integer that can be implicitly converted to any pointer type, representing a null pointer. Like NULL, possible ambiguity can arise in function overloading and template specialization."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Dynamic Memory Management\n",
    "\n",
    "With value variables, the C++ compiler manages memory automatically.  With the variable declaration within functions, the compiler automatically allocated space within that function's stack frame. This allocation is possible due to static typing of C++. When the variable goes out of scope (e.g., the function returns), that stack frame is destroyed and hence any allocated space within that stack is implicitly released. This automatic management eases the burden both upon the programmer and the system itself (less work to manage memory). \n",
    "\n",
    "However, this use of memory is pre-determined at compile when the programmer originally develops the code.  Circumstances might exist when we need to more actively control the allocation of objects and when they are destroyed. For instance, when creating an object as the result of function that is then returned to be used elsewhere in the program or  when we do not know the size or number of elements a particular data structure may contain.\n",
    "\n",
    "In C, programmers directly allocated the needed space with `malloc` and a computed size. In C++, the dynamic allocation is performed using the keyword `new` along with the type to be created. As with C, we will use a pointer variable to store the address of the allocated memory.\n",
    "\n",
    "```c++\n",
    "Point *p;\n",
    "p = new Point();\n",
    "\n",
    "```\n",
    "\n",
    "In the above code block, the first line allocates space to store the pointer `p`. Unless `p` is a global variable, this allocation occurs within the function's stack frame. In the second line, the compiler creates code to allocate space for a `Point` object in the heap and then assigns that address to `p`.\n",
    "\n",
    "As with any allocated resource (memory, files, network sockets, etc.), we need to use that resource, and, then, once we are finished with that resource, release that resource back to the system (free, close, etc.).  \n",
    "\n",
    "For dynamically allocated objects in C++, programmers use the `delete` keyword.  Only dynamically created objects can be destroyed in this manner.\n",
    "\n",
    "```c++\n",
    "delete p;\n",
    "\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Dynamic Memory Management for Arrays\n",
    "\n",
    "For better or worse, C++ treats built-in arrays the same way C treats arrays.  As with C, if we know the size of the array, we can declare an array with that size. \n",
    "\n",
    "However, if we need to dynamically allocate the array (e.g., so that it can be used as a result of a function), we can use the following pattern:\n",
    "\n",
    "```c++\n",
    "type *variableName = new type[numberOfElements];\n",
    "\n",
    "```\n",
    "\n",
    "Unlike C, it was unnecessary to compute the memory size - C++ handles this.\n",
    "\n",
    "When this allocation occurs, the default constructor for the object is utilized.\n",
    "\n",
    "To free the allocated memory, use\n",
    "\n",
    "```c++\n",
    "delete[] variableName;\n",
    "variableName = nullptr;\n",
    "\n",
    "```\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "cpp"
    }
   },
   "outputs": [],
   "source": [
    "//filename: point_dmm.cpp\n",
    "//compile: g++ -std=c++17 -o point_dmm point_dmm.cpp\n",
    "//execute: ./point_dmm\n",
    "#include <iostream>\n",
    "using namespace std;\n",
    "\n",
    "class Point {\n",
    "private:\n",
    "    double x = 0, y = 0;\n",
    "public:\n",
    "    Point(double initialX = 0.0, double initialY = 0.0) : x{initialX}, y{initialY} {}\n",
    "    double getX() const { return x; }\n",
    "    double getY() const { return y; }\n",
    "    void setX(double val) { x = val; }\n",
    "    void setY(double val) { y = val; }\n",
    "};\n",
    "\n",
    "int main(int argc, char *argv[]) {\n",
    "    size_t numPoints = 5;\n",
    "    Point *points = new Point[numPoints];\n",
    "    \n",
    "    for (size_t i; i < numPoints; i++) {\n",
    "        cout << i << \": \" << points[i].getX() << \",\" << points[i].getY() << endl;\n",
    "    }\n",
    "\n",
    "    delete[] points;\n",
    "    points = nullptr;   // indicates that we can no longer access the array\n",
    "\n",
    "    return EXIT_SUCCESS;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Dynamically Allocate Matrix\n",
    "\n",
    "Using matrices (two-dimensional arrays) is another common programming task.  Matrices often represent spreadsheet-like information.  They can also be used to represent positions on a game board such as chess or <a href=\"https://en.wikipedia.org/wiki/Battleship_(game)\">Battleship</a>.   For creating a two-dimensional array, we need to create an array of of pointers to a pointer.  We first create the array of pointers for the rows, then for each row allocate another array to represent the columns within that row."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "cpp"
    }
   },
   "outputs": [],
   "source": [
    "//filename: matrix.cpp\n",
    "//compile: g++ -std=c++17 -o matrix matrix.cpp\n",
    "//execute: ./matrix\n",
    "#include <iostream>\n",
    "#include <iomanip>\n",
    "using namespace std;\n",
    "\n",
    "class Point {\n",
    "private:\n",
    "    double x = 0.0, y = 0.0;\n",
    "public:\n",
    "    Point(double initialX = 0.0, double initialY = 0.0) : x{initialX}, y{initialY} {}\n",
    "    double getX() const { return x; }\n",
    "    double getY() const { return y; }\n",
    "    void setX(double val) { x = val; }\n",
    "    void setY(double val) { y = val; }\n",
    "};\n",
    "\n",
    "int main(int argc, char *argv[]) {\n",
    "    int rows = 3, columns = 5; \n",
    "    \n",
    "    Point **matrix = new Point*[rows];  // allocate space for the row pointers\n",
    " \n",
    "    for (int i = 0; i < rows; i++) {       // allocate space for the columns in each row\n",
    "        matrix[i] = new Point[columns];\n",
    "    }\n",
    " \n",
    "    //Saves the current state of cout flags (e.g., precision)\n",
    "    std::ios cout_state(nullptr);\n",
    "    cout_state.copyfmt(std::cout);\n",
    "\n",
    "    cout << setprecision(2);  // precision remains set until changed\n",
    "    cout << fixed;\n",
    "    cout << \"[\";\n",
    "    for (int i = 0; i < rows; i++) {\n",
    "        if (i > 0) { cout << endl << \" \"; }\n",
    "        for (int j = 0; j < columns; j++) {\n",
    "            cout << \"(\" << setw(6) << matrix[i][j].getX() << \",\"  \n",
    "                        << setw(6) << matrix[i][j].getY() << \")\";\n",
    "        }\n",
    "    }\n",
    "    cout << \"]\" << endl;\n",
    "    \n",
    "    // restore the state of cout\n",
    "    std::cout.copyfmt(cout_state);\n",
    "    \n",
    "    // free the allocated memory - just reverse the stesp\n",
    "    for (int i = 0; i < rows; i++) { // delete inner arrays (data for each row)\n",
    "        delete[] matrix[i];\n",
    "    }\n",
    "    delete[] matrix;\n",
    "    return 0;\n",
    "}\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Memory Reallocation\n",
    "\n",
    "Unlike C, C++ does not have a function to reallocate memory.  We'll need to manually allocate new space, copy over the existing data, and then delete the previously allocated space."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "cpp"
    }
   },
   "outputs": [],
   "source": [
    "//filename: realloc.cpp\n",
    "//compile: g++ -std=c++17 -o realloc realloc.cpp\n",
    "//execute: ./realloc\n",
    "#include <iostream>\n",
    "\n",
    "int* realloc_int_array(int* oldArray, std::size_t oldSize, std::size_t newSize) {\n",
    "    if (newSize == 0) {\n",
    "        delete[] oldArray;\n",
    "        return nullptr;\n",
    "    }\n",
    "    int* newArray = new int[newSize];\n",
    "    if(oldArray != nullptr) {\n",
    "        std::size_t copySize = (oldSize < newSize) ? oldSize : newSize;\n",
    "        std::copy(oldArray, oldArray + copySize, newArray);\n",
    "        delete[] oldArray;\n",
    "    }\n",
    "    return newArray;\n",
    "}\n",
    "\n",
    "int main() {\n",
    "    int* arr = new int[5];\n",
    "    for (int i = 0; i < 5; ++i) {\n",
    "        arr[i] = i;\n",
    "    }\n",
    "\n",
    "    arr = realloc_int_array(arr, 5, 10);  // change the size to 10.\n",
    "\n",
    "    // DANGER!!! Accessing the newly allocate memory before assignment \n",
    "    std::cout << arr[9] << \" - do not do this!\" << std::endl;\n",
    "    \n",
    "    delete[] arr;\n",
    "\n",
    "    return 0;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Additional Notes\n",
    "\n",
    "Be cognizant that C++ can create temporary objects through seeming innocuous appearing code.  For instance:  `cout << string1 + string2 << endl;` creates a temporary object from the concatenation of the objects `string1` and `string2`. Scott Meyers has the following in his book _More Effective C++_ in the section \"Understand the Origin of Temporary Objects\":\n",
    "\n",
    "<blockquote>\n",
    "True temporary objects in C++ are invisible - they don't appear in your source code. They arise whenever a non-heap object is created but not named. Such unnamed objects usually arise in one of two situations: when implicit type conversions are applied to make function calls succeed and when functions return objects.\n",
    "<p>\n",
    "Consider first the case in which temporary objects are created to make function calls succeed. This happens when the type of object passed to a function is not the same as the type of the parameter to which it is being bound.\n",
    "<p>\n",
    "These conversions occur only when passing objects by value or when passing to a reference-to-const parameter. They do not occur when passing an object to a reference-to-non-const parameter.\n",
    "<p>\n",
    "The second set of circumstances under which temporary objects are created is when a function returns an object.\n",
    "<p>\n",
    "Anytime you see a reference-to-const parameter, the possibility exists that a temporary will be created to bind to that parameter. Anytime you see a function returning an object, a temporary will be created (and later destroyed).\n",
    "</blockquote>\n",
    "\n",
    "C++ will make implicit conversions. With any constructor that takes just one argument, the compiler will look for a way to convert from the argument type to that of the constructor's parameter. To avoid this, place the keyword `explicit `in front of the constructor.  Generally speaking, any single argument constructors (except the copy constructor) should use `explicit`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Memory States\n",
    "\n",
    "The following diagram shows the different states that dynamically-allocated memory can have.  Initially, as the memory is unallocated, all that can be done with that space is allocated via new.  The memory that is allocated is now available to be written and/or freed.  However, it's invalid to read from the location the contents of the allocated memory could be anything.  Once, we then write to the memory location, we can then read those contents.  \n",
    "\n",
    "![](images/memoryStates-1697427706933.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Summary\n",
    "\n",
    "- Programmers are responsible for memory.\n",
    "- You must allocate the right amount of memory required.\n",
    "- You must free any memory that you allocate.\n",
    "- Resource leaks occur when you don't release unneeded resources - eventually programs will stop functioning correctly."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Sample LLM Prompts\n",
    "* Explain stack vs heap memory allocation in C++. Explain when to use each, their performance implications, and provide code examples demonstrating proper usage.\n",
    "* Develop an in-depth guide on the five memory regions in C++ programs (Text, Initialized Data, Uninitialized Data, Stack, and Heap). Explain the purpose of each region and provide examples of what types of data are stored in each.\n",
    "* Write a detailed tutorial on value variables in C++. Compare and contrast how value variables are handled in C++ versus Python, focusing on memory allocation and performance implications.\n",
    "* Craft a step-by-step guide on reference variables in C++. Explain their usage, limitations, and best practices. Include examples of using reference variables as function parameters and discuss the concept of const references.\n",
    "* Explain dynamic memory management in C++. Explain the use of new and delete operators, cover memory leaks, and provide best practices for managing dynamically allocated memory.\n",
    "* Explain dynamic memory allocation for arrays in C++. Cover both single-dimensional and multi-dimensional arrays, and explain proper deallocation techniques."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Review Questions\n",
    "1. In which memory region are global variables typically stored?\n",
    "2. What is the primary difference between stack and heap memory allocation?\n",
    "3. How do you dynamically allocate an array in C++?\n",
    "4. What is a memory leak, and how can it occur?\n",
    "5. What is the difference between passing by value and passing by reference in terms of memory usage?\n",
    "6. What is the purpose of the 'explicit' keyword when used with constructors?\n",
    "7. What operators are used to acquire more memory than a program is initially allocated?\n",
    "8. What is a dereference operator and why do we need one?\n",
    "\n",
    "[answers](answers/rq-15-answers.md)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
