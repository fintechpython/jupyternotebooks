# CPlusPlus / Classes
1. *How do constructors and functions differ in C++?*

    a. Constructors have the same name as the class, while functions have their own unique names.
    b. Constructors don't have a return type, not even void, while functions must specify a return type.
    c. Constructors are automatically called when an object is created, while functions are called explicitly.
    d. The primary purpose of constructors is to initialize object members, while functions perform operations.
    e. Constructors can't be called directly like regular functions; they're invoked during object creation.

2. *How is a defualt constructor identified? Does C++ always have a default constructor? When is the default constructor called?*

   A default constructor is one that can be called with no arguments. If a class does not explicitly define any constructors, the compiler implicitly defines a default constructor. The default constructor is called when an object is declared without any arguments.

3. *What actions should be performed within a destructor?*

   a. Freeing dynamically allocated memory
   b. Closing open files or resources
   c. Releasing system resources (e.g., network connections)
   d. Performing any necessary final cleanup specific to the class

4. *What is the Rule of Three in C++?*

   If a class defines any of the following: destructor, copy constructor, or copy assignment operator, it should probably explicitly define all three.

5. *What is the difference between shallow copy and deep copy?*

   Shallow copy duplicates object references, while deep copy creates new objects with the same values.

6. *How does a namespace differ from a class?*

   a. Purpose: Namespaces organize code and avoid name collisions, while classes create user-defined types.
   b. Instantiation: Namespaces cannot be instantiated, but classes can create objects.
   c. Members: Namespaces can contain various elements (functions, variables, types), while classes encapsulate data and functions.
   d. Scope: Namespaces affect name lookup, while classes create new scopes.
   e. Extensibility: Namespaces can be extended across files, classes cannot be modified after definition.

7. *How does generic programming differ from object-oriented programming?*

   The most obvious is that the choice of function invoked when you use generic programming is determined by the compiler at compile time, whereas for object-oriented programming, it is not determined until run time. 

8. *What is the difference between the interface and the implementation in a class?*

   The interface is the part of the class’s declaration that its users access directly. 
   The implementation is that part of the class’s declaration that its users access only indirectly through the interface.
   This separation allows for encapsulation, hiding the internal workings of the class while presenting a clear, stable interface to users.

9. *Why should the public interface to a class be as small as possible?*

   a. A small interface is easy to learn and easy to remember, and the implementer doesn’t waste a lot of time implementing unnecessary and rarely used facilities.
   b. A small interface also means that when something is wrong, there are only a few functions to check to find the problem.
