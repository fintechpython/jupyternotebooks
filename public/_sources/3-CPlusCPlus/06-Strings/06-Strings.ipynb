{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Strings in C++\n",
    "While C++ has full support for C-style strings, programmers should instead utilize the `string` class. Objects of this class represent strings as variable-length sequences of characters. Unlike immutable strings in Java and Python, C++ strings are mutable. (Note: C-style strings are discussed in the \"Built-in Arrays\" notebook.)\n",
    "\n",
    "To use the string type, you need to include the string header. You can put the \"using std::string;\" line in your source code to use just the `string` type rather than having to include the `std::` namespace identifier or bring in the entire `std` namespace.\n",
    "```c++\n",
    "#include <string>\n",
    "using std::string;\n",
    "```\n",
    "\n",
    "The string class provides significant advantages in that the class manages memory and overrides the standard operators such as `==` to check for equality and `+` for string concatenation.\n",
    "\n",
    "The following code demonstrates variaous ways to define and initialize strings:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "cpp"
    }
   },
   "outputs": [],
   "source": [
    "//filename: string_init.cpp\n",
    "//compile: g++ -std=c++17 -o string_init string_init.cpp\n",
    "//execute: ./string_init\n",
    "#include <iostream>\n",
    "#include <string>\n",
    "using std::string;\n",
    "\n",
    "int main(int argc, char *argv[]){\n",
    "    string s1;                // defualt initialization, string is empty\n",
    "    string x = \"hello world\"; // Uses a conversion constructor\n",
    "    string y(\"John\");         \n",
    "    string z{\"Steve\"};       \n",
    "    string w{y};              // w is a copy of y\n",
    "\n",
    "    std::cout << x << \"\\n\" << s1 << \"\\n\" << y << \"\\n\" << z << \"\\n\";\n",
    "}\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## String Operations\n",
    "\n",
    "Fortunately, the `string` class provides a comprehensive range of functionality for strings, similar to what's available with Python. The class handles memory management as well.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "cpp"
    }
   },
   "outputs": [],
   "source": [
    "//filename: greeting.cpp\n",
    "//compile: g++ -std=c++17 -o greeting greeting.cpp\n",
    "//execute: ./greeting\n",
    "#include <iostream>\n",
    "#include <string>\n",
    "using namespace std; \n",
    "\n",
    "int main(int argc, char *argv[]) {\n",
    "    string name;\n",
    "\n",
    "    cout << \"What is your name?\" << endl;\n",
    "    cin >> name;\n",
    "    cout << \"It's nice to meet you, \" << name << \"\\n\";\n",
    "}\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You should compile and run this code to see the behavior when entering different strings through the console.\n",
    "\n",
    "Try typing in your full name. You should notice that `cin` separates items by whitespace and only the first portion of the name is returned. (e.g, for \"Steve Smith\" only \"Steve\" is returned in the string.)\n",
    "\n",
    "To read a full line of input, we can use the <a href=\"https://en.cppreference.com/w/cpp/string/basic_string/getline\">getline()</a> function. Notice that `getline()` does strip the newline character, unlike similar counterparts in Python and C."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "cpp"
    }
   },
   "outputs": [],
   "source": [
    "//filename: line.cpp\n",
    "//compile: g++ -std=c++17 -o line line.cpp\n",
    "//execute: ./line\n",
    "#include <iostream>\n",
    "#include <string>\n",
    "using namespace std; \n",
    "\n",
    "int main(int argc, char *argv[]) {\n",
    "    string name;\n",
    "    \n",
    "    cout << \"What is your name?\" << endl;\n",
    "    if (getline(cin,name)) {\n",
    "        cout << \"It's nice to meet you, \" << name << \"XXXXX\" << \"\\n\";\n",
    "    }\n",
    "    else {\n",
    "        cout << \"No input available\\n\";\n",
    "    }\n",
    "}\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Run the program. Type in your full name as a test.\n",
    "\n",
    "How can you get the \"No input available message\" to appear?\n",
    "(anwer at the bottom of the page)\n",
    "\n",
    "Here's another example of `getline()` to just echo input from standard input:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "cpp"
    }
   },
   "outputs": [],
   "source": [
    "//filename: echo.cpp\n",
    "//compile: g++ -std=c++17 -o echo echo.cpp\n",
    "//execute: ./echo\n",
    "#include <iostream>\n",
    "#include <string>\n",
    "using namespace std; \n",
    "\n",
    "int main(int argc, char *argv[]) {\n",
    "    string line;\n",
    "    \n",
    "    cout << \"Type some data to echo.  Hit ctrl-d when you are complete.\" << endl;\n",
    "    while (getline(cin,line)) {\n",
    "        cout <<  line <<  \"\\n\";\n",
    "    }\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here's some additional sample code of the `string` class in use:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "cpp"
    }
   },
   "outputs": [],
   "source": [
    "//filename: example.cpp\n",
    "//compile: g++ -std=c++17 -o example example.cpp\n",
    "//execute: ./example\n",
    "#include <iostream>\n",
    "#include <string>\n",
    "using namespace std;;\n",
    "\n",
    "int main(int argc, char *argv[]){\n",
    "    string s1;                // default initialization, string is empty\n",
    "    string x = \"hello\";       // Uses the copy-assignment operator for the given literal\n",
    "    string y(\"John\");         // y is a copy of the string literal\n",
    "\n",
    "    if (s1.empty()) {\n",
    "        cout << \"s1 was empty, but has a capacity of \"<< s1.capacity() << \"\\n\";\n",
    "    }\n",
    "\n",
    "    if (not x.empty()) {\n",
    "        cout << \"x has a length of \" << x.length() << \" with capacity - \" << x.capacity() << \"\\n\";\n",
    "        cout << \"x has a length of \" << x.size() <<  \"\\n\";   //size was added for consistency with the standard library\n",
    "    }\n",
    "\n",
    "    x += \" world\";   // concatenation, existing object is altered - C++ strings are mutable\n",
    "    cout << x << \" - length: \" << x.length() <<  \", capacity: \" << x.capacity() << \"\\n\";\n",
    "\n",
    "    if (x == y) { // comparison operators overloaded\n",
    "        cout << \"strings are the same\" << \"\\n\";\n",
    "    }\n",
    "    else {\n",
    "        cout << \"the two strings differ\" << \"\\n\";\n",
    "    }\n",
    "\n",
    "    cout << x.substr(0,5) << \"-\"<< x.substr(6) << \"\\n\";\n",
    "\n",
    "    string line = \"It was the best of times, it was the worst of times\";\n",
    "    cout << line.find(\"best\") << \"\\n\";\n",
    "\n",
    "    // npos is constant containing the largest possible size \n",
    "    // of a string (same as max value of size_t)\n",
    "    // As find returns an unsigned value, -1 is not feasible so the\n",
    "    // largest possible value is used to signify a string was not found.\n",
    "    cout << (line.find(\"greatest\") == string::npos) << \"\\n\";\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a href=\"https://en.cppreference.com/w/cpp/string/basic_string\">https://en.cppreference.com/w/cpp/string/basic_string</a> lists all of the available methods on the `string` class.\n",
    "\n",
    "Here's an example to split a line by a specific string delimiter. In lines 10 &amp; 11, the `auto` keyword is used to automatically declare a variable of the appropriate type based upon the results of the left hand side of this equation.\n",
    "\n",
    "This process works by tracking two positions within a string as we seek to find the delimiter within the string. `start` tracks the current beginning of an extracted/found token. We can pass that value to find as an optional parameter to say \"starting looking for the delimiter at this position\". `end` tracks where we have last seen the delimiter. if a delimiter was not found, find returns `string::npos` (think of that as no position), which equals the maximum size of an unsigned long int."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "cpp"
    }
   },
   "outputs": [],
   "source": [
    "//filename: split.cpp\n",
    "//compile: g++ -std=c++17 -o split split.cpp\n",
    "//execute: ./split\n",
    "#include <iostream>\n",
    "#include <string>\n",
    "using namespace std;\n",
    "\n",
    "int main(int argc, char *argv[]) {\n",
    "    string s = \"java:c:c++:Python:Pascal:Basic\";\n",
    "    string delim = \":\";\n",
    "\n",
    "    auto start = 0U;\n",
    "    auto end = s.find(delim);\n",
    "    while (end != string::npos) {\n",
    "        std::cout << s.substr(start, end - start) << \"\\n\";\n",
    "        start = end + delim.length();\n",
    "        end = s.find(delim, start);\n",
    "    }\n",
    "\n",
    "    std::cout << s.substr(start, end) << \"\\n\";\n",
    "    std::cout << \"Final value of end: \" << end << \"\\n\";\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Converting Strings to Other Types\n",
    "As part of the Standard Library, C++ contains several functions to convert strings to numerical values:\n",
    "- floating-point numbers: [https://en.cppreference.com/w/cpp/string/basic_string/stof](https://en.cppreference.com/w/cpp/string/basic_string/stof)\n",
    "- \"integer\" numbers: [https://en.cppreference.com/w/cpp/string/basic_string/stol](https://en.cppreference.com/w/cpp/string/basic_string/stol)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "cpp"
    }
   },
   "outputs": [],
   "source": [
    "//filename: convert.cpp\n",
    "//compile: g++ -std=c++17 -o convert convert.cpp\n",
    "//execute: ./convert\n",
    "#include <iostream>\n",
    "#include <string>\n",
    "#include <stdexcept>\n",
    "using namespace std;\n",
    "\n",
    "int main(int argc, char *argv[]) {\n",
    "    double d;\n",
    "    string line;\n",
    "    \n",
    "    cout << \"Type some data to convert a double.  Hit ctrl-d when you are complete.\" << endl;\n",
    "    while (getline(cin,line)) {\n",
    "        try {\n",
    "            d = std::stod(line); \n",
    "            cout << \"Converted number: \" << d << \"\\n\";\n",
    "        } catch (const std::invalid_argument&) {\n",
    "            cerr << \"Invalid argument: \" << line << \"\\n\";\n",
    "        } catch (const std::out_of_range&) {\n",
    "            cerr << \"out of range of double: \" << line << \"\\n\";\n",
    "        }\n",
    "    }\n",
    "\n",
    "    return EXIT_SUCCESS;\n",
    "}\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `try-catch` statements will be discussed in the error handling and exceptions notebook.\n",
    "\n",
    "## Implementation Notes\n",
    "Behind the scenes, C++ uses an array (a contiguous segment of memory holding `char` objects) to store strings. The `string` class tracks the length(size) of the string stored within that array as well as the available capacity - the number of objects allocated. Looking at the example above, we see that C++ (at least this implementation) uses a default capacity of 15 to hold string values. As the string expands, the capacity will be increased when needed. We'll present more details of this implementation when we discuss memory location and allocation in C++."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## End of File\n",
    "To end input from the console, type ctrl-d.  This closes the input stream immediately and returns `EOF`.\n",
    "\n",
    "## Reference\n",
    "For specific details on the `std::string` class and the associated functionality, look at \n",
    "[CPP Reference: string](https://en.cppreference.com/w/cpp/string/basic_string) or [cplusplus: string](https://cplusplus.com/reference/string/string/)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Suggested LLM Prompts\n",
    "\n",
    "* Explore C-style strings in C++. Explain what they are, how they are represented in memory, and their limitations. Cover the basics of declaring and initializing C-style strings, common string manipulation functions from the <cstring> library, and potential pitfalls such as buffer overflows.\n",
    "* Create a guide on the std::string class in C++. Explain its advantages over C-style strings, including dynamic memory management and safer operations. Cover the basics of creating, modifying, and accessing std::string objects. Include examples of common member functions like length(), substr(), and find().\n",
    "* Explain string concatenation in C++. Compare and contrast concatenation methods for C-style strings (strcat) and std::string objects (+ operator, append() function). Discuss the efficiency of different concatenation techniques and provide best practices for performance-critical code.\n",
    "* Create a guide on string input and output in C++. Explain how to read strings from the console and files using C++ streams (cin, getline).\n",
    "* Explain string manipulation and transformation in C++. Cover topics such as substring extraction, string replacement, case conversion, and trimming whitespace. Provide examples using std::string member functions.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Review Questions\n",
    "For questions on specific functionality, you can also refer to the materials linked in the Reference section.\n",
    "\n",
    "1. What is string concatenation? How do you perform string concatenation in C++?\n",
    "2. How do you find a substring within a string?\n",
    "3. What's the purpose of the capacity() method?\n",
    "4. How do you convert a string to an integer?\n",
    "5. What's the difference between push_back() and append() for std::string?\n",
    "6. What's the difference between erase() and clear() for std::string?\n",
    "\n",
    "[answers](answers/rq-06-answers.md)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
