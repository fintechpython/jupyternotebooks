# CPlusPlus / Expressions And Operators
1. *Which operator do you use to read into a variable?*

   The operator to read into a variable is called the right shift operator >>

2. *What is the difference between = and ==?*

   The assignment operator (=) is used to assign a value to a variable, while the equality operator (==) is used to compare two values for equality.

3. *What is the difference between an expression and a statement?*

   An expression computes a value from a number of operands. Statement is a basic unit of a program controlling the execution flow in a function, such as if-statement, for statement, expression statement, and declaration. There are two kinds of statements: expression statements and declarations. An expression statement is simply an expression followed by a semicolon.

4. *What operator can be used on integers but not on floating-point numbers?*

   %

5. *What is an lvalue? List the operators that require an lvalue. Why do these operators, and not the others, require an lvalue?*

   An lvalue (short for “value that can appear on the left-hand side of an assignment”) where the operator modifies an operand. List of operators: *=, /=, %=, +=, -=. These operators require an lvalue on the left hand side because they may modify the value.

6. *What is the difference between the prefix and postfix increment operators (++i and i++)?*

   The prefix increment operator (++i) increments the value and then returns the incremented value, while the postfix increment operator (i++) returns the original value and then increments it.
