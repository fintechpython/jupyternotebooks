{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Namespaces\n",
    "\n",
    "Namespaces allow programmers to organize code into discrete, identifiable sections. They help avoid name clashes, especially when integrating with third-party libraries, by providing a distinct container for identifier names.  We've already been using the standard namespace, `std::`. Initially, we used `using namespace std;`  to bring in the entire namespace into our code for convenience, but then we migrated to either including specific objects such as `cout` with `using std::cout` or using the namespace identifier `std::` wherever `cout` was located in our code.\n",
    "\n",
    "Namespaces are containers where identifiers (like class, variable, and function names) reside. Namespaces also create a scope for these identifiers.\n",
    "\n",
    "To understand namespaces, we also need to understand where items are defined how that affects their scope.\n",
    "\n",
    "Variables defined - \n",
    "- in functions are _local_ variables\n",
    "- in classes are _member_ variables\n",
    "- elsewhere are _global_ variables\n",
    "\n",
    "Functions defined -\n",
    "- in classes are _member_ functions (methods).\n",
    "- elsewhere are _global_ functions.\n",
    "\n",
    "Namespaces add a layer of disambiguation when items have the same name, but exist in different parts of a system.\n",
    "\n",
    "\n",
    "## Declaring\n",
    "\n",
    "Use the following syntax to declare a namespace.\n",
    "\n",
    "```c++\n",
    "namespace namespace_name {\n",
    "    // code declarations\n",
    "}\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Accessing Namespace Members:\n",
    "\n",
    "To access members (variables, functions, classes, etc.) of a namespace, use the scope resolution operator ``::`.`\n",
    "\n",
    "## The using Directive:\n",
    "\n",
    "The `using`` directive allows you to bring the entire namespace into the current scope. Be cautious with this method as it can lead to name clashes.\n",
    "\n",
    "```c++\n",
    "using MySpace;\n",
    "```\n",
    "\n",
    "## The using Declaration:\n",
    "\n",
    "To introduce specific namespace members, use the `using`` declaration.\n",
    "\n",
    "```c++\n",
    "using MySpace::myVar;\n",
    "```\n",
    "\n",
    "The following code demonstrates declaring a namespace, `MyNamespace`, and then different ways of accessing it within `main`.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": [],
    "vscode": {
     "languageId": "cpp"
    }
   },
   "outputs": [],
   "source": [
    "//filename: namespaces.cpp\n",
    "//compile: g++ -std=c++17 -o namespaces namespaces.cpp\n",
    "//execute: ./namespaces\n",
    "#include <iostream>\n",
    "\n",
    "namespace MySpace {\n",
    "    int myVar = 10;\n",
    "    int alterMyVar() {\n",
    "        return 2 * myVar;\n",
    "    }\n",
    "}\n",
    "\n",
    "namespace AnotherSpace {\n",
    "    int myVar = 50;\n",
    "    int alterMyVar() {\n",
    "        return 3 * myVar;\n",
    "    }\n",
    "}\n",
    "\n",
    "int main(int argc, char *argv[]) {\n",
    "    int myVar = 3;\n",
    "\n",
    "    std::cout << \"main: myVar - \" << myVar << \"\\n\";\n",
    "    std::cout << \"MySpace: myVar - \" << MySpace::myVar << \"\\n\";\n",
    "    std::cout << \"MySpace: calling alterMyVar - \" << MySpace::alterMyVar() << \"\\n\";\n",
    "\n",
    "    {\n",
    "        using namespace AnotherSpace;  // this using is limited to the scope of {   }\n",
    "        std::cout << \"calling alterMyVar in AnotherSpace - \" << alterMyVar() << \"\\n\";\n",
    "        std::cout << \"calling alterMyVar in MySpace - \" << MySpace::alterMyVar() << \"\\n\";\n",
    "    }\n",
    "\n",
    "    using namespace MySpace;\n",
    "    std::cout << \"calling alterMyVar - \" << alterMyVar() << \"\\n\";\n",
    "\n",
    "    // Note that with the same identifier, C++ uses the current scope declaration.\n",
    "    std::cout << \"myVar - \" << myVar << \"\\n\";\n",
    "\n",
    "    std::cout << \"calling alterMyVar in AnotherSpace - \" << AnotherSpace::alterMyVar() << \"\\n\";\n",
    "\n",
    "    return EXIT_SUCCESS;\n",
    "}\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "Note that we can't use `using MyNamespace::myVar;` in `main` as `myVar` is already declared in the current scope."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "source": [
    "## C++ and Scope\n",
    "\n",
    "In C++, scope is a region of program text that determines the visibility and lifetime of variables, functions, and types (classes/structs).\n",
    "\n",
    "| Scope | Definition |\n",
    "|-------|------------|\n",
    "| Global | Identifiers declared outside of any function or class, accessible throughout the entire program |\n",
    "| Namespace | Identifiers declared within a namespace, accessible within that namespace or when explicitly qualified |\n",
    "| Class | Members declared within a class, accessible based on their access specifiers (public, private, protected). Covered in a later notebook. |\n",
    "| Local / Function | Variables declared inside a function, only accessible within that function. Includes parameters. local can also scope defined within a code block (enclosed by curly braces) |\n",
    "| Statement | Variables declared within the control statement of a `for` loop.|\n",
    "\n",
    "Study the following program to see how \"var\" is declared and utilized across multiple scopes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": [],
    "vscode": {
     "languageId": "cpp"
    }
   },
   "outputs": [],
   "source": [
    "// file: scope.cpp\n",
    "// compile: g++ -std=c++17 -o scope scope.cpp\n",
    "// execution: ./scope\n",
    "\n",
    "#include <iostream>\n",
    "using namespace std;\n",
    "\n",
    "int var = 25;   // Global scope\n",
    "\n",
    "namespace MySpace {\n",
    "    int var = 15;   // Namespace scope\n",
    "}\n",
    "\n",
    "void foo() {\n",
    "    cout << \"Inside foo():\\n\";\n",
    "    \n",
    "    cout << \"    \" << var << \": global scope - var\\n\";\n",
    "    int var = 20;   // Function scope\n",
    "    cout << \"    \" << var << \": local/function scope - var\\n\";\n",
    "    cout << \"    \" << ::var << \": global scope - var\\n\";\n",
    "    \n",
    "    if (true) {\n",
    "        int var = 30;  // Block scope (visible only inside this block)\n",
    "        int blockvar = 100; \n",
    "        cout << \"    \" << var << \": local(block) scope - var\\n\";\n",
    "    }\n",
    "    // cout << blockvar;  -- compiler error if uncommented, not in the current scope\n",
    "    cout << \"    \" << var << \": local/function scope - var\\n\";\n",
    "}\n",
    "\n",
    "class MyClass {\n",
    "public:\n",
    "    static int static_var;  // Class scope (static members)\n",
    "    int var;            // Class scope (non-static members)\n",
    "    \n",
    "    void classScope() {\n",
    "        cout << \"Inside MyClass::classScope():\\n\";\n",
    "        cout << \"    \" << var << \": class scope - var\\n\";\n",
    "        cout << \"    \" << static_var << \": class scope - static - static_var\\n\";\n",
    "\n",
    "        int var = 97;\n",
    "        cout << \"    \" << var << \": local scope - var\\n\";\n",
    "        cout << \"    \" << this->var << \": class scope - var\\n\";\n",
    "\n",
    "    }\n",
    "};\n",
    "\n",
    "// Define the static member outside the class\n",
    "int MyClass::static_var = 99;\n",
    "\n",
    "int main() {\n",
    "    cout << \"Inside main():\\n\";\n",
    "    cout << \"    \" << var << \": Global scope - var\\n\";\n",
    "    cout << \"    \" << MySpace::var << \": Namespace scope (MySpace) - var\\n\";\n",
    "\n",
    "    {\n",
    "        int var = 50; // local scope\n",
    "        cout << \"    \" << var << \": local scope - var\\n\";\n",
    "        cout << \"    \" << ::var << \": global scope - var\\n\";\n",
    "    }\n",
    "    int var = 80;\n",
    "    for (int var=60; var<61; var++) {\n",
    "        // int var = 70;  // Not allowed, redefinition of \"var\" in the same scope\n",
    "        cout << \"    \" << var << \": statement scope - var\\n\";\n",
    "    }\n",
    "    cout << \"    \" << var << \": local/function scope - var\\n\";\n",
    "\n",
    "    // Accessing static member directly using class name\n",
    "    cout << \"    \" << MyClass::static_var << \": class scope - MyClass::static_var\\n\";\n",
    "    \n",
    "    foo();\n",
    "    \n",
    "\n",
    "    // Class scope demonstration\n",
    "    MyClass obj;\n",
    "    obj.var = 40;\n",
    "    obj.classScope();  // Access class members\n",
    "\n",
    "    \n",
    "    return 0;\n",
    "}\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Suggested LLM Prompts\n",
    "* Explain what namespaces are, why they're used, and the syntax for declaring and using them. Cover the concept of name collision and how namespaces solve this problem. Provide examples of creating and using simple namespaces.\n",
    "* Create a guide on the std namespace in C++. Explain its importance, common elements found in the std namespace, and different ways to use std namespace members. Discuss best practices for using the std namespace in large projects.\n",
    "* Create a guide on using namespaces with templates in C++. Explain how namespaces interact with template definitions and specializations. Discuss techniques for managing template code organization using namespaces. Provide examples of complex scenarios involving templates and namespaces.\n",
    "* Explore best practices and common pitfalls with namespaces in C++. Cover topics such as avoiding namespace pollution, managing large-scale projects with multiple namespaces, and techniques for resolving name conflicts. Discuss the debate around using directives (using namespace std;) and provide guidelines for their appropriate use."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Review Questions\n",
    "\n",
    "1. What is the primary purpose of namespaces in C++?\n",
    "2. How do you declare a namespace in C++?\n",
    "3. What is the syntax for accessing a name inside a namespace?\n",
    "4. What is the difference between a using declaration and a using directive?\n",
    "5. How do you handle name conflicts between different namespaces?\n",
    "\n",
    "[answers](answers/rq-09-answers.md)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
