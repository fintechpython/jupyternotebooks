{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Essential Class Operations\n",
    "In C++, classes are a fundamental building block of object-oriented programming. When creating a class, several essential operations exist that you should understand and implement correctly to ensure proper memory management and object behavior. These operations are collectively known as the \"Rule of Five,\" which refers to the following five special member functions:\n",
    "\n",
    "* Default Constructor\n",
    "* Destructor\n",
    "* Copy Constructor\n",
    "* Copy Assignment Operator\n",
    "* Move Constructor and Move Assignment Operator (since C++11)\n",
    "\n",
    "If you define any of these special member functions in your class, it's generally recommended to define all five of them to ensure consistent behavior and prevent potential resource leaks or undefined behavior."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Default Constructor\n",
    "The default constructor is a constructor that takes no arguments. It is automatically generated by the compiler if you don't provide any other constructor. However, it's a good practice to define it explicitly, especially if your class has non-static member variables that require initialization.\n",
    "```cpp\n",
    "class MyClass {\n",
    "public:\n",
    "    MyClass() {\n",
    "        // Initialize member variables here\n",
    "    }\n",
    "    // ...\n",
    "};\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Destructor\n",
    "The destructor is a special member function that is automatically called when an object is destroyed or goes out of scope. It is responsible for deallocating any dynamically allocated memory and performing any necessary cleanup operations.\n",
    "```cpp\n",
    "class MyClass {\n",
    "public:\n",
    "    ~MyClass() {\n",
    "        // Deallocate dynamic memory\n",
    "        // Perform cleanup operations\n",
    "    }\n",
    "    // ...\n",
    "};\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Copy Constructor\n",
    "The copy constructor is a constructor that creates a new object by copying the contents of an existing object. It is automatically generated by the compiler if you don't provide one, but it's generally a good practice to define your own copy constructor, especially if your class has dynamically allocated memory or non-trivial member objects.\n",
    "```cpp\n",
    "class MyClass {\n",
    "public:\n",
    "    MyClass(const MyClass& other) {\n",
    "        // Copy member variables from other\n",
    "        // Deep copy if necessary\n",
    "    }\n",
    "    // ...\n",
    "};\n",
    "```\n",
    "\n",
    "TODO: Explain Deep vs Shallow Copy again"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Copy Assignment Operator\n",
    "The copy assignment operator (operator=) is a member function that copies the contents of one object to another existing object of the same class. Like the copy constructor, it is automatically generated by the compiler if you don't provide one, but you should define your own copy assignment operator if your class has dynamically allocated memory or non-trivial member objects.\n",
    "```cpp\n",
    "class MyClass {\n",
    "public:\n",
    "    MyClass& operator=(const MyClass& other) {\n",
    "        // Copy member variables from other\n",
    "        // Handle self-assignment\n",
    "        // Deep copy if necessary\n",
    "        return *this;\n",
    "    }\n",
    "    // ...\n",
    "};\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Move Constructor and Move Assignment Operator (C++11)\n",
    "The move constructor is a constructor that creates a new object by transferring (moving) the resources from a temporary object (rvalue reference) to the newly created object. The move assignment operator (operator=) is a member function that transfers (moves) the resources from an rvalue reference object to the current object. These operations are more efficient than copying, as they avoid unnecessary memory allocations and deallocations. They were introduced in C++11 to support move semantics and improve performance.\n",
    "```cpp\n",
    "class MyClass {\n",
    "public:\n",
    "    MyClass(MyClass&& other) noexcept {\n",
    "        // Move member variables from other\n",
    "        // Handle transfer of ownership\n",
    "    }\n",
    "\n",
    "    MyClass& operator=(MyClass&& other) noexcept {\n",
    "        // Move member variables from other\n",
    "        // Handle self-assignment\n",
    "        // Handle transfer of ownership\n",
    "        return *this;\n",
    "    }\n",
    "    // ...\n",
    "};\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## RAII\n",
    "RAII (Resource Acquisition Is Initialization) is a fundamental concept in C++ that ties resource management to the lifetime of objects. It ensures that resources (such as memory, file handles, or network connections) are acquired during object construction and released automatically when the object goes out of scope or is destroyed.\n",
    "\n",
    "RAII is closely related to the [Rule of Five](https://en.wikipedia.org/wiki/Rule_of_three_(C%2B%2B_programming)#Rule_of_five). When you define any of the special member functions (constructor, destructor, copy constructor, copy assignment operator, or move constructor/assignment operator), you are effectively managing resources within your class. By adhering to the Rule of Five, you ensure that resources are properly acquired, managed, and released throughout the object's lifetime, regardless of how the object is created, copied, moved, or destroyed.\n",
    "\n",
    "The followig example of a bank account class shows both RAII and the rule of five."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "cpp"
    }
   },
   "outputs": [],
   "source": [
    "//filename: bank_account.cpp\n",
    "//compile: g++ -std=c++17 -o bank_account bank_account.cpp\n",
    "//execute: ./bank_account\n",
    "\n",
    "#include <iostream>\n",
    "#include <string>\n",
    "\n",
    "class BankAccount {\n",
    "public:\n",
    "    // Default constructor\n",
    "    BankAccount() : _balance(0.0), _accountNumber(\"\") {\n",
    "        std::cout << \"Default constructor called\" << std::endl;\n",
    "    }\n",
    "\n",
    "    // Parameterized constructor\n",
    "    BankAccount(double initialBalance, const std::string& accountNumber)\n",
    "        : _balance(initialBalance), _accountNumber(accountNumber) {\n",
    "        std::cout << \"Parameterized constructor called\" << std::endl;\n",
    "    }\n",
    "\n",
    "    // Copy constructor\n",
    "    BankAccount(const BankAccount& other)\n",
    "        : _balance(other._balance), _accountNumber(other._accountNumber) {\n",
    "        std::cout << \"Copy constructor called\" << std::endl;\n",
    "    }\n",
    "\n",
    "    // Copy assignment operator\n",
    "    BankAccount& operator=(const BankAccount& other) {\n",
    "        std::cout << \"Copy assignment operator called\" << std::endl;\n",
    "        if (this != &other) {\n",
    "            _balance = other._balance;\n",
    "            _accountNumber = other._accountNumber;\n",
    "        }\n",
    "        return *this;\n",
    "    }\n",
    "\n",
    "    // Move constructor\n",
    "    BankAccount(BankAccount&& other) noexcept\n",
    "        : _balance(std::move(other._balance)), _accountNumber(std::move(other._accountNumber)) {\n",
    "        std::cout << \"Move constructor called\" << std::endl;\n",
    "        other._balance = 0.0;\n",
    "        other._accountNumber = \"\";\n",
    "    }\n",
    "\n",
    "    // Move assignment operator\n",
    "    BankAccount& operator=(BankAccount&& other) noexcept {\n",
    "        std::cout << \"Move assignment operator called\" << std::endl;\n",
    "        if (this != &other) {\n",
    "            _balance = std::move(other._balance);\n",
    "            _accountNumber = std::move(other._accountNumber);\n",
    "            other._balance = 0.0;\n",
    "            other._accountNumber = \"\";\n",
    "        }\n",
    "        return *this;\n",
    "    }\n",
    "\n",
    "    // Destructor\n",
    "    ~BankAccount() {\n",
    "        std::cout << \"Destructor called for account: \" << _accountNumber << std::endl;\n",
    "        // Release any resources acquired, if necessary\n",
    "    }\n",
    "\n",
    "    double getBalance() const { return _balance; }\n",
    "    std::string getAccountNumber() const { return _accountNumber; }\n",
    "\n",
    "private:\n",
    "    double _balance;\n",
    "    std::string _accountNumber;\n",
    "};\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## RAII and Scope\n",
    "The interaction between RAII and scope in C++ is crucial for managing resources efficiently and reliably. \n",
    "1. **Resource Acquisition**: When an object is created, its constructor is called. This constructor can acquire the necessary resources, such as dynamically allocated memory or opening a file.\n",
    "2. **Object Lifetime**: The object and the resources it holds remain valid until the end of its scope. This means that within the scope where the object is defined, you can safely use the acquired resources knowing they are available.\n",
    "3. **Scope Exit**: When the object goes out of scope, either due to reaching the end of a block or by an explicit delete operation, its destructor is automatically called. The destructor releases the acquired resources, ensuring they are properly deallocated or closed - i.e., the resource cleanup occurs. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "cpp"
    }
   },
   "outputs": [],
   "source": [
    "//filename: file_handler.cpp\n",
    "//compile: g++ -std=c++17 -o file_handler file_handler.cpp\n",
    "//execute: ./file_handler\n",
    "//After execution (to view the created file): cat example.txt\n",
    "#include <iostream>\n",
    "#include <fstream>\n",
    "\n",
    "class FileHandler {\n",
    "private:\n",
    "    std::ofstream file; // File stream for resource management\n",
    "\n",
    "public:\n",
    "    FileHandler(const std::string& filename) : file(filename) {\n",
    "        if (!file.is_open()) {\n",
    "            throw std::runtime_error(\"Failed to open file\");\n",
    "        }\n",
    "        std::cout << \"File opened successfully!\\n\";\n",
    "    }\n",
    "\n",
    "    ~FileHandler() {\n",
    "        file.close(); // Resource cleanup\n",
    "        std::cout << \"File closed\\n\";\n",
    "    }\n",
    "\n",
    "    void write(const std::string& data) {\n",
    "        file << data;\n",
    "    }\n",
    "};\n",
    "\n",
    "int main() {\n",
    "    try {\n",
    "        FileHandler handler(\"example.txt\"); // Object created, file opened\n",
    "\n",
    "        handler.write(\"Hello, RAII!\\n\"); // Using the file resource\n",
    "\n",
    "        // File automatically closed when 'handler' goes out of scope at the end of main()\n",
    "    } catch (const std::exception& e) {\n",
    "        std::cerr << \"Error: \" << e.what() << std::endl;\n",
    "        return 1;\n",
    "    }\n",
    "\n",
    "    return 0;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "FileHandler class manages the file resource.\n",
    "When handler object is created in the main() function, the file is opened.\n",
    "The file resource is automatically closed when handler goes out of scope at the end of main() function, regardless of how the block is exited (normally or due to an exception)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Sample LLM Prompts\n",
    "* Explain the 'Rule of Five' in C++ class design. Provide a detailed explanation of each operation, its purpose, and when it should be implemented. Include code examples for each operation.\n",
    "* Create a tutorial on implementing a proper default constructor in C++. Explain its importance, when it's automatically generated, and best practices for explicit definition. Include examples showing both trivial and non-trivial initializations.\n",
    "* Design a comprehensive guide on destructors in C++. Cover their role in resource management, when they're called, and how they relate to RAII. Provide examples of proper destructor implementation for classes with different resource types.\n",
    "* Develop a tutorial on copy constructors in C++. Explain deep vs. shallow copying, when to use each, and potential pitfalls. Include examples of copy constructor implementation for classes with various member types.\n",
    "* Discuss the move constructors and move assignment operators in C++11. Explain the concept of move semantics, their benefits, and how they improve performance. Include examples demonstrating proper implementation and use cases.\n",
    "* Explain the interaction between RAII and scope in C++. Explain how object lifetimes and resource management are tied together. Include examples demonstrating automatic resource cleanup in various scenarios, including exception handling"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Review Questions\n",
    "1. What are the “essential operations” for a class?\n",
    "2. What is a copy constructor?\n",
    "3. What is a copy assignment?\n",
    "4. What is the difference between copy assignment and copy initialization?\n",
    "5. What's the difference between a shallow copy and a deep copy?\n",
    "6. What is a destructor? When do we want one?\n",
    "7. What problem does RAII address?\n",
    "8. What is the benefit of implementing move semantics in a class?\n",
    "9. In the context of move operations, what is an rvalue reference?\n",
    "10. What is a potential issue with the compiler-generated copy constructor for classes with pointer members?\n",
    "\n",
    "[answers](answers/rq-16-answers.md)"
   ]
  }
 ],
 "metadata": {
  "language_info": {
   "name": "python"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
