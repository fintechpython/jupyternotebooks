# CPlusPlus / Standard Template Library
1. *What is an STL sequence?*

   An STL sequence is a range of elements defined by two iterators: begin and end. The element identified by begin is part of the sequence, but the end iterator points one beyond the last element of the sequence. This is known as a half-open range, denoted as [begin, end). This design allows for empty sequences when begin equals end.

2. *What is an STL iterator? What operations does it support?*

   An STL iterator is an object that points to (refers to) an element of a sequence (or one beyond the last element). It provides a way to access the elements of a container without exposing its underlying structure. Common operations supported by all iterators include:
   a. Comparison using == and !=
   b. Dereferencing using the unary * operator to access the element's value
   c. Moving to the next element using ++
   d. The -> operator for accessing members of pointed-to objects
   e. Some iterators (like random-access iterators) support additional operations such as:
   f. Addition and subtraction (+, -, +=, -=)
   g. Comparison (<, >, <=, >=)
   h. Subscripting ([])

3. *How do you iterate over a container using the STL?*

   There are several ways to iterate over a container using the STL:
   a. Using a traditional for loop with iterators:
   ```c++
   for (auto it = container.begin(); it != container.end(); ++it) {
      // Use *it to access the element
   }
   ```

   b. Using a range-based for loop
   ```c++
   for (const auto& element : container) {
       // Use element directly
   }
   ```

   c. Using STL algorithms that operate on ranges, such as std::for_each:
   ```c++
   std::for_each(container.begin(), container.end(), [](const auto& element) {
      // Process element
   });
   ```

4. *What does the STL algorithm find() do?*

   The STL algorithm find() searches for an element with a given value in a sequence. It returns an iterator to the first element that matches the given value. If no such element is found, it returns an iterator to the position one beyond the last element of the sequence.

5. *How does an STL algorithm take a container as an input argument?*

   STL algorithms typically don't take containers directly as input arguments. Instead, they operate on ranges defined by pairs of iterators. These iterators usually represent the beginning and end of the sequence to be processed. For example:
   ```c++
   std::vector<int> vec = {1, 2, 3, 4, 5};
   auto it = std::find(vec.begin(), vec.end(), 3);
   ```
   This design allows algorithms to work with any container type that provides suitable iterators, enhancing flexibility and reusability.

6. *How does an STL algorithm usually indicate “not found” or “failure”?*

   An STL algorithm usually indicates "not found" or "failure" by returning an iterator to the position one beyond the last element of the sequence being searched. This is typically the end() iterator of the container. For example, if std::find doesn't locate the element, it returns the end iterator:
   ```c++
   if (it == vec.end()) {
       std::cout << "Element not found";
   }
   ```

7. *What is an associative container? Give at least three examples.*

   An associative container is a data structure that stores elements in a way that enables fast retrieval based on keys. These containers provide efficient lookup, insertion, and deletion operations. In ordered associative containers, elements are sorted according to their keys, while unordered associative containers use hash tables for storage.
   Four examples of STL associative containers are:
   a. std::map: An ordered container of key-value pairs, where each key is unique.
   b. std::set: An ordered container that stores unique keys.
   c. std::unordered_map: An unordered container of key-value pairs, using a hash table for storage.
   d. std::unordered_set: An unordered container that stores unique keys, using a hash table.
