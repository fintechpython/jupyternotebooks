# CPlusPlus / Control Strucutres
1. *When should the for-loop be used and when should the while-loop be used?*

   Using a for-statement yields more easily understood and more maintainable code whenever a loop can be defined as a for-statement with a simple initializer, condition, and increment operation. Use a while-statement only when that’s not the case.

2. *What is the difference between the switch and if-else statements?*

   The switch statement is more efficient for evaluating multiple conditions against a single value, while if-else statements are more flexible for complex conditions.

3. *How can you skip an iteration in a loop?*

   Use the continue statement.

4. *How can you use a for loop to iterate over a two-dimensional array?*
   
   Use nested for loops, with the outer loop iterating over the rows and the inner loop iterating over the columns.

5. *What is the difference between the break and continue statements in a loop?*

   The break statement terminates the entire loop, while the continue statement skips the current iteration and moves to the next iteration.
