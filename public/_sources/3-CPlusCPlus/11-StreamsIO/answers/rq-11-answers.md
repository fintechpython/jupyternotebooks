# CPlusPlus / StreamsIO
1. *How do you open a file for writing in C++?*

   Using ofstream object, e.g., ofstream outFile("filename.txt");

2. *How do you check if a file was successfully opened?*

   By using the is_open() method or by checking the stream object in a boolean context

3. *What is the purpose of the eof() function?*

   It checks if the end of file has been reached

4. *What is the difference between cin.get() and cin.getline()?*

   cin.get() reads a single character, while cin.getline() reads a line of text

5. *How do you set the precision of floating-point output?*

   Using the setprecision() manipulator from the <iomanip> header

6. *How do you handle input errors when using cin?*

   By checking the stream state (e.g., cin.fail()) and using cin.clear() to reset error flags

7. *What is the difference between cin >> and getline() when reading strings?*

   cin >> stops at whitespace, while getline() reads until a newline character
