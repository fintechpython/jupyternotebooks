# Programming for Financial Technology

The guide provides much of the programming materials used in Duke University's 
[Masters of Engineering In Financial Technology](https://fintech.meng.duke.edu/) program.

Over the past decade, Python has evolved into the language of choice for many different application types. 
Python's extensive libraries such as [NumPy](https://numpy.org/), [pandas](https://pandas.pydata.org/), 
and [matplotlib](https://matplotlib.org/) provide robust tools for data manipulation, statistical analysis, 
and visualization, enabling developers to rapidly prototype and deploy sophisticated financial models and 
strategies. Moreover, Python's seamless integration with other languages and platforms (e.g., [Spark](https://spark.apache.org/))
facilitates interoperability and scalability, making it an indispensable tool for building scalable and resilient 
FinTech solutions that drive innovation and efficiency in today's fast-paced financial landscape.

Similarly, C++ serves as a cornerstone language for Financial Technology (FinTech), 
offering unparalleled performance, robustness, and flexibility to meet the demanding requirements
of this dynamic industry. Its speed and efficiency make it an ideal choice for developing
high-frequency trading systems, algorithmic trading platforms, risk management software, and 
complex mathematical models. With its close-to-the-hardware capabilities, C++ enables developers 
to optimize code for latency-sensitive applications, ensuring lightning-fast execution critical 
for capitalizing on market opportunities. Furthermore, C++'s extensive libraries and mature 
ecosystem provide a wealth of tools and frameworks tailored specifically for financial applications, 
empowering developers to build scalable, secure, and reliable solutions that drive innovation and 
efficiency in the ever-evolving landscape of finance.

This resource will eventually contain nine sections:
<ol start="0">
  <li> <b>Preliminaries:</b> Presents installing Python,  C++, and supporting tools such as <a href='https://code.visualstudio.com/'>VS Code</a> and <a href='https://git-scm.com/'>Git</a>.</li>
  <li> <b>Python Basics:</b> Provides an introduction to programming using Python </li>
  <li> <b>Advanced Python:</b> Contains additional Python topics, that while useful, are not critical to programming</li>
  <li> <b>C++:</b> Introduction to programming in C++.
  <li> <b>Databases:</b> Discusses an overview of database design, including SQL and NoSQL, and how to interact with those resources in Python.</li>
  <li> <b>Data Science:</b> Covers using a variety of data science libraries to perform data cleaning, handling, analytics, machine learning, and visualization.</li>
  <li> <b>Web Development:</b> Provides an introduction to web development, including HTML, CSS, and JavaScript, as well as server-side programming with <a href='https://flask.palletsprojects.com/'>Flask</a>.</li>
  <li> <b>Big Data:</b> Presents processing big data in Python using Apache Spark.</li>
  <li> <b>The Tools:</b> Introduces a series of tools commonly used in programming.</li>
</ol>


```{tableofcontents}
```
