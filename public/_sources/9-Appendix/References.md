# References

To access the O'Reilly Platform, first visit [https://go.oreilly.com/duke-university](https://go.oreilly.com/duke-university)

## Python
Lubanovic, Bill. _Introducing Python: Modern Computing in Simple Packages_ (2nd Ed.). O'Reilly Media, Inc. 2019.
[Amazon](https://www.amazon.com/Introducing-Python-Modern-Computing-Packages/dp/1492051365/)  [O'Reilly](https://learning.oreilly.com/library/view/introducing-python-2nd/9781492051374/)


## Web Development
Meyer, Eric, and Weyl, Estelle. _CSS: The Definitive Guide_ (5th Ed.). O'Reilly Media, Inc. 2023.
[O'Reilly](https://learning.oreilly.com/library/view/css-the-definitive/9781098117603/)

Flanagan, David. _JavaScript: The Definitive Guide_ (7th ed). O'Reilly Media, Inc. 2020. [O'Reilly](https://learning.oreilly.com/library/view/javascript-the-definitive/9781491952016/)