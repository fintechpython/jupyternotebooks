# Development Environment

This section will cover and use various tools and techniques common to modern web development, including different software programs used to write, manage, and execute the code for your web application. This chapter provides an overview of the various components.

If you have not yet done so, follow the instructions in the Preliminaries section on [Establishing your Environment](../0-Preliminaries/01-Environment.md) to ensure you have 
[Python](https://www.python.org/) and [Visual Studio Code](https://code.visualstudio.com/) installed.

While not directly covered in this section, you will need to utilize the 
[terminal/shell](../8-The%20Tools/1-Bash-Basics.md), [git](../8-The%20Tools/4-Git-Introduction.md),
and [Python virtual environments](../8-The%20Tools/6-PythonVirtualEnvironments.md).  Refer to those sections as needed.


## Web Browser
The first thing you'll need is a web browser for viewing the web pages you make! We recommend you install and use [**Chrome**](https://www.google.com/chrome/browser/desktop/index.html), which comes with an effective set of built-in [developer tools](https://developers.google.com/web/tools/chrome-devtools/) that will be especially useful.

We recommend Google Chrome for several reasons:
- According to [statcounter](https://gs.statcounter.com/browser-market-share), Chrome holds a ~65% market share on all platforms as of September 2024.
- Its rendering engine, [Blink](https://en.wikipedia.org/wiki/Blink_(browser_engine) is also used by Microsoft's Edge browser.
- Its Javascript engine, [V8](https://en.wikipedia.org/wiki/V8_(JavaScript_engine) has widely been adopted and is the core of the Node.js runtime environment.

You can access the Chrome Developer tools by selecting **View > Developer > Developer Tools** from Chrome's main menu (<span class="keys"><kbd class="key">Command(⌘)</kbd>+
<kbd class="key">Option(⌥)</kbd>+<kbd class="key">i</kbd></span> on a Mac, 
<span class="keys"><kbd class="key">Ctrl</kbd>+
<kbd class="key">Shift</kbd>+<kbd class="key">i</kbd></span> on Windows). You will pretty much always want to have these tools open when doing web development, especially when including interactivity via JavaScript.


## Visualization Library
This section utilizes the [Plotly Javascript](https://plotly.com/graphing-libraries/) library. While many fantastic visualization libraries exist,
we selected Plotly due to the similarities between the Python and Javascript libraries and their interactive capabilities.

## Application Framework
In this section, we utilize [Flask](https://flask.palletsprojects.com/), a popular, lightweight web framework for building web applications in Python. Flask is designed to be simple, flexible, and easy to use, making it an excellent choice for both beginners and experienced developers who want to create web applications quickly.

[Django](https://www.djangoproject.com/) is the other popular web frame for Python.  It is a high-level, full-featured web framework and follows the "batteries-included" 
philosophy, meaning it comes with many built-in features and tools, making it a powerful choice for both small and large projects.

We are using Flask over Django for several reasons:
- **Flask** is a **micro-framework** that gives you full control over your project’s architecture. It doesn't impose a rigid structure, so you're free to choose libraries, databases, and other components as needed. This allows us to focus on the core technologies underlying web development (the HTTP protocol, HTML, CSS, and Javascript) rather than learning a large framework that
abstracts/hides many of those technologies:
- Flask is a great choice for **small to medium-sized applications** or quick **prototypes** where you don’t need all of Django’s built-in features (like the admin panel or authentication). If your project doesn’t require complex functionality from the start, Flask’s minimalist nature allows you to build apps faster without unnecessary overhead.
- Flask has a **gentler learning curve** compared to Django, especially for beginners. It doesn’t require you to learn about Django’s ORM, middleware, or templating system before you can get started. You can focus on the essentials of web development while learning Flask and gradually add complexity as needed.

## Client-side Framework
Similarly, for a client-side framework we will primarily utilize [Bootstrap](https://getbootstrap.com/). Originally developed by Twitter, Bootstrap is a popular, open-source front-end framework that helps developers design responsive, mobile-first websites quickly using pre-built HTML, CSS, and JavaScript components. It provides a grid system and various UI elements like buttons, forms, and navigation bars, allowing for consistent and easily customizable web design.

As With the application frameworks, other alternatives exist.  Most notably, [React](https://react.dev/), [Vue](https://vuejs.org/), and [Angular](https://angular.dev/) focus on building dynamic, interactive web applications by managing state, rendering views efficiently, and handling complex logic. Our primary goal is for you to understand the underlying components so you can learn these frameworks as required for specific projects.

## Temporary Web Server
For writing client-side HTML, CSS, and Javascript, you can often open the corresponding web page directly in a browser.  From the File menu of the browser, select "Open File..."

You can also open the HTML file directly from the terminal.

On MacOS:
```bash
open pageName.html
```

On Windows, within WSL:
```bash
explorer.exe pageName.html
```

Other times, running a temporary webserver to serve static content may be necessary. Python provides such a webserver, which can be started with
```bash
python -m http.server -b 127.0.0.1
```
You'll then need to use a url of [http://localhost:8000/](http://localhost:8000/) to open the page.  This servers content from the current working directory in which the 
Python command was executed.

Within VS Code, you can install the [LiveServer](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer) extension.

## Original Info 340 Acknowledgements
Chapter was originally authored by [Joel Ross](https://faculty.washington.edu/joelross/) and [Mike Freeman](http://mfviz.com/#/). Some content was originally adapted from [tutorials](https://drstearns.github.io/tutorials/) by [David Stearns](https://www.linkedin.com/in/david-stearns-09a27319/). Some structure and explanations inspired by [Learning Web Design](https://learningwebdesign.com/) by Jennifer Robbins.