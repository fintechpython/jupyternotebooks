{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "0d096246",
   "metadata": {},
   "source": [
    "# SQL Alchemy\n",
    "\n",
    "[SQL Alchemy](https://www.sqlalchemy.org/) is a popular cross-database library that abstracts differences across different implementations of SQL in the various vendors.  At its highest level, SQL Alchemy provides an Object Relational Mapper (ORM) that binds Python classes to relational tables.  Underneath that layer is the \"Core,\" which provides an SQL Expression Language to attempt to provide a more Python-oriented way to query databases, and the \"Engine,\" which forms the gateway to the underlying database.  This engine translates various forms of SQL and can pool database connections for faster connectivity.  At the lowest level of the architecture, SQL Alchemy is built on top of Python's standard database library - [DBAPI](https://peps.python.org/pep-0249/). \n",
    "\n",
    "<img src='images/sqla_arch.png' width=400 alt='SQL Alchemy Architecture'>\n",
    "\n",
    "We'll start with the lowest level and then cover the ORM layer."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "aa9f90d8",
   "metadata": {},
   "source": [
    "## Connecting to a database\n",
    "\n",
    "Generally, to get started connecting to a database, you'll need to provide a database URI (connection string). For SQL Alchemy, the connect string has the format:\n",
    "\n",
    "*dialect + driver :// user : password @ host :port / dbname*\n",
    "\n",
    "|Item | Description|\n",
    "|:-----|:-----|\n",
    "| dialect | Database Type |\n",
    "| driver  | Specific driver to connect to the database type.  Optional |\n",
    "| user and password | Database authentication settings if needed |\n",
    "| host | which server is the database on? |\n",
    "| :port | which port to connect to? Will default to the vendor's standard port |\n",
    "| dbname | which database to initially connect to on the server |\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "8674d13a",
   "metadata": {},
   "outputs": [],
   "source": [
    "from sqlalchemy import create_engine\n",
    "from sqlalchemy import text"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "62e5f2d5",
   "metadata": {},
   "source": [
    "create_engine() is a factory to create database connections.  However, it does not actually connect to the database until connect() is called."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "bae6c643",
   "metadata": {},
   "outputs": [],
   "source": [
    "engine = create_engine(\"sqlite:///data/chinook.db\")\n",
    "connection = engine.connect()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9c563977",
   "metadata": {},
   "source": [
    "## Executing Queries\n",
    "\n",
    "Now, we can run SQL queries on that connection.  To execute standard \"text\" qeueries, we'll need to use the *text* object."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 30,
   "id": "bd926f95",
   "metadata": {},
   "outputs": [],
   "source": [
    "stmt = text('select firstname, lastname from customers order by lastname, firstname')\n",
    "result = connection.execute(stmt)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0a676a14",
   "metadata": {},
   "source": [
    "The *result* object that is returned is a special proxy object that acts as a database cursor to iterate over the rows returned from the query's execution.  The object also provides metadata about the results."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a3b827a7",
   "metadata": {},
   "source": [
    "If we are only interested in the first record, we can call ```first()``` which returns the first row and closes the result object. This row/record object looks like a standard named tuple for Python:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 31,
   "id": "45bc2b24",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "('Roberto', 'Almeida')\n",
      "Roberto\n",
      "Almeida\n",
      "{'FirstName': 'Roberto', 'LastName': 'Almeida'}\n",
      "Roberto\n"
     ]
    }
   ],
   "source": [
    "first_record = result.first()\n",
    "print(first_record)\n",
    "print(first_record[0])        # access tuple by position\n",
    "print(first_record[1])\n",
    "print(first_record._mapping)  # provides a dictionary interface\n",
    "print(first_record.FirstName) # access tuple by column name.  note this is case sensitive"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1af65801",
   "metadata": {},
   "source": [
    "Note that once a result object is closed, you can no longer call methods on that objcet.  Whatever resources have been allocated are freed."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "72572b78",
   "metadata": {},
   "source": [
    "We can also use common, idiomatic Python patterns with the results."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 34,
   "id": "7e4e7cea",
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Almeida, Roberto\n",
      "Barnett, Julia\n",
      "Bernard, Camille\n",
      "Brooks, Michelle\n",
      "Brown, Robert\n",
      "Chase, Kathy\n",
      "Cunningham, Richard\n",
      "Dubois, Marc\n",
      "Fernandes, João\n",
      "Francis, Edward\n",
      "Girard, Wyatt\n",
      "Gonçalves, Luís\n",
      "Gordon, John\n",
      "Goyer, Tim\n",
      "Gray, Patrick\n",
      "Gruber, Astrid\n",
      "Gutiérrez, Diego\n",
      "Hansen, Bjørn\n",
      "Harris, Frank\n",
      "Holý, Helena\n",
      "Hughes, Phil\n",
      "Hämäläinen, Terhi\n",
      "Johansson, Joakim\n",
      "Jones, Emma\n",
      "Kovács, Ladislav\n",
      "Köhler, Leonie\n",
      "Leacock, Heather\n",
      "Lefebvre, Dominique\n",
      "Mancini, Lucas\n",
      "Martins, Eduardo\n",
      "Mercier, Isabelle\n",
      "Miller, Dan\n",
      "Mitchell, Aaron\n",
      "Murray, Steve\n",
      "Muñoz, Enrique\n",
      "Nielsen, Kara\n",
      "O'Reilly, Hugh\n",
      "Pareek, Manoj\n",
      "Peeters, Daan\n",
      "Peterson, Jennifer\n",
      "Philips, Mark\n",
      "Ralston, Frank\n",
      "Ramos, Fernanda\n",
      "Rocha, Alexandre\n",
      "Rojas, Luis\n",
      "Sampaio, Madalena\n",
      "Schneider, Hannah\n",
      "Schröder, Niklas\n",
      "Silk, Martha\n",
      "Smith, Jack\n",
      "Srivastava, Puja\n",
      "Stevens, Victor\n",
      "Sullivan, Ellie\n",
      "Taylor, Mark\n",
      "Tremblay, François\n",
      "Van der Berg, Johannes\n",
      "Wichterlová, František\n",
      "Wójcik, Stanisław\n",
      "Zimmermann, Fynn\n"
     ]
    }
   ],
   "source": [
    "result = connection.execute(stmt)\n",
    "for first, last in result:\n",
    "    print(\"{}, {}\".format(last,first))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b705cea7",
   "metadata": {},
   "source": [
    "We can also put all of the results into a list."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 36,
   "id": "4cc19b2a",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{'FirstName': 'Julia', 'LastName': 'Barnett'}"
      ]
     },
     "execution_count": 36,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "result = connection.execute(stmt)\n",
    "all_records = result.all()         # returns a list of the row/records object\n",
    "all_records[1]._mapping            # for the second element, retrieve the dictionay interface"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "832de619",
   "metadata": {},
   "source": [
    "## Closing a connection\n",
    "Once you have completed your work with the database, you should call ```close()```.  This releases the connection back into the connection pool for future use."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "182803ca",
   "metadata": {},
   "source": [
    "## Context Manager\n",
    "You can also use a context mananger to implicitly close a connection."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 40,
   "id": "f5eb809f",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "347\n"
     ]
    }
   ],
   "source": [
    "with engine.connect() as connection:\n",
    "    result = connection.execute(text(\"select artists.name, albums.title from artists inner join albums on artists.artistid = albums.artistid\"))\n",
    "    all_rows = result.all()\n",
    "    print(len(all_rows))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cbedd245",
   "metadata": {},
   "source": [
    "## Transactions\n",
    "By default, SQLAlchemy places the connection into a transaction and you will need to explicitly commit the transaction for the change to persist."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 54,
   "id": "4bf86597",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "None\n"
     ]
    }
   ],
   "source": [
    "with engine.connect() as connection:\n",
    "    result = connection.execute(text(\"insert into artists (artistid,name) values(99999,'Fintech')\"))\n",
    "    \n",
    "with engine.connect() as connection:\n",
    "    result = connection.execute(text(\"select * from artists where artistid=99999\"))\n",
    "    print(result.first())   # no record available.  Transaction was never committed"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 55,
   "id": "fef39781",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "(99999, 'Fintech')\n"
     ]
    }
   ],
   "source": [
    "with engine.connect() as connection:\n",
    "    result = connection.execute(text(\"insert into artists (artistid,name) values(99999,'Fintech')\"))\n",
    "    connection.commit()     # mark the change as successful.  Database will commit the change\n",
    "    \n",
    "with engine.connect() as connection:\n",
    "    result = connection.execute(text(\"select * from artists where artistid=99999\"))\n",
    "    print(result.first())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 56,
   "id": "20da267c",
   "metadata": {},
   "outputs": [],
   "source": [
    "# clean up the change (removes the inserted recod)\n",
    "with engine.connect() as connection:\n",
    "    result = connection.execute(text(\"delete from artists where artistid=99999\"))\n",
    "    connection.commit()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f9d63171",
   "metadata": {},
   "source": [
    "You can also use the ```begin()``` method in the ```with``` clause to automatically commit the transaction\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 59,
   "id": "2f6d43d7",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "(99999, 'Fintech')\n"
     ]
    }
   ],
   "source": [
    "with engine.begin() as connection:\n",
    "    result = connection.execute(text(\"insert into artists (artistid,name) values(99999,'Fintech')\"))\n",
    "    # Transaction committed by context manager \n",
    "    \n",
    "with engine.connect() as connection:\n",
    "    result = connection.execute(text(\"select * from artists where artistid=99999\"))\n",
    "    print(result.first())   \n",
    "\n",
    "with engine.begin() as connection:\n",
    "    result = connection.execute(text(\"delete from artists where artistid=99999\"))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "80e114c7",
   "metadata": {},
   "source": [
    "You can also put the connection into \"auto commit\" mode, in which each statement forms a transaction itself and is automatically committed."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 61,
   "id": "63c988bc",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "(99999, 'Fintech')\n"
     ]
    }
   ],
   "source": [
    "with engine.connect() as connection:\n",
    "    connection = connection.execution_options(isolation_level=\"AUTOCOMMIT\")\n",
    "    result = connection.execute(text(\"insert into artists (artistid,name) values(99999,'Fintech')\"))\n",
    "    \n",
    "with engine.connect() as connection:\n",
    "    result = connection.execute(text(\"select * from artists where artistid=99999\"))\n",
    "    print(result.first())   # Transaction auto-committed through execution options\n",
    "    \n",
    "with engine.begin() as connection:\n",
    "    result = connection.execute(text(\"delete from artists where artistid=99999\"))  \n",
    "    # committed implicitly with begin() and context manager"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "47ce072b",
   "metadata": {},
   "source": [
    "To cancel a transaction explicitly, use ```rollback()```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bdcee14b",
   "metadata": {},
   "source": [
    "## Parameterized Query\n",
    "\n",
    "A [parameterized query](https://en.wikipedia.org/wiki/Prepared_statement) (prepared statement) contains placeholders for parameters whose values are specified when the query executes.\n",
    "\n",
    "These queries have a number of advantages:\n",
    "1. To avoid [SQL injection](https://en.wikipedia.org/wiki/SQL_injection) attacks.\n",
    "2. To avoid needing to encode special characters in field values.\n",
    "3. Performance\n",
    "\n",
    "As these queries are created on the target database system, that system compiles the query.  This both prevents the structure of the the query from being altered through an injection attack as well as speeds performance as the query plan is pre-determined.\n",
    "\n",
    "SQL Alchemy utilizes a _:variableName_ scheme for parameters."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 63,
   "id": "863960f5",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "(1, 'AC/DC')\n"
     ]
    }
   ],
   "source": [
    "with engine.connect() as connection:\n",
    "    query = text(\"select artistID, name from artists where artistid = :id\")\n",
    "    result = connection.execute(query,{\"id\": 1})\n",
    "    print(result.first())  "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e2ecbb5a",
   "metadata": {},
   "source": [
    "With insert and update queries, we can also send multiple records to be executed.  Behind the scenes, the SQL statement is executed once for each parameter set. This is also known as \"[executemany](https://docs.sqlalchemy.org/en/20/glossary.html#term-executemany)\"."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 82,
   "id": "0c89bd81",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "2 record(s) inserted\n",
      "[(99998, 'RockStart'), (99999, 'Fintech')]\n",
      "2 record(s) removed\n"
     ]
    }
   ],
   "source": [
    "with engine.connect() as connection:\n",
    "    query = text(\"insert into artists (artistid,name) values(:id, :name)\")\n",
    "    values = [ { \"id\" : 99999, \"name\" :\"Fintech\" }, {\"id\" : 99998, \"name\" : \"RockStart\"}]\n",
    "    result = connection.execute(query,values)\n",
    "    print(result.rowcount,\"record(s) inserted\")\n",
    "    connection.commit()     # mark the change as successful.  Database will commit the change\n",
    "    \n",
    "    query = text(\"select artistID, name from artists where artistid > :id\")\n",
    "    result = connection.execute(query,{\"id\": 99990})\n",
    "    print(result.all())\n",
    "    \n",
    "    # Now, remove those added records\n",
    "    query = text(\"delete from artists where artistid > :id\")\n",
    "    result = connection.execute(query,{\"id\": 99990})\n",
    "    print (result.rowcount, \"record(s) removed\")\n",
    "    connection.commit()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 79,
   "id": "dde499aa",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "0\n"
     ]
    }
   ],
   "source": [
    "with engine.connect() as connection:\n",
    "    query = text(\"delete from artists where artistid > :id\")\n",
    "    result = connection.execute(query,{\"id\": 99990})\n",
    "    print(result.rowcount)\n",
    "    connection.commit()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e7cb999d",
   "metadata": {},
   "source": [
    "## Converting SQL Alchemy Results to a Pandas DataFrame"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 90,
   "id": "d0329ad7",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "    Country          InvoiceDate  Total\n",
      "0    Brazil  2010-03-11 00:00:00   3.98\n",
      "1    Brazil  2010-06-13 00:00:00   3.96\n",
      "2    Brazil  2010-09-15 00:00:00   5.94\n",
      "3    Brazil  2011-05-06 00:00:00   0.99\n",
      "4    Brazil  2012-10-27 00:00:00   1.98\n",
      "..      ...                  ...    ...\n",
      "407   India  2009-07-08 00:00:00   5.94\n",
      "408   India  2010-02-26 00:00:00   1.99\n",
      "409   India  2011-08-20 00:00:00   1.98\n",
      "410   India  2011-09-30 00:00:00  13.86\n",
      "411   India  2012-05-30 00:00:00   8.91\n",
      "\n",
      "[412 rows x 3 columns]\n"
     ]
    }
   ],
   "source": [
    "import pandas as pd\n",
    "with engine.connect() as connection:\n",
    "    query = text(\"select c.country, i.invoicedate,i.total from customers c, invoices i where c.customerid=i.customerid\")\n",
    "    result = connection.execute(query)\n",
    "    df = pd.DataFrame(result.all())\n",
    "    df.columns = result.keys()\n",
    "    print(df)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f2f91d9d",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
