# LLM Prompts

Initial thoughts on prompts for LLMs 
- give direction
- specify context
- if you need output in a specific format, specify it.

- Avoid the tempation to say "write a python program to <solve my homework>"

## Core Python

### Introduction
- How should I use a LLM to help me learn how to program?

How should I use a LLM to help me learn how to program?  For each point that you make, provide three sample effective prompts.

Whate are some bad examples in using a LLM when learning how to program

- Explain the following code:
  ```
  import math

  num_sec_in_day = 24 * 60 * 60
  num_tries_per_second = 1_000_000_000
  num_cities = 20
  num_route_permutations = math.factorial(num_cities)
  num_days = num_route_permutations // num_tries_per_second // num_sec_in_day
  print(f"{num_days:,}")
  ```
  
### Problem Solving by Progamming
- Explain Drew Hilton's Seven Steps for Programming
- Show an example of those seven steps to compute a stock's volatility for the past week.  Assume that the inputs are the last 10 closing prices for a particular stock, say IBM.  Use Python as the programming language.
- What is pseudocode?
- In programming, what is an IPO chart?  Give an example that calculates the average of a ist of numbers.
  -- Can you explain more about process in that chart?

### Data: Types, Values, Variables, and Names
- Explain objects in Python as if I am just learning the programming language.
- why is it important to follow naming conventions?

how do boolean values differ from integers in Python?  When should I use each type






## C++
- Why does the following program fail to compile?
```
#include <iostream>
#include <string>
using namespace std; 

int main(S) {
    string name;
    
    cout << "What is your name?" << endl;
    if (getline(cin,name)) {
        cout << "It's nice to meet you, " << name << "XXXXX" << "\n"
    }
    else {
        cout << "No input available\n";
    
}
```

- how does the for loop in c++ differ from that in PYthon?

## Graphs
- Explain how Prim's algorithm works
- step through an example of using prim's algorithm.


dice game - win 2* for 5-9, lose otherwise.  Build a simulation

2 - 1
3 - 2
4 - 3
5 - 4
6 - 5
7 - 6
8 - 5
9 - 4
10 - 3
11 - 2
12 - 1 

5-9: 24/36
Others: 12 0
