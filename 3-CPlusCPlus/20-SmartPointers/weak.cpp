#include <memory>
#include <iostream>

struct Resource {
    Resource() { std::cout << "Resource Acquired\n"; }
    ~Resource() { std::cout << "Resource Destroyed\n"; }
    void sayHello() { std::cout << "Hello from Resource\n"; }
};

int main() {
    std::shared_ptr<Resource> ptr1 = std::make_shared<Resource>();
    std::weak_ptr<Resource> weakPtr = ptr1;

    {
        std::shared_ptr<Resource> ptr2 = weakPtr.lock();
        if (ptr2) {
            ptr2->sayHello();
            std::cout << "Reference Count: " << ptr1.use_count() << "\n";
        }
    }

    ptr1.reset(); // Destroy the resource

    std::shared_ptr<Resource> ptr3 = weakPtr.lock();
    if (!ptr3) {
        std::cout << "Resource no longer exists.\n";
    }

    return 0;
}