#include <memory>
#include <iostream>

struct Resource {
    Resource() { std::cout << "Resource Acquired\n"; }
    ~Resource() { std::cout << "Resource Destroyed\n"; }
    void sayHello() { std::cout << "Hello from Resource\n"; }
};

int main() {
    std::shared_ptr<Resource> ptr1 = std::make_shared<Resource>();
    {
        std::shared_ptr<Resource> ptr2 = ptr1; // Shared ownership
        ptr2->sayHello();
        std::cout << "Reference Count: " << ptr1.use_count() << "\n";
    } // ptr2 goes out of scope

    std::cout << "Reference Count after ptr2 is destroyed: " << ptr1.use_count() << "\n";
    return 0;
}
