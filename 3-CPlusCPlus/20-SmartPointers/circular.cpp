#include <memory>
#include <iostream>

struct B; // Forward declaration

struct A {
  std::shared_ptr<B> b_ptr;
  ~A() { std::cout << "A destroyed\n"; }
};

struct B {
  std::shared_ptr<A> a_ptr; // Causes circular reference
  ~B() { std::cout << "B destroyed\n"; }
};

int main() {
  auto a = std::make_shared<A>();
  auto b = std::make_shared<B>();
  a->b_ptr = b;
  b->a_ptr = a; // Circular reference

  // Neither A nor B will be destroyed
  return 0;
}
