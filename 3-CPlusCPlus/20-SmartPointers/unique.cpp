#include <memory>
#include <iostream>

struct Resource {
    Resource() { std::cout << "Resource Acquired\n"; }
    ~Resource() { std::cout << "Resource Destroyed\n"; }
    void sayHello() { std::cout << "Hello from Resource\n"; }
};

int main() {
    // Creating a unique_ptr
    std::unique_ptr<Resource> ptr1 = std::make_unique<Resource>();
    ptr1->sayHello();

    // Transferring ownership
    std::unique_ptr<Resource> ptr2 = std::move(ptr1);
    if (!ptr1) {
        std::cout << "ptr1 is now empty\n";
    }

    return 0;
}