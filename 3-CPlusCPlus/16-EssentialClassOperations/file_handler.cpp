#include <iostream>
#include <fstream>

class FileHandler {
private:
    std::ofstream file; // File stream for resource management

public:
    FileHandler(const std::string& filename) : file(filename) {
        if (!file.is_open()) {
            throw std::runtime_error("Failed to open file");
        }
        std::cout << "File opened successfully!\n";
    }

    ~FileHandler() {
        file.close(); // Resource cleanup
        std::cout << "File closed\n";
    }

    void write(const std::string& data) {
        file << data;
    }
};

int main() {
    try {
        FileHandler handler("example.txt"); // Object created, file opened

        handler.write("Hello, RAII!\n"); // Using the file resource

        // File automatically closed when 'handler' goes out of scope at the end of main()
    } catch (const std::exception& e) {
        std::cerr << "Error: " << e.what() << std::endl;
        return 1;
    }

    return 0;
}
