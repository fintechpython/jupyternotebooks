#include <iostream>
#include <string>

class BankAccount {
public:
    // Default constructor
    BankAccount() : _balance(0.0), _accountNumber("") {
        std::cout << "Default constructor called" << std::endl;
    }

    // Parameterized constructor
    BankAccount(double initialBalance, const std::string& accountNumber)
        : _balance(initialBalance), _accountNumber(accountNumber) {
        std::cout << "Parameterized constructor called" << std::endl;
    }

    // Copy constructor
    BankAccount(const BankAccount& other)
        : _balance(other._balance), _accountNumber(other._accountNumber) {
        std::cout << "Copy constructor called" << std::endl;
    }

    // Copy assignment operator
    BankAccount& operator=(const BankAccount& other) {
        std::cout << "Copy assignment operator called" << std::endl;
        if (this != &other) {
            _balance = other._balance;
            _accountNumber = other._accountNumber;
        }
        return *this;
    }

    // Move constructor
    BankAccount(BankAccount&& other) noexcept
        : _balance(std::move(other._balance)), _accountNumber(std::move(other._accountNumber)) {
        std::cout << "Move constructor called" << std::endl;
        other._balance = 0.0;
        other._accountNumber = "";
    }

    // Move assignment operator
    BankAccount& operator=(BankAccount&& other) noexcept {
        std::cout << "Move assignment operator called" << std::endl;
        if (this != &other) {
            _balance = std::move(other._balance);
            _accountNumber = std::move(other._accountNumber);
            other._balance = 0.0;
            other._accountNumber = "";
        }
        return *this;
    }

    // Destructor
    ~BankAccount() {
        std::cout << "Destructor called for account: " << _accountNumber << std::endl;
        // Release any resources acquired, if necessary
    }

    double getBalance() const { return _balance; }
    std::string getAccountNumber() const { return _accountNumber; }

private:
    double _balance;
    std::string _accountNumber;
};

int main(int argc, char *argv[]) {
    return 0;
}