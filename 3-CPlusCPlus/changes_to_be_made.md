4. *Which operator do you use to read into a variable?*
Current Location: 03-ExpressionsAndOperators
Status: Needs to be moved.
Suggested New Location: 11-StreamsIO

5. *What is the difference between an expression and a statement?*
Current Location: 03-ExpressionsAndOperators
Status: Needs to be moved.
Suggested New Location: 04-ControlStructures

6. *How can you use a for loop to iterate over a two-dimensional array?*
Current Location: 04-ControlStructures
Status: Needs to be moved.
Suggested New Location: 14-Built-InArrays

7. *How do we syntactically distinguish between a function declaration and a function definition?*
Current Location: 05-Functions
Status: Requires additional material.
Suggested Content: Add a section explaining the difference between function declarations and definitions.

8. *Why is it a good idea to initialize variables as they are declared?*
Current Location: 05-Functions
Status: Needs to be moved.
Suggested New Location: 02-VariablesAndTypes
Suggested Content: Add content on the importance of initializing variables.

11. *Name four major types of errors and briefly define each one.*
Current Location: 08-Exceptions
Status: Requires additional material.
Suggested Content: Add a section at the beginning of the chapter introducing different types of errors, including syntax, runtime, logical, and compilation errors.


13. *What is the difference between cin.get() and cin.getline()?*
Current Location: 11-StreamsIO
Status: Needs to be moved.
Suggested New Location: 06-Strings
Suggested Content: Add a subsection near the getline() examples comparing different input methods for characters and strings.

14. *What is the difference between cin >> and getline() when reading strings?*
Current Location: 11-StreamsIO
Status: Needs to be moved.
Suggested New Location: 06-Strings

15. *What is the difference between shallow copy and deep copy?*
Current Location: 12-Classes
Status: Requires additional material.
Suggested Content: The material provided mentions deep copy in the context of copy constructors but does not fully explain the differences between shallow and deep copy. To make this question fully answerable, include a more detailed explanation of shallow copy, particularly how it duplicates object references without creating new objects, and how deep copy creates entirely new objects with the same values.

16. *How does generic programming differ from object-oriented programming?*
Current Location: 12-Classes
Status: Needs to be removed, already in 17-GenericProgrammingAndTemplates.

17. *What happens if you dereference a null pointer?*
Current Location: 13-Pointers
Status: Requires additional material.
Suggested Content: Explicitly state the consequences of dereferencing a null pointer.

20. *How do you copy an array?*
Current Location: 14-Built-InArrays
Status: Requires additional material.
Suggested Content: Add content on methods to copy arrays.

21. *What happens if you try to access an array element outside its bounds?*
Current Location: 14-Built-InArrays
Status: Requires additional material.
Suggested Content: Explicitly state that C++ does not provide automatic bounds checking for arrays and explain that accessing out-of-bounds elements leads to undefined behavior.

22. *What's the proper way to return an array from a function?*
Current Location: 14-Built-InArrays
Status: Requires additional material.
Suggested Content: Explicitly state that returning raw arrays from functions is not possible. Provide alternatives like returning std::array, std::vector, or dynamically allocated arrays.

23. *What is a memory leak, and how can it occur?*
Current Location: 15-MemoryManagement
Status: Requires additional material.
Suggested Content: Add a specific section on memory leaks and their causes.

24. *What is the difference between copy assignment and copy initialization?*
Current Location: 16-EssentialClassOperations
Status: Requires additional material.
Suggested Content: Add an explanation of copy initialization after the "Copy Constructor" section (16.3).

25. *What's the difference between a shallow copy and a deep copy?*
Current Location: 16-EssentialClassOperations
Status: Requires additional material.
Suggested Content: Add a section that explicitly explains shallow versus deep copying, including the implications for classes with pointer members.

