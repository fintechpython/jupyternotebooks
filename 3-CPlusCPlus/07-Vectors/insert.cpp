#include <vector> 
#include <iostream>
using std::vector; 

// Defining a templated function so that we can print vectors of any type
template<typename T>
void printVector(const std::vector<T>& vec) {
    std::cout << "{ ";
    bool first = true;
    for (const auto& elem : vec) {   // This is a fencepost loop.  Special case the first
        if (first) {
            std::cout << elem;
            first = false;
        }
        else {
            std::cout << ", " << elem;
        }
    }
    std::cout << " }" <<  "\n";
}

int main(int argc, char *argv[]) {
    vector<int> vec{1, 2,  4, 5}; 

    vec.insert(vec.begin() +2 ,3);
    vec.insert(vec.end(),8);
    vec.insert(vec.end()-1,7);   // what happens if you try vec.end() + 1 ?
    printVector(vec);

    // Insert multiple copies of an element
    vec.insert(vec.begin(), 2, 0); // Insert two '0's at the start
    printVector(vec);

    // remove the first element
    vec.erase(vec.begin());
    printVector(vec);

    // remove the last two elements
    vec.erase(vec.end()-2,vec.end());
    printVector(vec);

    vec.clear();
    printVector(vec);
}