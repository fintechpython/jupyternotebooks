#include <iostream>
#include <string>
#include <vector>

/**
 * @brief Split apart a line based upon delimiters in the string
 * 
 * @param line The string to be split
 * @param delim a string specifying the delimeter used to split apart the string
 * @return a vector of string objects
 */
std::vector<std::string> splitLine(std::string line, std::string delim) {
    std::vector<std::string> result;

    auto start = 0U;
    auto end = line.find(delim);
    while (end != std::string::npos) {
        result.push_back(line.substr(start, end - start));
        start = end + delim.length();
        end = line.find(delim, start);
    }
    std::string last = line.substr(start, end);
    if (last.length()) {
        result.push_back(last);
    }

    return result;
}

int main(int argc, char *argv[]) {
    std::vector<std::string> samples = {"", "java:c:c++:Python:Pascal:Basic", "C++", "C++:Java:"};
    std::vector<int> expected_sizes = {0,6,1,2};
    std::string delim = ":";

    for (size_t i = 0U; i< samples.size(); i++) {
        std::string line = samples[i];
        std::cout << "Testing: " << line << std::endl;
        std::vector<std::string> splits = splitLine(line,delim);
        for (std::string item: splits) {
            std::cout << item <<  "\n";
        }
        if (splits.size() == expected_sizes[i]) {
            std::cout << "result size matched" <<  "\n";
        }
        else {
            std::cout << "ERROR - result size unexpected, expected: " << expected_sizes[i] <<  "\n";
        }
        std::cout << "Finished: " << line <<  "\n";


    }
    
}