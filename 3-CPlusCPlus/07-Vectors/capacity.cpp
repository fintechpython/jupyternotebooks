#include <vector> 
#include <iostream>
using std::vector; 


int main(int argc, char *argv[]) {
    vector<int> vec{1, 2, 3};

    std::cout << "Initial size: " << vec.size() <<  "\n";
    std::cout << "Initial capacity: " << vec.capacity() <<  "\n";

    vec.push_back(4);  // note the doubling of the capacity
    std::cout << "Size: " << vec.size() <<  "\n";
    std::cout << "Capacity: " << vec.capacity() <<  "\n";
} 