#include <random>
#include <iostream>
#include <vector>    
#include <string>
using std::cout, std::vector;  

int main() {
    std::random_device rd;  // Random engine
    std::mt19937 eng(rd());   // Mersenne Twister engine seeded with rd().  Can fix the seed with an int
    
    // Create an object that produces random numbers from the Uniform distribution between 1 and 1000
    std::uniform_int_distribution<> distr(1, 1000);  

    int random_integer = distr(eng);
    cout << "Random number: " << random_integer <<  "\n";

    vector<int> items; 
    int capacity_increases = 0;
    int last_capacity = 0; 
    for (int i=1; i < 1000; i++) {
        items.push_back( distr(eng));  // add a random number to the vector
        if (items.capacity() != last_capacity) { // check if the vector capacity increases
            capacity_increases++;
            last_capacity = items.capacity();
            cout << i << " " << last_capacity <<  "\n";
        }
    }
    cout << "Total number of increases: " << capacity_increases <<  "\n";

    return 0;
}
