#include <vector> 
using std::vector; 

int main(int argc, char *argv[]) {
    int n = 4;
    vector<int> vec;                         // empty vector holding objects of type int.
    vector<int> five{1, 2, 3, 4, 5};         // vector contains as many elements as there are 
                                             // initializers (values with { } on the right-hand side)
    vector<int> alt_five = {1, 2, 3, 4, 5};  // equivalent to the above declaration
    vector<int> another(alt_five);           // another has a copy of each element of alt_five
    vector<int> v1(n);                       // vector has n copies of a value-initialized object.
                                             // (would be the default constructor or zero for numbers)
    vector<int> v2(n,5);                     // holds n copies of the value 5.
}