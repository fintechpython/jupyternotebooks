#include <vector> 
#include <iostream>
using std::vector; 


int main(int argc, char *argv[]) {
    vector<int> vec{1, 2, 3};

    for (auto it = vec.begin(); it != vec.end(); it++) {
        std::cout << *it << ' ';
    }
    for (auto it = vec.end(); it >= vec.begin(); it--) { // note the comparison
        std::cout << *it << ' ';
    }
    std::cout << std::endl;

}