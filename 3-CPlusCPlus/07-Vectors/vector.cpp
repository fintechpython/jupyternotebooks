#include <iostream>
#include <vector>              // Include the vector header
#include <string>
#include <algorithm>           // Will use to sort the list

using std::cin, std::cout;  // bring in these names from the std namespace
using std::string, std::vector;        

int main(int argc, char *argv[]) {
    string item;
    vector<string> items;    // creates an empty vector;

    cout << "Enterlots of strings to add to the vector, ctrl-d to stop: ";
    while (cin >> item) {
        items.push_back(item);
    }

    cout << "Number of items: " << items.size() << "\n";

    std::sort(items.begin(), items.end());         // begin() and end() are iterators - see below

    cout << "Iterate over the list with a for-each loop :" <<  "\n";
    for (string s: items) {
        cout << s <<  "\n";
    }

    return EXIT_SUCCESS;
}