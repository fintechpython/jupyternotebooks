#include <vector> 
#include <iostream>
using std::vector; 


int main(int argc, char *argv[]) {
    vector<int> vec{1, 2, 3};

    std::cout << "1 - size: " << vec.size() << ", capacity: " << vec.capacity() <<  "\n";

    vec.reserve(10);  // increase capacity to 10
    std::cout << "2 - size: " << vec.size() << ", capacity: " << vec.capacity() <<  "\n";

    vec.push_back(4);
    vec.shrink_to_fit();
    std::cout << "3 - size: " << vec.size() << ", capacity: " << vec.capacity() <<  "\n";

    vec.resize(2); // truncates to just the first two elements
    std::cout << "4 - size: " << vec.size() << ", capacity: " << vec.capacity() <<  "\n";

}