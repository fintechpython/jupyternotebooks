class Account {
public:
    Account(double initialAmount): amount{initialAmount} {};
    void calculate() { amount += amount * interestRate;}
    static double getRate() { return interestRate;}
    static void setRate(double newRate) { interestRate = newRate;}
private:
    double amount;
    static double interestRate;
};