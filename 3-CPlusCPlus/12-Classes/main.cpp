#include "point.hpp"
#include <iostream>

int main() {
  std::cout << "Hello World!\n";

  Point a;                // Declares a, initalizes with the default constructor
  Point b{5, 7};          // declares b, initilizes with a constructor
  std::cout << "Point a: " << a << std::endl;
  std::cout << "Point b: " << b << std::endl;

  b.scale(2);           // calls method
  std::cout << "Point b(scaled): " << b << std::endl;   //uses overload operator << on ostream
  Point d = b * 2;       // declares d, assigns it as the result of b * 2.  * is defined in Point
  std::cout << "Point d(b*2): " << d << std::endl;
 
  Point e = 2 * b;       // uses the freestanding method.
  std::cout << "Point e(2*b): " << e << std::endl;

  e += b;       
  std::cout << "Point e += b: " << e << std::endl;
}