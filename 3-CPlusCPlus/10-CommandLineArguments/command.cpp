#include <iostream>
#include <vector> 
#include <string>

using std::cin, std::cout;  // bring in these names from the std namespace
using std::string, std::vector;        

int main(int argc, char *argv[]) {
    std::vector<std::string> arguments(argv, argv + argc);  // to skip the program name, add + 1 to argv 
                                                            // in the first argument

    cout << "Command-line arguments:" <<  "\n";
    for (string argument: arguments) {
        cout << argument <<  "\n";
    }

}
