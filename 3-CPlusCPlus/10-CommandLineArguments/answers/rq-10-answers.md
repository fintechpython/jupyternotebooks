# CPlusPlus / CommandLineArguments
1. *What are the parameters typically used in the main function to handle command line arguments?*
   
   int main(int argc, char* argv[])

2. *What does argc represent in the main function?*

   The number of command line arguments, including the program name.

3. *What does argv represent in the main function?*

   An array of C-style strings containing the command line arguments.

4. *What is a common way to handle unknown arguments?*

   Print an error message and/or display usage information.

5. *How can you check if any user arguments were provided to the program?*

   if(argc > 1)
