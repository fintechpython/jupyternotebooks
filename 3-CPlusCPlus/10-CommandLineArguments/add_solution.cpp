#include <iostream>
#include <string>

using std::cout;
using std::string; 

int convertInt(string s) {
    std::size_t idx = 0;
    int result = std::stoi(s,&idx);
    if (idx != s.size()) {
        throw std::invalid_argument("unprocessed input: "+s);
    }
    return result; 
}

int main(int argc, char *argv[]) {
    if (argc != 3) {
        std::cerr << "Usage: "<< argv[0] << "num1 num2\n";
        return EXIT_FAILURE;
    }
    int n1;
    int n2;

    try {
        n1 = convertInt(string{argv[1]});
        n2 = convertInt(string{argv[2]});
        cout << n1 + n2 << "\n";
    } catch (const std::invalid_argument& a) {
        std::cerr << "Invalid argument: " << a.what() << "\n";
        return EXIT_FAILURE;
    } catch (const std::out_of_range& r) {
        std::cerr << "out of range of double: " << r.what() << "\n";
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}