# CPlusPlus / Inheritance
1. *What is the syntax for declaring a derived class in C++?*

   ```c++
   class DerivedClass : [visibility_modifier] BaseClass { /* class body */ };
   ```
   
2. *What is the difference between public and protected inheritance?*

   In public inheritance, public members of the base class become public in the derived class. In protected inheritance, public members of the base class become protected in the derived class.

3. *What is an abstract class in C++?*

   An abstract class is a class that contains at least one pure virtual function and cannot be instantiated.

4. *How do you declare a pure virtual function in C++?*

   By using the "virtual" keyword and assigning 0 to the function declaration, e.g., virtual void func() = 0;

5. *What is the purpose of the 'override' keyword in C++?*

   It explicitly indicates that a function is meant to override a virtual function from a base class.

6. *Why should destructors in base classes be declared as virtual?*

   To ensure that the correct destructor is called when deleting a derived class object through a base class pointer.

7. *Can constructors be inherited in C++?*

   No, constructors are not inherited, but they can be called from derived class constructors.
