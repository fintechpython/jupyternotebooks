#include<iostream>
using namespace std;

class Base {
public:
    virtual void show() {
        cout << "This is base class.\n";
    }
};

class Derived : public Base {
public:
    void show() override {
        Base::show();
        cout << "This is derived class.\n";
    }
};

class Another : public Base {
public:
    void show() override {
        cout << "This is another class.\n";
    }
};

int main() {
    Derived d;
    d.show();
    Another a;
    a.show();
}