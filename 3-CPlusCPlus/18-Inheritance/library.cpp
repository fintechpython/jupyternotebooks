#include <iostream>
#include "librarysystem.hpp"
#include <vector>

int main() {

    EBook ebook("The Joy of Programming", "John Slankas", 2023, "Technology", 500, 2.5, "ePub");
    PrintedBook printedBook("Introducing Python, 2nd Edition", "Bill Lubanvoic", 2019, "Technology", 627, "Softcover", 2.35);
    DigitalMovie digitalMovie("The Shawshank Redemption", "Frank Darabont", 1994, "Drama", 144, 1.5, "MP4");
    BluRay bluRay("The Godfather", "Francis Ford Coppola", 1972, "Drama", 195, "1080p");
    DVD dvd("The Matrix", "Lana Wachowski, Lilly Wachowski", 1999, "Action", 156, 2);


    std::vector<LibraryItem*> libraryItems;
    libraryItems.push_back(&ebook);
    libraryItems.push_back(&printedBook);
    libraryItems.push_back(&digitalMovie);
    libraryItems.push_back(&bluRay);
    libraryItems.push_back(&dvd);

    for (const auto& item : libraryItems) {
        item->displayDetails();
        std::cout << "---------------------------" << "\n";
    }

    return EXIT_SUCCESS;
}