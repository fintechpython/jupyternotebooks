#include <iostream>

int main() {
    int a = 5;
    int b = 10;

    // Pointer
    int *ptr = &a;
    std::cout << "*ptr = " << *ptr << std::endl; // Outputs: 5
    ptr = &b;
    std::cout << "*ptr = " << *ptr << std::endl; // Outputs: 10

    // Reference
    int &ref = a;
    std::cout << "ref = " << ref << std::endl; // Outputs: 5
    

    return 0;
}