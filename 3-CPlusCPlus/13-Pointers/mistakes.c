int main(int argc, char *argv[]) {
    int i   = 10;
    int *ip = &i;

    ip = i;    /* ip is an address, but i is just a value */
    *ip = &i;  /* &i is address but *ip is not */

    return 0;
}