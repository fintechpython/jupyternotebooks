#include <iostream>

int& badReference() {
    int temp = 10;
    return temp; // temp is destroyed when the function exits
}

int main() {
    int &a = badReference();
 
    std::cout << "ref = " << a << std::endl; // behavior is undefined.
    

    return 0;
}