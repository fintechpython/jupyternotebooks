#include <iostream>
using std::cout;

void swap(int *a, int *b) {
    int temp = *a;
    *a = *b;
    *b = temp;
    cout << "in swap: a's value " << a << "\n";
    cout << "in swap, a points to " << *a << "\n";
}

int main(int argc, char *argv[]) {
    int a = 10;
    int b = 15;
    cout << "before swap: a address " << &a << "\n";
    swap(&a,&b);
    cout << "after swap: a address " << &a << "\n";
    cout << "(a,b) = (" << a << "," << b << ")\n";

    return 0;
}