#include <iostream>
using std::cout;

class point {
    public:
    int x;
    int y;
};

int main(int argc, char *argv[]) {
    point p;
    
    p.x = 1;
    p.y = 5;

    point *q = &p;

    cout << "(x,y): (" << (*q).x << "," << q->y << ")\n";

    return EXIT_SUCCESS;
}