#include <iostream>
using std::cout;

int main () {
   int  var = 1842; 
   int  *ip;        /* declare pointer variable (item #1) */

   ip = &var;       /* store the address of var in ip (item #2) */

   cout << "Address of var: " << &var << "\n";
   cout << "Address stored in ip:" << ip << "\n";
   cout << "dereferencing ip: " << *ip << "\n";   /* (item #3) */

   *ip = 1992;   /*changing the contents (value) stored at a particular location (item #4) */
   cout << "Contents of var: " << var << "\n";

   return 0;
}