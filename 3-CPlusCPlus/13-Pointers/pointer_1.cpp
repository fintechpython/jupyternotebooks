#include <iostream>
using std::cout;

int main(int argc, char *argv[]) {
    int b = 1842;    /* define a "regular" variable b */
    int c = 1992;
    int *a;           /* declare a pointer variable a */
  
    a = &b;           /* store the address of b in a */

    cout << "Value of b: " << b << "\n";
    cout << "Value of a*: " << *a << "\n";
    cout << "value of a: " << a << "\n";     /* a is a pointer, and contains the address of b */
    cout << "Address of b: " << &b << "\n";  
    cout << "Address of a: " << &a << "\n";

    cout << "Value of c: " << c << " (before assignment)\n";
    c = *a;  /* c now contains 1842 */
    cout << "Value of c: " << c << " (after assignment)\n";

    return 0;
}