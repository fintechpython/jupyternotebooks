{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Pointers\n",
    "\n",
    "A pointer is a variable that stores/contains a memory address. Often, these addresses are just those of other variables. However, in later lectures/pages, we will dynamically allocate memory and assign the initial address of that allocated memory to a pointer variable. \n",
    "\n",
    " In some ways, we've already been using pointers through object references in Python.  Consider this Python code:\n",
    "\n",
    "```python\n",
    "primes = [1,2,3,5,7,11,13,17,19]\n",
    "p = primes\n",
    "```\n",
    "\n",
    "`primes` and `p` both refer to the same underlying list object.  We just have not had the capability to directly access or manipulate those address values. Notice that they have they same object ID:\n",
    "\n",
    "```python\n",
    "//filename: python_id.py\n",
    "//compile: not applicable\n",
    "//execute: python python_id.py\n",
    "primes = [1,2,3,5,7,11,13,17,19]\n",
    "p = primes\n",
    "print(id(primes));\n",
    "print(id(p));\n",
    "```\n",
    "\n",
    "Output: (numbers may differ, but both lines will be the same)\n",
    "```\n",
    "4374464832\n",
    "4374464832\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the center of the image below, we have an example of some arbitrary computer memory.  We have defined a variable `b` that's stored at location 1008.  We then define a pointer named `a`.  That variable's contents contains the address of `b`. \n",
    "\n",
    "```c++\n",
    "int b = 1842;\n",
    "int *a = &b;\n",
    "```\n",
    "\n",
    "`int *a` defines an int pointer.  The unary operator `&` (address of) gives the address of an object in memory. The address of operator can only be applied to variables and array elements.  To access the contents of the memory location that a pointer references, we use the unary operator * (dereferences).  Going back to the third Python notebook, you can equate this address to the address of a building in a city - it just references a location in memory.\n",
    "\n",
    "Adding a `*` after any type name changes that type declaration to a pointer of that type.\n",
    "\n",
    "![](images/rvm4WIV.png)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "cpp"
    }
   },
   "outputs": [],
   "source": [
    "//filename: pointer_1.cpp\n",
    "//compile: g++ -std=c++17 -o pointer_1 pointer_1.cpp\n",
    "//execute: ./pointer_1\n",
    "#include <iostream>\n",
    "using std::cout;\n",
    "\n",
    "int main(int argc, char *argv[]) {\n",
    "    int b = 1842;    /* define a \"regular\" variable b */\n",
    "    int c = 1992;\n",
    "    int *a;           /* declare a pointer variable a */\n",
    "  \n",
    "    a = &b;           /* store the address of b in a */\n",
    "\n",
    "    cout << \"Value of b: \" << b << \"\\n\";\n",
    "    cout << \"Value of a*: \" << *a << \"\\n\";\n",
    "    cout << \"value of a: \" << a << \"\\n\";     /* a is a pointer, and contains the address of b */\n",
    "    cout << \"Address of b: \" << &b << \"\\n\";  \n",
    "    cout << \"Address of a: \" << &a << \"\\n\";\n",
    "\n",
    "    cout << \"Value of c: \" << c << \" (before assignment)\\n\";\n",
    "    c = *a;  /* c now contains 1842 */\n",
    "    cout << \"Value of c: \" << c << \" (after assignment)\\n\";\n",
    "\n",
    "    return 0;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Another way to think about pointers is that a variable name (or a reference variable) directly refers to a value while a point indirectly refers to a value.  The term `indirection` means referencing a value through a pointer.  `&` creates a pointer value(address). `*` \"dereferences\" a pointer value(address) to access the underlying value.\n",
    "\n",
    "Remember that every variable is stored somewhere in memory, so we should then be able to store that address in another variable.\n",
    "\n",
    "## Initializing Pointers\n",
    "\n",
    "To initialize a pointer, assign it the address value of a variable/object generated with the `&` address operator. If such a variable is not available when defining the pointer variable, use the value of `nullptr`. Any pointer with this value \"points to nothing\", and we generally refer to this as a \"null pointer\".  Prior to C++11, null pointers were specified by `NULL` or `0`.  However, these are both integer literals that can lead to type-related issues - is this a pointer value (i.e., an address) or an integer value?\n",
    "\n",
    "## Pointers and Functions\n",
    "\n",
    "Within C++, we can pass arguments to functions in three ways:\n",
    "\n",
    "1. pass-by-value\n",
    "2. pass-by-reference with a reference argument(s)\n",
    "3. pass-by-reference with pointer argument(s) \n",
    "\n",
    "The Functions notebook contained a bad implementation for a function `swap`. By default, C++ passes parameters by value. As such, any changes made to the parameters will only be reflected within the function itself.  Once the function exits, those parameters (variables) fall out of scope and their storage is released. In a prior Docable, we demonstrated using references with swap, which is preferred.  However, due the legacy with C, we can use pointer argument(s) to implement pass-by-reference.  This scenario is technically pass-by-value, but we  pass the addresses of variables into functions and then dereference those pointers to access and manipulate those contents.  Here is an implementation through using pointers for the two parameters:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "cpp"
    }
   },
   "outputs": [],
   "source": [
    "//filename: swap_pointer.cpp\n",
    "//compile: g++ -std=c++17 -o swap_pointer swap_pointer.cpp\n",
    "//execute: ./swap_pointer\n",
    "#include <iostream>\n",
    "using std::cout;\n",
    "\n",
    "void swap(int *a, int *b) {\n",
    "    int temp = *a;\n",
    "    *a = *b;\n",
    "    *b = temp;\n",
    "    cout << \"in swap: a's value \" << a << \"\\n\";\n",
    "    cout << \"in swap, a points to \" << *a << \"\\n\";\n",
    "}\n",
    "\n",
    "int main(int argc, char *argv[]) {\n",
    "    int a = 10;\n",
    "    int b = 15;\n",
    "    cout << \"before swap: a address \" << &a << \"\\n\";\n",
    "    swap(&a,&b);\n",
    "    cout << \"after swap: a address \" << &a << \"\\n\";\n",
    "    cout << \"(a,b) = (\" << a << \",\" << b << \")\\n\";\n",
    "\n",
    "    return 0;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "<a href=\"https://pythontutor.com/render.html#code=%23include%20%3Ciostream%3E%0Ausing%20std%3A%3Acout%3B%0A%0Avoid%20swap%28int%20*a,%20int%20*b%29%20%7B%0A%20%20%20%20int%20temp%20%3D%20*a%3B%0A%20%20%20%20*a%20%3D%20*b%3B%0A%20%20%20%20*b%20%3D%20temp%3B%0A%20%20%20%20cout%20%3C%3C%20%22in%20swap%3A%20a's%20value%20%22%20%3C%3C%20a%20%3C%3C%20%22%5Cn%22%3B%0A%20%20%20%20cout%20%3C%3C%20%22in%20swap,%20a%20points%20to%20%22%20%3C%3C%20*a%20%3C%3C%20%22%5Cn%22%3B%0A%7D%0A%0Aint%20main%28int%20argc,%20char%20*argv%5B%5D%29%20%7B%0A%20%20%20%20int%20a%20%3D%2010%3B%0A%20%20%20%20int%20b%20%3D%2015%3B%0A%20%20%20%20cout%20%3C%3C%20%22before%20swap%3A%20a%20address%20%22%20%3C%3C%20%26a%20%3C%3C%20%22%5Cn%22%3B%0A%20%20%20%20swap%28%26a,%26b%29%3B%0A%20%20%20%20cout%20%3C%3C%20%22after%20swap%3A%20a%20address%20%22%20%3C%3C%20%26a%20%3C%3C%20%22%5Cn%22%3B%0A%20%20%20%20cout%20%3C%3C%20%22%28a,b%29%20%3D%20%28%22%20%3C%3C%20a%20%3C%3C%20%22,%22%20%3C%3C%20b%20%3C%3C%20%22%29%5Cn%22%3B%0A%0A%20%20%20%20return%200%3B%0A%7D&amp;cumulative=false&amp;curInstr=0&amp;heapPrimitives=nevernest&amp;mode=display&amp;origin=opt-frontend.js&amp;py=cpp_g%2B%2B9.3.0&amp;rawInputLstJSON=%5B%5D&amp;textReferences=false\">View the execution</a>\n",
    "\n",
    "As another way to relate this back to Python, imagine that we are passing mutable objects into the function.  However, instead of accessing those variables normally, we must access them through pointers.\n",
    "\n",
    "## Quick Review: Basic Usage\n",
    "\n",
    "Key operations with pointers:\n",
    "\n",
    "1. Define a pointer variable: `_type_ *_pointerName_`\n",
    "2. Assign the address of another variable to that pointer:  `_pointerName_ = &_variableName_`\n",
    "3. Access the the value at the address in the pointer: `*_pointerName_`_ _(dereferencing)\n",
    "4. Changing the contents of an address: _`*pointerName = newValue`_\n",
    "\n",
    " Note that we use the address-of operator `&` to get the address of a variable and the dereferencing operator `*` to access the value at that address."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "cpp"
    }
   },
   "outputs": [],
   "source": [
    "//filename: review.cpp\n",
    "//compile: g++ -std=c++17 -o review review.cpp\n",
    "//execute: ./review\n",
    "#include <iostream>\n",
    "using std::cout;\n",
    "\n",
    "int main () {\n",
    "   int  var = 1842; \n",
    "   int  *ip;        /* declare pointer variable (item #1) */\n",
    "\n",
    "   ip = &var;       /* store the address of var in ip (item #2) */\n",
    "\n",
    "   cout << \"Address of var: \" << &var << \"\\n\";\n",
    "   cout << \"Address stored in ip:\" << ip << \"\\n\";\n",
    "   cout << \"dereferencing ip: \" << *ip << \"\\n\";   /* (item #3) */\n",
    "\n",
    "   *ip = 1992;   /*changing the contents (value) stored at a particular location (item #4) */\n",
    "   cout << \"Contents of var: \" << var << \"\\n\";\n",
    "\n",
    "   return 0;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "ip is said to \"point to\" var.\n",
    "\n",
    "\n",
    "## nullptr Pointer\n",
    "\n",
    "Introduced C++ 11, `nullptr `is a keyword that represents the null pointer value. Prior to then, C++ (and C) used `NULL` - a special constant that stands for 0 (zero). As mentioned above, you should use `nullptr `as it is a type-safe and clear way to represent a null pointer.\n",
    "\n",
    "Use `nullptr `for these reasons:  \n",
    "1. To initialize a pointer variable that has not been assigned a value memory address (remember variables in C++  when uninitialized can hold any value).  \n",
    "2. To check for `nullptr `before accessing the variable.  \n",
    "3. To pass a null pointer to a function when we do not want to pass a valid memory address (the function many alter its behavior based upon `nullptr `). \n",
    "4. To assign to a pointer variable after we have de-allocated memory that pointer referenced (presented in a later notebook)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "cpp"
    }
   },
   "outputs": [],
   "source": [
    "//filename: null.cpp\n",
    "//compile: g++ -std=c++17 -o null null.cpp\n",
    "//execute: ./null\n",
    "#include <iostream>\n",
    "\n",
    "int main(int argc, char *argv[]) {\n",
    "    int i   = 10;\n",
    "    int *ip = nullptr;\n",
    "\n",
    "    std::cout << \"ip value: \" << ip << \"\\n\";\n",
    "\n",
    "    return 0;\n",
    "}\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Just because it prints `0`, don't interchange `nullptr `with `NULL `or `0`.\n",
    "\n",
    "## Pointers and Classes / Structs\n",
    "\n",
    "We can also have pointers to class and structs. However, when referencing these pointers, we need to either use parenthesis around the pointer dereferencing or use the operator `->`.   If `p` is a pointer to a a class/structure, then `p->_member-of-class/structure_` refers to the particular member.  The reason for this is that the class/structure member operator `. `has a higher precedence than `*`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "cpp"
    }
   },
   "outputs": [],
   "source": [
    "//filename: structure.cpp\n",
    "//compile: g++ -std=c++17 -o structure structure.cpp\n",
    "//execute: ./structure\n",
    "#include <iostream>\n",
    "using std::cout;\n",
    "\n",
    "class point {\n",
    "    public:\n",
    "    int x;\n",
    "    int y;\n",
    "};\n",
    "\n",
    "int main(int argc, char *argv[]) {\n",
    "    point p;\n",
    "    \n",
    "    p.x = 1;\n",
    "    p.y = 5;\n",
    "\n",
    "    point *q = &p;\n",
    "\n",
    "    cout << \"(x,y): (\" << (*q).x << \",\" << q->y << \")\\n\";\n",
    "\n",
    "    return EXIT_SUCCESS;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Common Mistakes\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- Accessing a memory location that has been freed/deallocated. This also includes accessing memory allocated\n",
    "  by a function once that function exists - commonly referred to as a dangling pointer or dangling reference.  See the second example below.\n",
    "- Trying to access a memory location when the pointer variable contains `nullptr` or was never properly initialized.\n",
    "- In pointer arithmetic, accessing beyond the bounds of the allocated memory segment (e.g., going beyond the end of an array).\n",
    "\n",
    "![](images/compiler_complaint_xkcd.png)\n",
    "[https://xkcd.com/371/](https://xkcd.com/371/)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "c"
    }
   },
   "outputs": [],
   "source": [
    "//filename: mistakes.c\n",
    "//compile: gcc -std=gnu-99 -o mistakes mistakes.c\n",
    "//execute: ./mistakes\n",
    "int main(int argc, char *argv[]) {\n",
    "    int i   = 10;\n",
    "    int *ip = &i;\n",
    "\n",
    "    ip = i;    /* ip is an address, but i is just a value */\n",
    "    *ip = &i;  /* &i is address but *ip is not */\n",
    "\n",
    "    return 0;\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "//filename: dangling.cpp\n",
    "//compile: g++ -std=c++17 -o dangling dangling.cpp\n",
    "//execute: ./dangling\n",
    "// Note: Program compiles with a warning message: \n",
    "//       warning: reference to stack memory associated with local variable 'temp' returned\n",
    "\n",
    "#include <iostream>\n",
    "\n",
    "int& badReference() {\n",
    "    int temp = 10;\n",
    "    return temp; // temp is destroyed when the function exits\n",
    "}\n",
    "\n",
    "int main() {\n",
    "    int &a = badReference();\n",
    " \n",
    "    std::cout << \"ref = \" << a << std::endl;  // behavior is undefined\n",
    "    \n",
    "    return 0;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Morale: don't ignore compiler errors.  (Note: the above is actually a C program.  In C++, this would not compile.)\n",
    "\n",
    "## Pointer Usage in Modern C++\n",
    "\n",
    "While pointers can be exceedingly elegant and powerful, they can also be exceedingly difficult to work to properly program with nuanced defects difficult to find.  New C++ projects should use reference variables and smart pointers.  Where contiguous memory segments are needed, use `std::array` and `std:vector` objects from the STL instead of built-in arrays (or pointer-based arrays).  Use C++ string objects rather than pointer-based (`char *`) C-style strings.  However, legacy code and certain data structures (e.g., trees and graphs) may necessitate the use of pointers.  \n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Pointers vs References\n",
    "\n",
    "| Feature | Pointer | Reference\n",
    "|---------|---------|----------\n",
    "| Syntax  | `int *ptr;` | `int &ref = var;` |\n",
    "| Initialization | Can be declared without initialization |\tMust be initialized at declaration\n",
    "| Nullability | Can be `nullptr` | Always an alias to an existing object\n",
    "| Dereferencing Syntax | Requires `*` operator | Automatically dereferenced"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "cpp"
    }
   },
   "outputs": [],
   "source": [
    "//filename: compariaon.c\n",
    "//compile: g++ -std=c++17 -o comparison comparison.cpp\n",
    "//execute: ./comparison\n",
    "#include <iostream>\n",
    "\n",
    "int main() {\n",
    "    int a = 5;\n",
    "    int b = 10;\n",
    "\n",
    "    // Pointer\n",
    "    int *ptr = &a;\n",
    "    std::cout << \"*ptr = \" << *ptr << std::endl; // Outputs: 5\n",
    "    ptr = &b;\n",
    "    std::cout << \"*ptr = \" << *ptr << std::endl; // Outputs: 10\n",
    "\n",
    "    // Reference\n",
    "    int &ref = a;\n",
    "    std::cout << \"ref = \" << ref << std::endl; // Outputs: 5\n",
    "    \n",
    "\n",
    "    return 0;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Additional Tutorials\n",
    "Understanding and effectively using pointers is one of the more challenging aspects of C++ programming. \n",
    "\n",
    "Here are two resources for C++:\n",
    "- [https://www.learncpp.com/cpp-tutorial/introduction-to-pointers/](https://www.learncpp.com/cpp-tutorial/introduction-to-pointers/) Learn CPP is an excellent resource for C++ in general.\n",
    "- [Learning C++ Pointers for REAL Dummies](http://alumni.cs.ucr.edu/~pdiloren/C++_Pointers/wherelive.htm) Introduces pointers through the concept of houses and their addresses.\n",
    "\n",
    "The following resources was created for C, but still broadly applies to C++: [A Tutorial on Pointers and Arrays in C](https://github.com/jflaherty/ptrtut13/blob/master/md/pointers.md)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Sample LLM Prompts\n",
    "* Write a comprehensive guide on pointer declaration and initialization in C++. Explain the syntax for declaring pointers of different data types, how to initialize pointers with addresses, and the importance of initializing pointers to nullptr when not immediately assigning an address. Include code examples for each concept.\n",
    "* Explain what dereferencing means, how to use the dereference operator (*), and the difference between a pointer variable and the value it points to. Provide examples demonstrating how to read and modify values through pointers.\n",
    "* Create an in-depth explanation of pointer arithmetic in C++. Cover topics such as incrementing and decrementing pointers, adding and subtracting integers from pointers, and how pointer arithmetic relates to array indexing. Include examples that demonstrate these concepts with different data types.\n",
    "* Explain the difference between pass-by-reference with a reference argument(s) and pass-by-reference with pointer argument(s)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Review Questions\n",
    "\n",
    "1. What advantages does `nullptr` provide over `NULL` or using the value `0`?\n",
    "2. What happens if you dereference a null pointer?\n",
    "3. How do you dynamically allocate memory for a single integer in C++?\n",
    "4. How do you free dynamically allocated memory in C++?\n",
    "5. What is the difference between int* const ptr and const int* ptr?\n",
    "\n",
    "\n",
    "[answers](answers/rq-13-answers.md)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
