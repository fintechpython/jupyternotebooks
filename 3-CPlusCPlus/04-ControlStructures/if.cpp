#include <iostream>
using std::cout;
using std::endl;

int main(int argc, char *argv[]) {
    int age = 15;
    if (age >= 16) {
        cout << "You can drive!\n";
        cout << "You need a license first, though...\n";
    }
    else {
        cout << "You are not old enough to drive.\n";
        cout << "You will need to wait until you are 16.\n";
    }
}