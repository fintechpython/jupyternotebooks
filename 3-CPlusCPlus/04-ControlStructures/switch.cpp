#include <iostream>
using std::cout;
using std::endl;

int main(int argc, char *argv[]) {
    char suite = 'H';
    switch(suite) {
        case 'C': cout << "Clubs\n";    break;
        case 'D': cout << "Diamonds\n"; break;
        case 'H': cout << "Hearts\n";   /* fall through */
        case 'S': cout << "Spades\n";   break;
        default:  cout << "Unknown suite: " << suite << "\n";
    }

}