#include <iostream>
using std::cout;
using std::endl;

int main(int argc, char *argv[]) {
    for (int i=0; i < 10; i++) {
        if (i % 2 == 1) {
            cout << i << "\n";
        }
    }
    return 0;
}