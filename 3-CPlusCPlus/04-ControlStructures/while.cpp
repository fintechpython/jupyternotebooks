#include <iostream>
using std::cout;

int main(int argc, char *argv[]) {
    int i = 1;
    while (i < 6) {
        cout << i << "\n";
        i++;
    }
    return EXIT_SUCCESS;
}