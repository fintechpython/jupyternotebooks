#include <iostream>
using std::cout;
using std::endl;

int main(int argc, char *argv[]) {
    int grade = 78;

    if (grade >= 90) {
        cout << "A\n";
    } 
    else if (grade >= 80) {
        cout << "B\n";
    } 
    else if (grade >= 70) {
        cout << "C\n";
    }
    else if (grade >= 60) {
        cout << "D\n";
    }
    else {
        cout << "F\n";
    } 
}