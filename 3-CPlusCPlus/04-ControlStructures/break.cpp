#include <iostream>
using std::cout;


int main(int argc, char *argv[]) {
    int i = 1;
    while (i < 6) {
        if (i % 3 == 0) { 
            break;
        } else {
            cout << i << "\n";
            i++;
        }
    }
    cout << "After the while loop" << "\n";

    return 0;
}