# CPlusPlus / Introduction


1. *What is the purpose of the #include directive?*

   The #include directive instructs the computer to make available facilities from files.

2. *What is the difference between a source file and an object file?*

   A Source file is a file containing source code, i.e., the code as produced by a programmer and readable by other programmers. Object files contain object code from a compiler intended as input for a linker.