#include <iostream>
using namespace std;

int max(int a, int b) {
    cout << "int max called: ";
    if (a > b) { return a; }
    else       { return b; }
}

double max(double a, double b=0) {
    cout << "double max called: ";
    if (a > b) { return a; }
    else       { return b; }
}

int main(int argc, char *argv[]) {
    cout << max(42, 39) << "\n";
    cout << max(2.72, 3.14) << "\n";
    cout << max(-3.14) << "\n";
}