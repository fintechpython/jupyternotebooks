#include <iostream>
using namespace std;

int main() { 
    cout << "Please a stock name and price: ";  
    string name;          // string variable
    double price = -1.0;  // double variable  
    cin >> name;          // read a string 
    cin >> price;         // read the price into a double
    
    if (price > 0) {
        cout << name << " has a stock price of $" << price << "\n"; 
    }
    else {
        cout << "Invalid price entered for the stock: " << name << "\n";
    }
}