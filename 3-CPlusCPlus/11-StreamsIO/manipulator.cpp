#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

int main(int argc, char *argv[]) {

    int num = 1842;
    double val = 79.861;
    double negval = -55.78;
    string name = "Duke University";
    
    cout << "Demonstrate field width (requires iomanip include)\n";
    cout << "01234567890123456789\n";
    cout << num << endl;               // normal output
    cout << setw(20) << num << endl;    // field large enough, note right justification
    cout << setw(2) << num << endl;    // field too small
    
    cout << val << endl;
    cout << setw(20) << val << endl;
    cout << setw(2) << val << endl;
    
    cout << name << endl;
    cout << setw(20) << name << endl;
    cout << setw(2) << name << endl;

    cout << "Demonstrate justification and width\n";
    cout << "01234567890123456789\n";
	cout << "Default:" << endl;
	cout << setw(20) << num << endl;
	cout << setw(20) << val << endl; 
	cout << setw(20) << negval << endl; 
	cout << setw(20) << name << endl;  
	
	cout << "Using right:" << right << endl;
	cout << setw(20) << num << endl;
	cout << setw(20) << val << endl;
	cout << setw(20) << negval << endl; 
	cout << setw(20) << name << endl;
	
	cout << "Using left:" << left << endl;
	cout << setw(20) << num << endl;
	cout << setw(20) << val << endl; 
	cout << setw(20) << negval << endl; 
	cout << setw(20) << name << endl;
	
	cout << "Using internal:" << internal << endl;
	cout << setw(20) << num << endl;
	cout << setw(20) << val << endl; 
	cout << setw(20) << negval << endl; 
	cout << setw(20) << name << endl;

    return EXIT_SUCCESS;
}