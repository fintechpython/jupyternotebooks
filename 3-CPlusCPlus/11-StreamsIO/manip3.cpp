#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

int main(int argc, char *argv[]) {
    // Formatting floating Point Numbers
	double num = 123;
	double val = 431.12234;
	double val2 = 1.237829183772;
	double val3 = 1234567.123;
	
	// Default output
	// Notice the default precision
	// is 6 for double/float that is
	// 6 digits will be displayed
	cout << "Default: " << endl;
	cout << num  << endl
	     << val  << endl
	     << val2 << endl
	     << val3 << endl;
	     
	// set fixed and do some spacing
	cout << fixed;
	cout << endl << "fixed: " << endl;
	cout << num  << endl
	     << val  << endl
	     << val2 << endl
	     << val3 << endl;
	
	// set scientific and do some spacing
	cout << scientific;
	cout << endl << "scientific: " << endl;
	cout << num  << endl
	 << val  << endl
	 << val2 << endl
	 << val3 << endl;
    return EXIT_SUCCESS;
}