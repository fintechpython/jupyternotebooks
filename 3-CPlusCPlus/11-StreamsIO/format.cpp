#include <iostream>
#include <format> // Include the <format> header for C++20 formatting
#include <print>  // requires C++23

int main() {
    int value = 42;
    double pi = 3.14159;

    // Basic formatting using std::format
    std::string formatted_string = std::format("The value is {} and pi is {}", value, pi);
    std::cout << formatted_string << std::endl;

    // Formatting with width and precision
    std::cout << std::format("Value: {:5} Pi: {:.2f}\n", value, pi);

    // Aligning text to the left
    std::cout << std::format("Value: {:<5} Pi: {:.2f}\n", value, pi);

    // Formatting with padding and precision
    std::print("Value: {:-^10} Pi: {:.3f}\n", value, pi);    //std::println() is also available

    return 0;
}

