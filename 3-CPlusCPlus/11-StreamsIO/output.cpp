#include <iostream>
#include <fstream>
#include <string>

#include <iostream>
#include <fstream>
using namespace std;


int main(int argc, char *argv[]) {
    std::string filename = "file_name.txt";

    std::ofstream myFile(filename);
    if (!myFile.is_open()) {
        std::cerr << "Unable to open file: " << filename << std::endl;
        return EXIT_FAILURE;
    }

    myFile << "It was the best of times, it was the worst of times, \n";
    myFile << "it was the age of wisdom, it was the age of foolishness, \n";
    myFile << "it was the epoch of belief, it was the epoch of incredulity, \n";
    myFile << "it was the season of light, it was the season of darkness, \n";
    myFile << "it was the spring of hope, it was the winter of despair. - Charles Dickens\n";

    myFile.close();

    return EXIT_SUCCESS;
}