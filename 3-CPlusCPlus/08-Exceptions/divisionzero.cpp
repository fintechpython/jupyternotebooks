#include <iostream>

int main(int argc, char *argv[]) {
    try {
        
        int divisor = 0;
        if (divisor == 0) {
            throw std::runtime_error("Division by zero exception");   // Note: throwing a value object here
        }
        int result = 10 / divisor;  // this statement never executes.  Creates a "float-point" exception, but outside of C++
                                    // can not detect this error
    }
    catch (const std::runtime_error &e) { // note output to cout instead of cerr to display within docable.
        std::cout << "Runtime error: " << e.what() <<  "\n";
        return EXIT_FAILURE;
    } catch (...) {
        std::cout << "Unknown exception caught" <<  "\n";
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}