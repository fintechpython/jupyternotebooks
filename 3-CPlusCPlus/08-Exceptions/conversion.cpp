#include <iostream>
#include <string>

int convertInt(std::string s) {
    std::size_t idx = 0;
    int result = std::stoi(s,&idx);
    if (idx != s.size()) {
        throw std::invalid_argument("unprocessed input: "+s);
    }
    return result; 
}

int main() {
    try {
        std::cout << convertInt("100") << "\n";
        std::cout << convertInt("100.4") << "\n";
    } catch (const std::invalid_argument& a) {
        std::cerr << "Invalid argument: " << a.what() << "\n";
        return EXIT_FAILURE;
    } catch (const std::out_of_range& r) {
        std::cerr << "out of range of int: " << r.what() << "\n";
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}