#include <iostream>
#include <string>
#include <stdexcept>
#include <vector>

using namespace std;

int main(int argc, char *argv[]) {
    vector<double> numbers;
    string line;
    
    cout << "Type some numbers to average  Hit ctrl-d when you are complete." << endl;
    while (getline(cin,line)) {
        try {
            double d = std::stod(line); 
            numbers.push_back(d);
        } catch (const std::invalid_argument&) {
            cerr << "Invalid argument: " << line << endl;
        } catch (const std::out_of_range&) {
            cerr << "out of range of double: " << line << endl;
        }
    }
    double total = 0.0;
    for (int i=0; i < numbers.size(); i++) {
        total += numbers[i];
    }
    cout << "Average: " << total/numbers.size() << endl;
    cout << "Number of elements: " << numbers.size() << endl;
    
    return EXIT_SUCCESS;
}
