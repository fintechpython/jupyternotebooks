// Compile: g++ -std=c++17 -o map map.cpp
#include <iostream>
#include <map>

int main() {
    std::map<std::string, int> ageMap;
    ageMap["Alice"] = 25;
    ageMap["Bob"] = 30;
    ageMap.insert({"Charlie", 35});

    // Find a value
    if (ageMap.find("Bob") != ageMap.end()) {
        std::cout << "Bob's age: " << ageMap["Bob"] << std::endl;
    }

    // Iterate over elements
    for (const auto& pair : ageMap) {
        std::cout << pair.first << ": " << pair.second << std::endl;
    }

    return 0;
}