// Compile: g++ -std=c++17 -o unordered_set unordered_set.cpp
#include <iostream>
#include <unordered_set>

int main() {
    std::unordered_set<int> numberSet = {5, 1, 4, 2};

    // Insert an element
    numberSet.insert(3);

    // Find an element
    if (numberSet.find(3) != numberSet.end()) {
        std::cout << "3 is in the set." << std::endl;
    }

    // Iterate over elements (order not guaranteed)
    for (int num : numberSet) {
        std::cout << num << " ";
    }
    std::cout << std::endl;

    return 0;
}