#include <iostream>
#include <string>
#include <stdexcept>
#include <vector>

using namespace std;

template <typename T>
void print(T begin, T end) {
    while (begin != end) {
        cout << *begin << "\n";
        begin++;
    }
}
int main(int argc, char *argv[]) {
    vector<string> lines;
    lines.push_back("It was the best of times.");
    lines.push_back("It was the worst of times.");
    lines.push_back("It was the age of wisdom");
    lines.push_back("It was the age of foolishness");
    
    
    for (vector<string>::iterator it = lines.begin();    // lines.begin() marks the start of the container range
         it < lines.end();                               // lines.end() marks one past the end of the container range
         it++ ) {
        cout << *it << endl;        
    } 

    cout << "\nNow, repeat, but using a generic function and the iterator range\n";

    print(lines.begin(),lines.end());

    cout << "\n\nNow in reverse:\n";
    for (auto it = lines.rbegin();  it < lines.rend(); it++ ) { //loop backwards, use auto for the variable type
        cout << *it << endl;        
    }    

    return EXIT_SUCCESS;
}
