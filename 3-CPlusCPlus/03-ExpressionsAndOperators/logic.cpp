#include <iostream>
using std::cout;
using std::endl;

int main() {
    int a = 11, b = 5, c = 11, result;

    result = (a == b) && (c > b);
    cout << "(a == b) && (c > b) is " << result << "\n";

    result = (a == b) && (c < b);
    cout << "(a == b) && (c < b) is " << result << "\n";

    result = (a == b) || (c < b);
    cout << "(a == b) || (c < b) is " << result << "\n";

    result = (a != b) || (c < b);
    cout << "(a != b) || (c < b) is " << result << "\n";

    result = !(a != b);
    cout << "!(a != b) is  " << result << "\n";

    result = not(a == b);
    cout << "!(a == b) is  " << result << "\n";

    return 0;
}