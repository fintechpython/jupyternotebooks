#include <iostream>
using std::cout;
using std::endl;

int main() {
    int a = 11, b = 5, c = 11;

    cout << a << " == " << b << " is " << (int) (a == b) << "\n";
    cout << a << " == " << c << " is " << (int) (a == c) << "\n";
    cout << a << " > "  << b << " is " << (int) (a > b) << "\n";
    cout << a << " > "  << c << " is " << (int) (a > c) << "\n";
    cout << a << " < "  << b << " is " << (int) (a < b) << "\n";
    cout << a << " < "  << c << " is " << (int) (a < c) << "\n";
    cout << a << " != " << b << " is " << (int) (a != b) << "\n";
    cout << a << " != " << c << " is " << (int) (a != c) << "\n";
    cout << a << " >= " << b << " is " << (int) (a >= b) << "\n";
    cout << a << " >= " << c << " is " << (int) (a >= c) << "\n";
    cout << a << " <= " << b << " is " << (int) (a <= b) << "\n";
    cout << a << " <= " << c << " is " << (int) (a <= c) << "\n";

    return 0;
}