#include <iostream>
using namespace std;

int main(int argc, char *argv[]) {
    int a = 9,b = 4;
    int result;
    
    result = a * b;
    cout << "a * b = " << result << "\n";

    result = a / b;
    cout << "a / b = " << result << "\n";

    cout << "a / b = " << (float) a / b << "\n";   /* results in a float value */ 

    result = a % b;
    cout << "a % b = " << result << "\n";

    cout << "-3 % 9 = " << -3 % 9 << "\n";   /* NOTE: remainder, not modulo.  Python produces 6 */

    result = a + b;
    cout << "a + b = " << result << "\n";

    result = a - b;
    cout << "a - b = %" << result << "\n";
    
    return EXIT_SUCCESS;
}