#include <iostream>
using namespace std;

int main(int argc, char *argv[]) {
    int i = 20;
    int j = 4;

    cout << "i: " << i << ", j: " << j << "\n";

    i = j;

    cout << "i: " << i << ", j: " << j << "\n";

    return 0;
}