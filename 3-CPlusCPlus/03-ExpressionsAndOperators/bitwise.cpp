#include <iostream>
using std::cout;
using std::endl;


std::string toBinaryString(int number) {
    if (number == 0) { return "0"; }

    std::string binary = "";
    while (number > 0) {
        binary = std::to_string(number % 2) + binary; // Add remainder at the front of string
        number /= 2; 
    }

    return binary;
}

void pBinary(int number, int num_digits) {
    std::string binary = toBinaryString(number);
    while (binary.length() < num_digits) {
        binary = "0" + binary;
    }
    cout << binary;
    /*
    int digit;
    for(digit = num_digits - 1; digit >= 0; digit--) {
        cout << (number & (1 << digit)) ? '1' : '0';
    }
    */
}

int main() {
    unsigned int a = 8;
    unsigned int b = 8;
    unsigned int c = 7;
    unsigned int d = 1;

    pBinary(a,4); cout << " & "; pBinary(a,4); cout << " = "; pBinary(a&b,4); cout << "\n";
    pBinary(a,4); cout << " & "; pBinary(c,4); cout << " = "; pBinary(a&c,4); cout << "\n";
    pBinary(a,4); cout << " | "; pBinary(c,4); cout << " = "; pBinary(a|c,4); cout << "\n";
    pBinary(c,4); cout << " ^ "; pBinary(d,4); cout << " = "; pBinary(c^d,4); cout << "\n";
    cout << "~"; pBinary(c,4); cout << " = "; pBinary(~c,4); cout << "\n";
    pBinary(d,4); cout << " << 3";  cout << " = "; pBinary(d << 3,4); cout << "\n";
    pBinary(a,4); cout << " >> 1";  cout << " = "; pBinary(a >> 1,4); cout << "\n";
}