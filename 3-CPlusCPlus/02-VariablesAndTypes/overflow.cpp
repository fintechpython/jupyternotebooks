#include <iostream>
using namespace std;

int main() {
    int value = 3;

    for (int i = 1; i < 21; i++)  {
        cout << i << " " << value << "\n";
        value *= 3;
    }
    return 0;
}