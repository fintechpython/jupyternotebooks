#include <iostream>
using namespace std;

int main() {
    int sum = 22;
    int count = 7;
    double mean;

    /* implicit conversion after perform integer division */
    mean =  sum / count;
    cout << "Value of mean: " << mean << "\n";

    /* explicit conversion: convert an int to a double, then perform float division */
    mean = (double) sum / count;
    cout << "Value of mean: " << mean << "\n";

    /* implicit conversion, can be raised as a warning through the gcc option -Wconversion */
    int average = mean;
    cout << "Average: " << average << "\n";

    /* programmer demonstrating clear intent to perform the conversion (preferred) */
    average = (int) mean;
    cout << "Average: " << average << "\n";
}