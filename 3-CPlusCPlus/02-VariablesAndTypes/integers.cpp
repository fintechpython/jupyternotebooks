#include <iostream>
#include <limits>

int main() {
    std::cout << "char:\n" <<
                 "  bits: "   << std::numeric_limits<char>::digits      << "\n" <<
                 "  signed: " << std::numeric_limits<char>::is_signed   << "\n" <<
                 "  min: "    << (int) std::numeric_limits<char>::min() << "\n" <<
                 "  max: "    << (int) std::numeric_limits<char>::max() << "\n";

    // the cast to an int type was necessary as C++ would have displayed the min/max
    // char instead

    std::cout << "int:\n" <<
                 "  bits: "   << std::numeric_limits<int>::digits    << "\n" <<
                 "  signed: " << std::numeric_limits<int>::is_signed << "\n" <<
                 "  min: "    << std::numeric_limits<int>::min()     << "\n" <<
                 "  max: "    << std::numeric_limits<int>::max()     << "\n";                 

    std::cout << "long:\n" <<
                 "  bits: "   << std::numeric_limits<long>::digits    << "\n" <<
                 "  signed: " << std::numeric_limits<long>::is_signed << "\n" <<
                 "  min: "    << std::numeric_limits<long>::min()     << "\n" <<
                 "  max: "    << std::numeric_limits<long>::max()     << "\n";                 

    std::cout << "short:\n" <<
                 "  bits: "   << std::numeric_limits<short>::digits    << "\n" <<
                 "  signed: " << std::numeric_limits<short>::is_signed << "\n" <<
                 "  min: "    << std::numeric_limits<short>::min()     << "\n" <<
                 "  max: "    << std::numeric_limits<short>::max()     << "\n";      

    std::cout << "unsigned char:\n" <<
                 "  bits: "   << std::numeric_limits<unsigned char>::digits      << "\n" <<
                 "  signed: " << std::numeric_limits<unsigned char>::is_signed   << "\n" <<
                 "  min: "    << (int) std::numeric_limits<unsigned char>::min() << "\n" <<
                 "  max: "    << (int) std::numeric_limits<unsigned char>::max() << "\n";                   

   std::cout << "unsigned int:\n" <<
                 "  bits: "   << std::numeric_limits<unsigned int>::digits    << "\n" <<
                 "  signed: " << std::numeric_limits<unsigned int>::is_signed << "\n" <<
                 "  min: "    << std::numeric_limits<unsigned int>::min()     << "\n" <<
                 "  max: "    << std::numeric_limits<unsigned int>::max()     << "\n";                   

    int i = 5;
    std::cout << "int size (bytes): " << sizeof(i) << "\n";
    
    return 0;
}