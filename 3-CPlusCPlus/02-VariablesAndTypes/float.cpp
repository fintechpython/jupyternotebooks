#include <iostream>
#include <limits>

int main() {
    std::cout << "double:\n" <<
                 "  precision bits: "   << std::numeric_limits<double>::digits      << "\n" <<
                 "  signed: " << std::numeric_limits<double>::is_signed   << "\n" <<
                 "  min: "    << std::numeric_limits<double>::min() << "\n" <<
                 "  max: "    << std::numeric_limits<double>::max() << "\n"
                 "  exponent bits: " << 
                    sizeof(double) * 8 -  
                    std::numeric_limits<double>::digits -
                    std::numeric_limits<double>::is_signed << "\n";


    return 0;
}