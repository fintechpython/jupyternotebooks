#include <stdio.h>
#include <stdlib.h>

typedef struct _point point;
struct _point {
    int x;
    int y;
};

int main(int argc, char *argv[]) {
    point p;
    
    p.x = 1;
    p.y = 5;

    point q = {40, 60};

    printf("(x,y): (%d,%d)\n", p.x, p.y);
    printf("(x,y): (%d,%d)\n", q.x, q.y);

    return 0;
}