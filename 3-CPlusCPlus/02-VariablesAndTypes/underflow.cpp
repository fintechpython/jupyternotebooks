#include <iostream>
using namespace std;

int main(int argc, char** argv) {
    float value = 0.3;

    for (int i = 1; i < 12; i++)  {
        cout << i << " " << value << "\n";
        value /= 300000;
    }
    return 0;
}
