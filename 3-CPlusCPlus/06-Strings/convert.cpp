#include <iostream>
#include <string>
#include <stdexcept>
using namespace std;

int main() {
    double d;
    string line;
    
    cout << "Type some data to convert a double.  Hit ctrl-d when you are complete." << endl;
    while (getline(cin,line)) {
        try {
            d = std::stod(line); 
            cout << "Converted number: " << d << "\n";
        } catch (const std::invalid_argument&) {
            cerr << "Invalid argument: " << line << "\n";
        } catch (const std::out_of_range&) {
            cerr << "out of range of double: " << line << "\n";
        }
    }

    return EXIT_SUCCESS;
}
