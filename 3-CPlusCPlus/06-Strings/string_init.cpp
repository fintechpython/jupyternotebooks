#include <iostream>
#include <string>
using std::string;

int main(){
    string s1;                // defualt initialization, string is empty
    string x = "hello world"; // Uses a conversion constructor from a string literal
    string y("John");         
    string z{"Steve"};        
    string w{y};              // w is a copy of y

    std::cout << x << "\n" << s1 << "\n" << y << "\n" << z << "\n";
}