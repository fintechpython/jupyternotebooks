#include <iostream>
#include <string>
using namespace std; 

int main() {
    string line;
    
    cout << "Type some data to echo.  Hit ctrl-d when you are complete." << endl;
    while (getline(cin,line)) {
        cout <<  line <<  "\n";
    }
}