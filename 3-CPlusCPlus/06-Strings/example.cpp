#include <iostream>
#include <string>
using namespace std;;

int main(){
    string s1;                // default initialization, string is empty
    string x = "hello";       // Uses the copy-assignment operator for the given literal
    string y("John");         // y is a copy of the string literal

    if (s1.empty()) {
        cout << "s1 was empty, but has a capacity of "<< s1.capacity() << "\n";
    }

    if (not x.empty()) {
        cout << "x has a length of " << x.length() << " with capacity - " << x.capacity() << "\n";
        cout << "x has a length of " << x.size() <<  "\n";   //size was added for consistency with the standard library
    }

    x += " world";   // concatenation, existing object is altered - C++ strings are mutable!
    cout << x << " - length: " << x.length() <<  ", capacity: " << x.capacity() << "\n";

    if (x == y) { // comparison operators overloaded
        cout << "strings are the same" << "\n";
    }
    else {
        cout << "the two strings differ" << "\n";
    }

    cout << x.substr(0,5) << "-"<< x.substr(6) << "\n";

    string line = "It was the best of times, it was the worst of times";
    cout << line.find("best") << "\n";

    // npos is constant containing the largest possible size 
    // of a string (same as max value of size_t)
    // As find returns an unsigned value, -1 is not feasible so the
    // largest possible value is used to signify a string was not found.
    cout << (line.find("greatest") == string::npos) << "\n";
}