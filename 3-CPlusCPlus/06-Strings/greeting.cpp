#include <iostream>
#include <string>
using namespace std; 

int main() {
    string name;

    cout << "What is your name?" << endl;
    cin >> name;
    cout << "It's nice to meet you, " << name << "\n";
}