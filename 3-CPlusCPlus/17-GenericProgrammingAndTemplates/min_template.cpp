#include <iostream>
#include <string>
using namespace std;

template <typename T>
T myMin(T a, T b) {
    if (a < b) {
        return a;
    }
    else {
        return b;
    }
}

int main(int argc, char *argv[]) {
    cout << myMin(1.0,0.5) << endl;
    cout << myMin<int>(42, 37.4) << endl;

    // use the C++ string class
    string s1("Jane");
    string s2("John");
    cout << myMin(s1, s2) << endl;

    return EXIT_SUCCESS;
}