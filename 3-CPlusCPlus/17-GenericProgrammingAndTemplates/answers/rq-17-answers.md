# CPlusPlus / Generic Programming And Templates
1. *What is a template?*

   A template in C++ is a powerful feature that allows you to write generic code that can work with different data types without having to duplicate the code for each type. It's essentially a blueprint or a parameterized formula for creating functions or classes.

2. *How does generic programming differ from object-oriented programming?*

   Generic programming and object-oriented programming (OOP) differ in several key aspects:
   a. generic programming uses parameterized types to achieve abstraction and code reuse, while OOP relies on classes and inheritance hierarchies. 
   b. generic programming focuses on creating algorithms and data structures that work with many types, employing compile-time polymorphism, whereas OOP models real-world entities and their relationships, typically using runtime polymorphism through virtual functions. 
   c. generic programming often offers more flexibility in terms of types and better performance due to compile-time code generation, while OOP provides a more structured approach to modeling complex systems.

3. *How do you declare a template function in C++?*

   Using the syntax: template <typename T> returnType functionName(parameters)

4. *Why are templated class definitions typically included in header files?*

   Because the entire class definition must be available for the compiler to instantiate the class.

5. *What happens if a type used with a template doesn't support an operation used in the template?*

   It results in a compilation error when the template is instantiated with that type.

6. *How do you declare a template with a non-type parameter?*

   For example: template <typename T, int N>

7. *What is the difference between function overloading and function templates?*

   Function overloading uses different function definitions for different types, while function templates use a single definition for multiple types.
