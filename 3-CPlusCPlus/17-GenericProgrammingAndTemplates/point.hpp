#include <string>
#include <cmath>
using std::string;

#ifndef POINT_TEMPLATE_H_
#define POINT_TEMPLATE_H_

template <typename T>
class Point {
private:
    T x;
    T y;

public:
    Point(T initialX, T initialY) : x{initialX}, y{initialY} {}

    T getX() const {
        return x;
    }

    void setX(T val) {
        x = val;
    }

    T getY() const{
        return this->y;
    }

    void setY(T val) {
        this->y = val;
    }

    T distance(Point other) const {
        T dx = x - other.x;
        T dy = y - other.y;
        return sqrt(dx * dx + dy *dy);
}

    string toString() {
        return "("+std::to_string(x)+","+std::to_string(y)+")";
    }
};

#endif