#include <iostream>
using namespace std;

class Point {
private:
    double x, y;
public:
    Point(double initialX = 0.0, double initialY = 0.0) : x{initialX}, y{initialY} {}
    double getX() const { return x; }
    double getY() const { return y; }
    void setX(double val) { x = val; }
    void setY(double val) { y = val; }
};

int main(int argc, char *argv[]) {
    Point p(5,2);
    Point& c = p;
    Point& d(p);
    c.setX(1);

    cout << "p: " << p.getX() << "," << p.getY() << endl;
    cout << "c aka p: " << c.getX() << "," << c.getY() << endl;
    cout << "d aka p: " << d.getX() << "," << d.getY() << endl;
    
    // Demonstrates that c and d are references / aliases to the original object
    cout << "Memory address - p:" << &p << endl;
    cout << "Memory address - c:" << &c << endl;
    cout << "Memory address - d:" << &d << endl;
}