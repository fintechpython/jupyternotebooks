#include <iostream>

int* realloc_int_array(int* oldArray, std::size_t oldSize, std::size_t newSize) {
    if (newSize == 0) {
        delete[] oldArray;
        return nullptr;
    }
    int* newArray = new int[newSize];
    if(oldArray != nullptr) {
        std::size_t copySize = (oldSize < newSize) ? oldSize : newSize;
        std::copy(oldArray, oldArray + copySize, newArray);
        delete[] oldArray;
    }
    return newArray;
}

int main() {
    int* arr = new int[5];
    for (int i = 0; i < 5; ++i) {
        arr[i] = i;
    }

    arr = realloc_int_array(arr, 5, 10);  // change the size to 10.

    // DANGER!!! Accessing the newly allocate memory before assignment 
    std::cout << arr[9] << " - do not do this!" << std::endl;
    
    delete[] arr;

    return 0;
}
