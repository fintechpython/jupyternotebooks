#include <iostream>
using namespace std;

class Point {
private:
    double x, y;
public:
    Point(double initialX = 0.0, double initialY = 0.0) : x{initialX}, y{initialY} {}
    double getX() const { return x; }
    double getY() const { return y; }
    void setX(double val) { x = val; }
    void setY(double val) { y = val; }

    Point operator+(const Point& other) const { 
        return Point( x + other.x, y + other.y);
    }
    Point& operator+=(const Point& other){ 
        this->x += other.getX();
        this->y += other.getY();
        return *this;
    }
};

void swap(int& a, int& b) {
    int temp = a;
    a = b;
    b = temp;
}

void swap(Point& a, Point&  b) {
    Point temp = a;
    a = b;
    b = temp;
}

int main(int argc, char *argv[]) {
    Point a(5,2);
    Point b(-12, 7);

    a = a + b;  // a = -7,9 now

    swap(a,b);

    cout << "a: " << a.getX() << "," << a.getY() << endl;
    cout << "b: " << b.getX() << "," << b.getY() << endl;

    int i = 42;
    int j = 92;
    swap(i,j);
    cout << "i: " << i << endl;
    cout << "j: " << j << endl;
}