# CPlusPlus / Namespace
1. *What is the primary purpose of namespaces in C++?*

   To avoid name collisions and organize code into logical groups.

2. *How do you declare a namespace in C++?*

   Using the namespace keyword, e.g., namespace MyNamespace { ... }

3. *What is the syntax for accessing a name inside a namespace?*

   Using the scope resolution operator (::), e.g., MyNamespace::myFunction()

4. *What is the difference between a using declaration and a using directive?*
   
   A using declaration brings in a specific name, while a using directive brings in all names from a namespace.

5. *How do you handle name conflicts between different namespaces?*

   By using fully qualified names or creating namespace aliases to disambiguate.
