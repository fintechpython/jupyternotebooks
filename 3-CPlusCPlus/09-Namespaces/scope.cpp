// g++ -std=c++17 -o scope scope.cpp

#include <iostream>
using namespace std;

int var = 25;   // Global scope

namespace MySpace {
    int var = 15;   // Namespace scope
}

void foo() {
    cout << "Inside foo():\n";
    
    cout << "    " << var << ": global scope - var\n";
    int var = 20;   // Function scope
    cout << "    " << var << ": local/function scope - var\n";
    cout << "    " << ::var << ": global scope - var\n";
    
    if (true) {
        int var = 30;  // Block scope (visible only inside this block)
        int blockvar = 100; 
        cout << "    " << var << ": local(block) scope - var\n";
    }
    // cout << blockvar;  -- compiler error if uncommented, not in the current scope
    cout << "    " << var << ": local/function scope - var\n";
}

class MyClass {
public:
    static int static_var;  // Class scope (static members)
    int var;         // Class scope (non-static members)
    
    void classScope() {
        cout << "Inside MyClass::classScope():\n";
        cout << "    " << var << ": class scope - var\n";
        cout << "    " << static_var << ": class scope - static - static_var\n";

        int var = 97;
        cout << "    " << var << ": local scope - var\n";
        cout << "    " << this->var << ": class scope - var\n";

    }
};

// Define the static member outside the class
int MyClass::static_var = 99;

int main() {
    cout << "Inside main():\n";
    cout << "    " << var << ": Global scope - var\n";
    cout << "    " << MySpace::var << ": Namespace scope (MySpace) - var\n";

    {
        int var = 50; // local scope
        cout << "    " << var << ": local scope - var\n";
        cout << "    " << ::var << ": global scope - var\n";
    }
    int var = 80;
    for (int var=60; var<61; var++) {
        // int var = 70;  // Not allowed, redefinition of "var" in the same scope
        cout << "    " << var << ": statement scope - var\n";
    }
    cout << "    " << var << ": local/function scope - var\n";

    // Accessing static member directly using class name
    cout << "    " << MyClass::static_var << ": class scope - MyClass::static_var\n";
    
    foo();
    

    // Class scope demonstration
    MyClass obj;
    obj.var = 40;
    obj.classScope();  // Access class members

    
    return 0;
}
