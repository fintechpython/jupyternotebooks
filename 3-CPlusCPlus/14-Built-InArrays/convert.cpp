#include <iostream>
#include <array>

int main(int argc, char *argv[]) { 
    int numbers[] = { 4820, 3130, 3124, 1992, 1842 };
   
    // create std::array from a built-in array
    auto array{std::to_array(numbers)};

    std::cout << "array.size() = " << array.size() << "\n";
    for (int num: numbers) {
        std::cout << num << " ";
    }
    std::cout << "\n";

}