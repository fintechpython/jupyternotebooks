#include <iostream>
#include <algorithm>

int main(int argc, char *argv[]) {
    int numbers[] = { 4820, 3130, 3124, 1992, 1842 };

    std::sort(std::begin(numbers),std::end(numbers));  // try placing "&numbers[0]" as the argument to begin()

    for (int num: numbers) {
        std::cout << num << "\n";
    }

    return EXIT_SUCCESS;
}