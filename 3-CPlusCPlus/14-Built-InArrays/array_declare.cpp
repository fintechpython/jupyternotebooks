#include <iostream>

int main(int argc, char *argv[]) {
    int numbers[10]; /* declares an array called numbers with 10 integers, values not initialized */ 
    int contestants[] = {1, 2, 3}; /* array of 3 elements initialized with the values of 1,2,3 */
    float averages[5] = {7.6, 3.2}; /* array of 5 elements, the last 3 elements are zeros */

    const int nRows = 20;
    const int nColumns = 40;
    int matrix[nRows][nColumns]; /* declares a matrix */

    /* Create an array where the size comes from a variable */
    int numElements = 30;
    double data[numElements];
    double d = 3.14;
    std::cout << "data array size: " << sizeof(data) << "\n";
    std::cout << "size of d: " << sizeof(d) << "\n";

   return EXIT_SUCCESS;
}