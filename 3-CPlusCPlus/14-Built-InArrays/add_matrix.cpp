#include <iostream>

int main(int argc, char *argv[]) {
    int a[3][4] = { { 5, 6, 7, 8 }, { 10, 20, 30, 40 }, {5, 15, 20, 25} };
    int b[3][4] = { { 1, 2, 3, 4 }, {  3,  2,  1,  0 }, {0, 1,  2,  3 } };
    int sum[3][4];
    
    for (int row = 0; row < 3; row++) {
        for (int column = 0; column < 4; column++) {
            sum[row][column] = a[row][column] + b[row][column]; 
        }
    }

    std::cout << "Result:\n";    
    for (int row = 0; row < 3; row++) {
        for (int column = 0; column < 4; column++) {
            std::cout << "\t" << sum[row][column]; 
        }
        std::cout << "\n";    /*new line to end the row */
    }    

    /* demonstrates how the data is layed out in memory */
    std::cout << "\n";
    int *ptr = (int *) a;
    for (int i = 0; i < 12; i++) {
        std::cout << *ptr << " ";
        ptr++;
    }
    std::cout << "\n";

    return EXIT_SUCCESS;

}