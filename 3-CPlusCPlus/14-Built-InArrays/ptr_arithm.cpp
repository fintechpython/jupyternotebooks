#include <iostream>

int main(int argc, char *argv[]) {
    int i = 10;
    char c = 'A';
    
    int *ip = &i;
    char *cp = &c;

    /* demonstrates the difference when adding 1 between int and char pointer types */
    std::cout << "i: initial address " << ip << ", incremented address " << ip+1 << "\n";   /* Note: ip+1 does not point to valid memory location */
    std::cout << "c: initial address " << cp << ", incremented address " << cp+1 << "\n";   // notice how C++ streams assumes char * is C-style string. 
                                                                                            // this output line behavior is undefined
    std::cout << "c: initial address " << (void *) cp << ", incremented address " << (void *) cp+1 << "\n";  // cast to void * to show the address

    int arr[10] = {}; 
    int *ptr = arr;

    ptr++;        /* this now points to arr[1] */
    *ptr = 1;
    
    *(ptr+5) = 6;   /* ptr + 5 is not arr[6], we had incremented ptr in line 17 */
    
    ptr = &arr[9];   /* point ptr to the memory address of the last element */
    *ptr = 9;

    --ptr;     /* ptr now points to arr[8] */
    *ptr = 8;

    for (int i=0;i<10; i++) {
        std::cout << arr[i] << " ";
    }
    std::cout << "\n";


    return EXIT_SUCCESS;
}