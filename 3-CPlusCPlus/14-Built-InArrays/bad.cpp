#include <iostream>

int* f(size_t array_length) {
    int a[array_length] ;     /* allocates space for this on the call stack */
    return a;
}

int main(int argc, char *argv[]) {
    int *ptr = f(5);    /* f's return value points to a memory address */
                        /* that has been deallocated / no longer exists */

    std::cout  << *ptr << "\n";

    return EXIT_SUCCESS;
}