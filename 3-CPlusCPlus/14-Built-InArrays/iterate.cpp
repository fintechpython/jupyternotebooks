#include <iostream>
#include <iomanip>

int main() {
    char greeting[] = "Hello, World!";
    char* p = greeting;  // Pointer initialized to the start of the string

    while (*p != '\0') {
        std::cout << *p << ": " << std::setw(15) << std::left << p << " address: " << (void *) p << "\n";
        p++;
    }
    std::cout << "\n";

    return 0;
}