#include <array>
#include <iostream>
#include <numeric>
#include <span>
#include <vector>

// passing built-in array, need size to due to "decay"
void displayArray(const int items[], size_t size) {
    for (size_t i = 0; i < size; i++) {
        std::cout << items[i] << " ";
    }
    std::cout << "\n";
}

void displaySpan(std::span<const int> items) {
    for (const auto& item : items) { // spans are iterable
        std::cout << item << " ";
    }
    std::cout << "\n";
}

// modify elements in the original data structure
void square(std::span<int> items) {
    for (int& item : items) {
        item *= item;
    }
}

int main() {
    int value_array[] = {1, 2, 3, 4, 5};
    std::array value_stdarray{11, 12, 13, 14, 15};
    std::vector value_vector{101, 102, 103, 104, 105};

    std::cout << "value_array via displayArray: ";
    displayArray(value_array, 4);

    std::cout << "values_array via displaySpan: ";
    displaySpan(value_array); // notice how the compile automatically creates the span

    std::cout << "value_stdarray via displaySpan: ";
    displaySpan(value_stdarray);
    std::cout << "value_vector via displaySpan: ";
    displaySpan(value_vector);

    // Show that passing a span still refers to the underlying data (a view into it)
    square(value_array);
    std::cout << "value_array after squaring each element: ";
    displaySpan(value_array);

    // create a span object directly, use some of the methods in span: https://en.cppreference.com/w/cpp/container/span
    std::span mySpan{value_array}; // span<int>
    std::cout << "mySpan's first element: " << mySpan.front() << "\nmySpan's last element: " << mySpan.back() << "\n";

    // access a span element via []
    std::cout << "The element at index 2 is: " << mySpan[2] << "\n";

    // spans can be used with standard library algorithms 
    std::cout << "Sum mySpan: " << std::accumulate(std::begin(mySpan), std::end(mySpan), 0) << "\n";

    // Create subviews of a container
    std::cout << "First three elements of mySpan: ";
    displaySpan(mySpan.first(3));
    std::cout << "Last three elements of mySpan: ";
    displaySpan(mySpan.last(3));
    std::cout << "Middle three elements of mySpan: ";
    displaySpan(mySpan.subspan(1, 3));

    square(mySpan.subspan(1, 3)); // change subview's contents.
    std::cout << "value_array after modify subspan: ";
    displaySpan(value_array); 
}
// Note: Adapted from C++20: How to Program by Deitel and Deitel.