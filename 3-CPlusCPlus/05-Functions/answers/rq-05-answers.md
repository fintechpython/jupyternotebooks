# CPlusPlus / Functions
1. *Compare and contrast functions in Python and C++.*

    Similarities:
        Both languages use functions for code organization and reusability.
        Both support passing arguments and returning values.
        Both allow for default argument values.

    Differences:
        Syntax: C++ requires explicit type declarations for parameters and return values, while Python uses dynamic typing.
        Function overloading: C++ supports function overloading (multiple functions with the same name but different parameter lists), while Python does not.
        Return values: C++ functions must specify a return type, including 'void' for no return value. Python functions always return a value (None by default).
        Parameter passing: C++ uses call-by-value by default, with options for call-by-reference. Python uses call-by-object-reference.
        Function prototypes: C++ often requires function prototypes, while Python does not.

2. *Name the four parts of a function*

   Name, return type, parameters, and the function body
   
3. *Explain how C++ uses call by value. Why did this create issues with the swap function?*

    In C++, when arguments are passed to a function using call by value, a copy of the argument is created and passed to the function. Changes made to the parameter inside the function do not affect the original variable in the calling code. 
    Issues with the swap function: The classic swap function aims to exchange the values of two variables. However, using call by value creates problems:
    ```c++
    void swap(int a, int b) {
        int temp = a;
        a = b;
        b = temp;
    }

    int main() {
        int x = 5, y = 10;
        swap(x, y);
        // x is still 5, y is still 10
    }
    ```
    This swap function doesn't work as intended because:
    1. Copies of x and y are passed to the function.
    2. The swapping occurs on these copies (a and b), not the original variables.
    3. When the function ends, the changes are discarded with the copies.
    4. The original variables x and y remain unchanged.

    To solve this, C++ programmers typically use call by reference for the swap function:
    ```c++
    void swap(int& a, int& b) {
        int temp = a;
        a = b;
        b = temp;
    }
    ```

4. *Describe what the line

    ```c++
    char foo(int x)
    ```
    means in a function definition.*

    The line of this definition tells us that this is a function called foo, it takes an int argument called x, and it returns a char type of result.

5. *How do we syntactically distinguish between a function declaration and a function definition?*

    A function declaration tells the compiler about a function's name, return type, and parameters, but does not provide the body of the function. A function definition provides the actual body of the function.

6. *Why is it a good idea to initialize variables as they are declared?*

    It's a good idea to initialize variables as they are declared because an uninitialized variable can lead to obscure bugs. For instance, if a variable is used before it's assigned a value, it can result in undefined behavior. This behavior can vary across different machines and even different runs of the same program on the same machine.

7. *What is the difference between pass-by-value and pass-by-reference?*

    Pass-by-value, a copy of the argument is passed to the function, whereas in pass-by-reference, a reference to the argument is passed, allowing the function to modify the original value.