#include <iostream>

int main() {
    int original = 10;
    int &ref = original; // ref is a reference to original

    std::cout << "Original: " << original << ": " << &original << '\n';
    std::cout << "Reference: " << ref <<  ": " << &ref << '\n';   

    // Modifying the reference modifies the original variable
    ref = 20;

    std::cout << "After modification:" << std::endl;
    std::cout << "Original: " << original << std::endl; // Outputs: 20
    std::cout << "Reference: " << ref << std::endl;     // Outputs: 20

    return 0;
}