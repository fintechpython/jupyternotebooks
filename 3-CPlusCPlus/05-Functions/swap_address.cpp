#include <iostream>
using std::cout;

void swap(int a, int b) {
    int t = a;
    a = b;
    b = t;
    cout << "in swap: a address " << &a << "\n";
}

int main(int argc, char *argv[]) {
    int a = 10;
    int b = 15;
    cout << "before swap: a address " << &a << "\n";
    swap(a,b);
    cout << "after swap: a address " << &a << "\n";
}