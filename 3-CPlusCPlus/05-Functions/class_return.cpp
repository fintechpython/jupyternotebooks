#include <iostream>
using std::cout;

class point {
public:
    int x;
    int y;
};

point squarePoint(point t) {
    cout << "in call: t address " << &t << "\n";
    point result;
    result.x = t.x * t.x;
    result.y = t.y * t.y;
    cout << "in call: result address " << &result << "\n";
    return result;
}

int main(int argc, char *argv[]) {
    point p;
    
    p.x = 1;
    p.y = 5;

    cout << "(x,y): (" << p.x << ", " << p.y << ")" << "\n";
    point p2;
    cout << "before call: p2 address " <<  &p2 << "\n";
    p2 = squarePoint(p);
    cout << "(x,y): (" << p2.x << ", " << p2.y << ")" << "\n";
    cout << "after call: p2 address " << &p2 << "\n";

    return EXIT_SUCCESS;
}