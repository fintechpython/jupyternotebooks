#include <iostream>
using std::cout;

void swap(int a, int b) {
    int t = a;
    a = b;
    b = t;
    cout << "in swap: (a, b) = (" << a << ", " << b << ")" << "\n";
}

int main(int argc, char *argv[]) {
    int a = 10;
    int b = 15;
    cout << "before swap: (a, b) = (" << a << ", " << b << ")" << "\n";
    swap(a,b);
    cout << "after swap: (a, b) = (" << a << ", " << b << ")" << "\n";
    if (1) {
        int a = 33;
        int c = 50;
        cout << "Within if: a=" << a << "\n";
        cout << "Within if: b=" << b << "\n";
        cout << "Within if: c=" << c << "\n";
    }
    cout << "After if: a=" << a << "\n";
    cout << "Value of c: " << c << "\n";  // This line causes a compilation error. 
                                          // Try running to see the error, comment out the line, and run again 
                                          // Note: This is an exeption, to always work top-down with errors
                                          //       However, note that the first message is a warning, not an error.
}